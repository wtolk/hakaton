class WtolkUploader {
    constructor(options) {
        this.debug_mode = true;
        this.el = document.querySelector(options.el);
        this.input_name = options.input_name;

        this.createContainer();
        this.setListeners();
        this.setSortable();

        if (options.files) {
            this.setFiles(options.files);
            this.files_count = this.files.length;
            this.reloadItems();
        } else {
            this.files = [];
            this.files_count = 0;
        }
        this.loaded_files = [];
        this.loaded_once = true;
    }

    //Создаем контейнер
    createContainer() {
        this.container = document.createElement('div');
        this.container.classList.add('uploader_container');
        this.el.appendChild(this.container);

        this.createHeader();
        this.createFooter();
    }

    //Создаем верхнюю часть
    createHeader() {
        this.header = document.createElement('div');
        this.header.classList.add('uploader_header');
        this.container.appendChild(this.header);

        this.input_pool = document.createElement('div');
        this.input_pool.classList.add('uploader_input_pool');
        this.header.appendChild(this.input_pool);

        this.input_text = document.createElement('label');
        this.input_text.setAttribute('for', 'uploader_');
        this.input_text.innerHTML = 'Переместите сюда файлы или&nbsp;<span class="uploader_text_underlined"> выберите вручную</span>';
        this.input_text.classList.add('uploader_input_text');
        this.input_pool.appendChild(this.input_text);


        this.input = document.createElement('input');
        this.input.classList.add('uploader_input');
        this.input.setAttribute('type', 'file');
        this.input.setAttribute('name', this.input_name);
        this.input.setAttribute('multiple', true);
        this.input.id = 'uploader_';
        this.input_pool.appendChild(this.input);

        this.positions_input = document.createElement('input');
        this.positions_input.setAttribute('type', 'hidden');
        this.positions_input.setAttribute('name', 'uploader[positions]');
        this.container.appendChild(this.positions_input);

        this.remove_input = document.createElement('input');
        this.remove_input.setAttribute('type', 'hidden');
        this.remove_input.setAttribute('name', 'uploader[remove]');
        this.container.appendChild(this.remove_input);
    };

    createFooter() {
        this.files_list = document.createElement('div');
        this.files_list.classList.add('uploader_files_list');
        this.files_list.id = 'uploader_files_list';
        this.container.appendChild(this.files_list);
    }

    setListeners() {
        this.input.addEventListener('change', () => {
            this.loadFiles();
        });
        let el = this.header.firstElementChild.firstElementChild, k = 3;
        this.header.addEventListener('dragenter', () => {
            el.style.backgroundColor = 'rgba(0,0,0,.1)';
            el.style.padding = WtolkUploader.getIntStyle(el, 'padding') * k + 'px';
        });
        this.header.addEventListener('dragleave', () => {
            el.style.backgroundColor = 'rgba(255,255,255,0)';
            el.style.padding = WtolkUploader.getIntStyle(el, 'padding') / k + 'px';
        });
        this.header.addEventListener('drop', () => {
            el.style.backgroundColor = 'rgba(255,255,255,0)';
            el.style.padding = WtolkUploader.getIntStyle(el, 'padding') / k + 'px';
        })
    }

    static getIntStyle(el, property) {
        let css_val = window.getComputedStyle(el, null).getPropertyValue(property);
        return parseInt(css_val);
    }

    read(f) {
        var reader = new FileReader();
        reader.readAsDataURL(f);
        return new Promise((resolve) => {
            console.log(2)
            reader.onload = (e) => {
                resolve({
                    index: this.files_count,
                    path: e.target.result,
                    type: f.type,
                    filename: f.name,
                    uuid: WtolkUploader.getUuid()
                });
            };
        });
    }

     loading() {
        return new Promise(resolve => {
            Object.keys(this.input.files).forEach( (key) => {
                let type = this.input.files[key].type;
                let uuid = WtolkUploader.getUuid();
                if (type.split('/')[0] === 'image') {
                     this.read(this.input.files[key]).then((file) => {
                        this.files.push(file);
                        this.loaded_files.push(this.input.files[key]);
                        this.drawItem(file);
                        this.files_count++;
                        console.log(3)
                    }).then(() => {
                        // resolve();
                    });
                    // reader.readAsDataURL();
                } else {
                    let file = {index: this.files_count, type: type, filename: this.input.files[key].name, uuid: uuid};
                    this.files.push(file);
                    this.loaded_files.push(this.input.files[key]);
                    this.drawItem(file);
                    this.files_count++;
                    // resolve();
                }
            });
            resolve();
        });
    }

    loadFiles() {
        this.log('.....loadFiles......');

        if (this.checkFiles()) {
            console.log(1);
            this.loading().then(() => {
                console.log('after foreach');
                if (!this.loaded_once) {
                    this.updateInputFules()
                }
                this.loaded_once = false;
            });
        } else {
            this.revertUpload();
        }

    }

    updateInputFules() {
        console.log('update input files')
        var dT = new ClipboardEvent('').clipboardData || new DataTransfer();

        // dT.items.add(this.input.files.item(i));

        // for (var i = 0; i < this.input.files.length; i++) {
        //     for (var c = 0; c < this.loaded_files.length; c++) {
        //         if (this.loaded_files[c].name !== this.input.files[i].name) {
        //             dT.items.add(this.input.files.item(i));
        //         }
        //     }
        // }

        for (var i = 0; i < this.input.files.length; i++) {
            dT.items.add(this.input.files.item(i));
        }

        for (var j = 0; j < this.loaded_files.length; j++) {
            dT.items.add(this.loaded_files[j]);
        }

        this.input.files = dT.files;
    }

    checkFiles() {
        let result = true;
        this.loaded_files.forEach((file) => {
            for (var i = 0; i < this.input.files.length; i++) {
                if (this.input.files[i].name === file.name) {
                    result = false;
                }
            }
        });
        this.files.forEach((file) => {
            for (var i = 0; i < this.input.files.length; i++) {
                if (this.input.files[i].name === file.filename) {
                    result = false;
                }
            }
        });
        return result;
    }

    revertUpload() {
        alert('Один из загружаемых файлов уж загружен. Пожалйста, повторите попытку');
        console.log('Один из загружаемых файлов уж загружен. Пожалйста, повторите попытку');
        var dT = new ClipboardEvent('').clipboardData || new DataTransfer();
        if (!this.loaded_once) {
            for (var j = 0; j < this.loaded_files.length; j++) {
                dT.items.add(this.loaded_files[j]);
            }
        }
        this.input.files = dT.files;
    }

    reloadItems() {
        while (this.files_list.firstChild) {
            this.files_list.removeChild(this.files_list.firstChild);
        }

        this.files.forEach((file) => {
            this.drawItem(file)
        });
        this.updatePositions()
    }

    setFiles(files) {
        this.files = files;
        this.files.map((file) => {
            file.type = 'uploded/' + file.extension;
        });
    }

    drawItem(file) {
        let item = document.createElement('div');
        item.classList.add('uploader_file_card');

        if (['jpg', 'jpeg', 'png', 'gif'].indexOf(file.type.split('/')[1]) !== -1) {
            item.style.backgroundImage = 'url(' + file.path + ')';
        } else {
            let ext_container = document.createElement('div');
            ext_container.classList.add('uploader_ext_container');

            let ext_row = document.createElement('div');
            ext_row.classList.add('uploader_ext_row');

            let ext = document.createElement('div');
            ext.innerText = '.' + file.filename.split('.').pop();
            ext.classList.add('uploader_ext');

            ext_row.appendChild(ext);
            ext_container.appendChild(ext_row);
            item.appendChild(ext_container);
        }

        item.dataset.index = file.index;
        if (!file.uuid) {
            file.uuid = WtolkUploader.getUuid();
        }
        item.dataset.uuid = file.uuid;
        if (file.id) {
            item.dataset.id = file.id;
        }
        item.dataset.filename = file.filename;

        let remove_btn = document.createElement('i');
        remove_btn.classList.add('uploader_remove_btn');
        remove_btn.classList.add('icon-cross2');
        remove_btn.onclick = () => {
            this.removeItem(file)
        };

        let filename = document.createElement('div');
        filename.classList.add('uploader_filename');
        filename.innerHTML = file.filename;
        item.appendChild(filename);

        item.appendChild(remove_btn);
        this.files_list.appendChild(item);
        this.updatePositions()
    }

    setSortable() {
        this.log('...Set sortable...');
        Sortable.create(this.files_list, {
            onAdd: () => {
                this.updatePositions()
            },
            onEnd: () => {
                this.updatePositions()
            },
            onRemove: () => {
                this.updatePositions();
            }
        });
    }

    updatePositions() {
        let cards = document.querySelectorAll('.uploader_file_card');
        let positions = [];
        cards.forEach((card) => {
            positions.push(card.dataset);
        });
        this.positions_input.value = JSON.stringify(positions);
    }

    removeItem(file) {

        let item_key;
        this.files.forEach((f, k) => {
            if (f.filename === file.filename) {
                item_key = k;
            }
        });

        if (file.id) {
            let remove_list;
            if (this.remove_input.value === '') {
                remove_list = [];
            } else {
                remove_list = JSON.parse(this.remove_input.value);
            }
            remove_list.push(file.id);
            this.remove_input.value = JSON.stringify(remove_list);
        } else {
            var dT = new ClipboardEvent('').clipboardData || new DataTransfer();

            for (var i = 0; i < this.input.files.length; i++) {
                if (this.input.files[i].name !== file.filename) {
                    dT.items.add(this.input.files.item(i));
                }
            }
            this.input.files = dT.files;
        }

        this.files.splice(item_key, 1);
        this.files.forEach((file, key) => {
            file.index = key + 1;
        });

        let loaded_key;
        Object.keys(this.loaded_files).forEach((k) => {
            if (this.loaded_files[k].name === file.filename) {
                loaded_key = k;
            }
        });
        this.loaded_files.splice(loaded_key, 1);

        document.querySelectorAll('.uploader_file_card')[item_key].remove();
        // this.reloadItems();
        this.updatePositions();
    }


    static getUuid() {
        return `f${(~~(Math.random() * 1e8)).toString(16)}`;
    }

    log(msg) {
        if (this.debug_mode) {
            console.log(msg)
        }
    }
}