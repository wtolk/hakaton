<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class Servant
{
    protected $args;
    protected $version = '2.6';

    public function __construct($argv)
    {
        array_shift($argv);
        $this->args = $argv;
    }


    public function run()
    {
        $method = array_shift($this->args);

        if (method_exists($this, $method))
        {
            $this->{$method}($this->args);
            
        }
        else
        {
            echo "Method <".$method."> doesn't exists".PHP_EOL;
            
        }
    }



    // Возвраает массив, ключи которго - названия колонок таблицы, а значения - их типы.
    protected function getTableInfo($table_name)
    {
        $columns = Capsule::connection()->getSchemaBuilder()->getColumnListing($table_name);
        $info = [];
        foreach($columns as $column)
        {
            $info[$column] = Capsule::connection()->getSchemaBuilder()->getColumnType($table_name, $column);
        }

        return $info;
    }



    // Создаёт класс модели в app/models
    protected function make_model($params)
    {
        $class_name = $params[0];
        $table_name = isset($params[2])? $params[2]: $class_name;

        if (!Capsule::schema()->hasTable($table_name)) {
            echo "Table <".$table_name."> doesn't exists in database".PHP_EOL;
        }

        $info = $this->getTableInfo($table_name);

        $primary_key = key($info);

        $casts = PHP_EOL;
        foreach($info as $column => $type)
        {
            $casts .= "\t\t'".$column."' => '".$type."',".PHP_EOL;
        }
        $casts .= "\t";

        $template = file_get_contents(__DIR__.'/../templates/model.php');
        $template = str_replace('${class_name}', $class_name, $template);
        $template = str_replace('${table_name}', $table_name, $template);
        $template = str_replace('${primary_key}', $primary_key, $template);
        $template = str_replace('${casts}', $casts, $template);

        $fp = fopen(__DIR__."/../../../app/models/".$class_name.".php", "w");
        fwrite($fp, $template);
        fclose($fp);
        echo PHP_EOL.'Model '.$class_name.' was succesfully generated';
    }



    // Создает класс контроллер в app/controllers
    protected function make_controller($params)
    {
        $class_name = $params[0];
        $view_name = strtolower($class_name);

        $template = file_get_contents(__DIR__.'/../templates/controller.php');
        $template = str_replace('${class_name}', $class_name, $template);
        $template = str_replace('${view_name}', $view_name, $template);

        $fp = fopen(__DIR__ . "/../../../app/Controllers/" .$class_name."Controller.php", "w");
        fwrite($fp, $template);
        fclose($fp);

        echo PHP_EOL.'Controller '.$class_name.'Controller was successfully generated';
    }



    // Добавляет роуты в admin и public
    protected function make_routes($params)
    {
        $route = $params[0];
        $low_route = strtolower($route);

        $admin_php = file_get_contents(__DIR__."/../../../app/routes/admin.php");

        $admin_use = explode('//delimiter//', $admin_php)[0];
        $admin_use .= 'use App\\controllers\\'.$route.'Controller;'.PHP_EOL."//delimiter//";

        $admin_routes = explode('//delimiter//', $admin_php)[1];
        $admin_routes_templ = file_get_contents(__DIR__."/../templates/routes_admin.php");
        $admin_routes_templ = str_replace('${route}', $route, $admin_routes_templ);
        $admin_routes_templ = str_replace('${low_route}', $low_route, $admin_routes_templ);
        $admin_routes .= PHP_EOL.$admin_routes_templ.PHP_EOL;

        $admin_php = $admin_use.$admin_routes;

        $fp = fopen(__DIR__."/../../../app/routes/admin.php", "w");
        fwrite($fp, $admin_php);
        fclose($fp);


        $api_php = file_get_contents(__DIR__."/../../../app/routes/api.php");

        $api_use = explode('//delimiter//', $api_php)[0];
        $api_use .= 'use App\\controllers\\'.$route.'Controller;'.PHP_EOL."//delimiter//";

        $api_routes = explode('//delimiter//', $api_php)[1];
        $api_routes_templ = file_get_contents(__DIR__."/../templates/routes_api.php");
        $api_routes_templ = str_replace('${route}', $route, $api_routes_templ);
        $api_routes_templ = str_replace('${low_route}', $low_route, $api_routes_templ);
        $api_routes .= PHP_EOL.$api_routes_templ.PHP_EOL;

        $api_php = $api_use.$api_routes;

        $fp = fopen(__DIR__."/../../../app/routes/api.php", "w");
        fwrite($fp, $api_php);
        fclose($fp);

        echo PHP_EOL.'Routes was succesfully updated';
    }



    protected function make_views($params)
    {
        $class_name = $params[0];
        $table_name = $params[2];

        $class_name_low = strtolower($class_name);

        $info = $this->getTableInfo($table_name);

        $triple_tabs = "\t"."\t"."\t";

//        LIST
        $template = file_get_contents(__DIR__.'/../templates/view/list/1.php');

        $table_heads = PHP_EOL;
        foreach($info as $column => $type)
        {
            $table_heads .= $triple_tabs.'<th>'.$column.'</th>'.PHP_EOL;
        }
        $template.=$table_heads;

        $template .= file_get_contents(__DIR__.'/../templates/view/list/2.php');

        $table_cells = PHP_EOL;
        foreach($info as $column => $type)
        {
            $table_cells .= $triple_tabs.'<td>'.PHP_EOL.
                $triple_tabs."\t".'<a href="{{ path_for(\'${class_name_low}.showAdmin${class_name}Edit\', {\'id\':${class_name_low}.id}) }}">'.'{{ ${class_name_low}'.'.'.$column.'}}'.'</a>'.PHP_EOL.
                $triple_tabs.'</td>'.PHP_EOL;
        }
        $template.=$table_cells;

        $template .= file_get_contents(__DIR__.'/../templates/view/list/3.php');

        $template = str_replace('${class_name_low}', $class_name_low, $template);
        $template = str_replace('${class_name}', $class_name, $template);

        if (!file_exists(__DIR__."/../../../app/views/admin/".$class_name_low.'s'))
        {
            mkdir(__DIR__."/../../../app/views/admin/".$class_name_low.'s');
        }

        file_put_contents(__DIR__."/../../../app/views/admin/".$class_name_low.'s/'.$class_name_low."s-list.twig", $template);
//        --------------


//        FORM
        $template = file_get_contents(__DIR__.'/../templates/view/form/1.php');

        $template .= PHP_EOL;
        foreach($info as $column => $type)
        {
            if ($column == 'id')
            {
                continue;
            }
            $template .=  '<div class="form-group row">'.PHP_EOL;
            $template .= '<label class="col-form-label col-lg-3" for="'.$column.'-input">'.ucfirst($column).'</label>'.PHP_EOL;

            if ($type == 'string')
            {
                $input_type = 'text';
            }elseif (in_array($type, ['integer', 'tinyInteger', 'smallInteger', 'bigInteger', 'double', 'float', 'decimal']))
            {
                $input_type = 'number';
            }elseif ($type == 'date')
            {
                $input_type = 'date';
            }elseif ($type == 'boolean')
            {
                $input_type = 'chekbox';
            }

            $template .= '<div class="col-lg-9">'.PHP_EOL;

            if ($type != 'text')
            {
                $template .= '<input id="'.$column.'-input" type="'.$input_type.'" class="form-control" name="'.$class_name_low.'['.$column.']" value="{{ '.$class_name_low.'.'.$column.' }}">'.PHP_EOL;
            }else{
                $template .= '<textarea id="'.$column.'-input" name="'.$class_name_low.'['.$column.']" cols="30" rows="3" class="form-control">{{'.$class_name_low.'.'.$column.' }}</textarea>'.PHP_EOL;
            }

            $template .= '</div>'.PHP_EOL.'</div>'.PHP_EOL.PHP_EOL;
        }

        $template .= file_get_contents(__DIR__.'/../templates/view/form/2.php');

        $template = str_replace('${class_name_low}', $class_name_low, $template);
        $template = str_replace('${class_name}', $class_name, $template);

        file_put_contents(__DIR__."/../../../app/views/admin/".$class_name_low.'s/'.$class_name_low."-form.twig", $template);
//      ---------------------
        echo PHP_EOL.'Views was successfully generated';
    }

    public function make_role_permissions($params)
    {
        $class_name = $params[0];
        $table_name = $params[2];


        $class_name_low = strtolower($class_name);
        $roles_view = file_get_contents(__DIR__."/../../../app/views/admin/users/user-roles.twig");

        $part_1 = explode('{#POINTER#}', $roles_view)[0];
        $part_2 = explode('{#POINTER#}', $roles_view)[1];

        $template = file_get_contents(__DIR__."/../templates/roles.php");

        $template = str_replace('${class_name_low}', $class_name_low, $template);
        $template = str_replace('${class_name}', $class_name, $template);

        $part_1 .= PHP_EOL.$template;

        $roles_view = $part_1.'{#POINTER#}'.$part_2;

        file_put_contents(__DIR__."/../../../app/views/admin/users/user-roles.twig", $roles_view);
        echo PHP_EOL.'Role permissions list was successfully updated';
    }



    // Создает модель, Контроллер и Роуты
    protected function make($params)
    {
        $this->make_model($params);
        $this->make_controller($params);
        $this->make_routes($params);
        $this->make_views($params);
        $this->make_role_permissions($params);
    }



    // Применяет миграции
    protected function migrate()
    {
        define("MIGRATIONS_PATH", __DIR__."/../../../app/db/migrations");

        if (!Capsule::schema()->hasTable('migrations')) {
            Capsule::schema()->create('migrations', function($table) {
                $table->increments('id');
                $table->string('filename');
                $table->string('class');
                $table->integer('time');
            });
        }

        $migrations = Capsule::table('migrations')->orderBy('id')->select('class')->get()->toArray();

        $files = glob(MIGRATIONS_PATH.'/*.php');

        $migrated_files = [];

        $passed = [];
        foreach ($files as $file) {

            $filename = basename($file, '.php');
            $filename = explode('_', $filename);
            $file_time = intval(array_shift($filename));
            $class = implode('_', $filename) . '_' . $file_time;

            if (in_array($class, $migrations)) {
                array_push($passed, $class);
                continue;
            }

            require_once($file);

            $obj = new $class;
            try {
                $obj->up();
                Capsule::table('migrations')->insert(['filename' => basename($file, '.php'), 'time' => $file_time, 'class' => $class]);
                $migrated_files[] = basename($file, '.php');
            } catch (Exception $e) {
                $text = explode('(', $e->getMessage())[0];
                $text = str_replace('SQLSTATE[42S01]: Base table or view already exists: 1050', '', $text);
                $text = str_replace('SQLSTATE[42S21]: Column already exists: 1060', '', $text);
                echo trim($text).PHP_EOL;
            }

        }

        echo 'Migrate done!'.PHP_EOL;
        echo 'Successful: ' . count($migrated_files).PHP_EOL;

        foreach ($migrated_files as $f) {
            echo $f.PHP_EOL;
        }

        echo 'Passed:' . count($passed);
        foreach ($passed as $class) {
            echo $class.PHP_EOL;
        }
        echo PHP_EOL;
    }



    //Создание шаблона новой миграции в db/migrations
    protected function make_migration($params)
    {
        $migration_name = $params[0];
        $a = explode('_',$migration_name);

        if($a[1] == 'table')
        {
            $table_name = $a[2];
            for ($i = 3; $i <= count($a); $i++)
            {
                if(isset($a[$i]))$table_name .= "_".$a[$i];
            }
            $template = file_get_contents(__DIR__."/../templates/migration_table.php");
            $template = str_replace('${insert}', '', $template);
        }else{
            $table_name = explode('_',$a[3])[0];
            for ($i = 4; $i <= count($a); $i++)
            {
                if(isset($a[$i]))$table_name .= "_".$a[$i];
            }
            $template = file_get_contents(__DIR__."/../templates/migration_col.php");
        }

        $time = date('U');
        
        $template = str_replace('${time}', $time, $template);
        $template = str_replace('${table_name}', $table_name, $template);
        $template = str_replace('${migration_name}', $migration_name, $template);

        file_put_contents(__DIR__.'/../../../app/db/migrations/'.$time.'_'.$migration_name.'.php', $template);
    }



    //Создает шаблон сида
    protected function make_seed($params)
    {
        $class_name = $params[0];


        if (isset($params[1]))
        {
            $table_name = $params[1];

            $info = $this->getTableInfo($table_name);

            if (!Capsule::schema()->hasTable($table_name)) {
                echo "Table <".$table_name."> doesn't exists in database".PHP_EOL;
                
            }

            $insert = PHP_EOL;
            foreach($info as $column => $type)
            {
                $insert.= "\t\t\t"."'".$column."' => \$faker-> ,//".$type.PHP_EOL;
            }
            $insert .= "\t\t\t";

            $template = file_get_contents(__DIR__.'/../templates/seed-table.php');
            $template = str_replace('${table_name}', $table_name, $template);
            $template = str_replace('${insert}', $insert, $template);
        }else{
            $template = file_get_contents(__DIR__.'/../templates/seed.php');
        }

        $template = str_replace('${class_name}', $class_name, $template);

        if(file_exists(__DIR__."/../../../app/db/seeds/".$table_name.".php"))
        {
            $answer = '';
            while (!in_array($answer, ['y','Y','n','N']))
            {
                $answer = readline('This seed already exist. Overwrite? [y,n]');

            }
            if ($answer == 'n') exit;
        }

        file_put_contents(__DIR__."/../../../app/db/seeds/".$class_name.'.php', $template);
    }



    //Заполняет таблицу рандомными значениями
    protected function seed($params)
    {
        if ($params)
        {
            $class_name = $params[0];
            $count = $params[1];
            if(!file_exists(__DIR__."/../../../app/db/seeds/".$class_name.".php"))
            {
                echo "This seed doesn't exist".PHP_EOL;
            }
            require __DIR__."/../../../app/db/seeds/".$class_name.".php";
            $class_name::run($count);
        }
        else{
            $seeds = glob(__DIR__."/../../../app/db/seeds/*.php");
            foreach ($seeds as $seed)
            {
                $class_name = (array_first(explode('.',end(explode('/',$seed)))));
                require __DIR__."/../../../app/db/seeds/".$class_name.".php";
                $class_name::run();
            }
        }
    }


    protected function copy_table($params)
    {
        $table_name = $params[0];
        if (!$table_name)
        {
            echo 'Argumement <table_name> passed';
            exit;
        }

        $rows = Capsule::table($table_name)->get()->toArray();

        $info = self::getTableInfo($table_name);

        $cols = '';

        foreach($info as $column=>$type)
        {
            if ($column == 'id'){$type = 'increments';}
            $cols .= '$table->'.$type.'(\''.$column.'\');'.PHP_EOL."\t"."\t"."\t";
        }



        $temp = "\t"."\t"."\t"."'\${column}' => '\${value}',".PHP_EOL;

        $insert = '';
        foreach ($rows as $row)
        {
            $insert .= "Capsule::table('".$table_name."')->insert([".PHP_EOL;

            foreach ($info as $column=>$type)
            {
                $insert .= str_replace('${column}', $column, $temp);
                $insert = str_replace('${value}', $row->$column, $insert);
            }

            $insert .= "\t"."\t".']);'.PHP_EOL.PHP_EOL."\t"."\t";
        }

        $template = file_get_contents(__DIR__."/../templates/migration_table.php");

        $time = date('U');

        $template = str_replace('${time}', $time, $template);
        $template = str_replace('${table_name}', $table_name, $template);
        $template = str_replace('${migration_name}', 'create_table_'.$table_name, $template);
        $template = str_replace('${insert}', $insert, $template);
        $template = str_replace('${cols}', $cols, $template);

        file_put_contents(__DIR__.'/../../../app/db/migrations/'.$time.'_'.'create_table_'.$table_name.'.php', $template);

    }


    protected function app_version_iterate(){
        $current_version = intval(\App\Helpers\Dev::getDotEnv('APP_VERSION'));
        \App\Helpers\Dev::setDotEnv('APP_VERSION', $current_version+1);
    }

    // Выводит cписок команд
    protected function help()
    {
        echo PHP_EOL."Wtolk Servant [Version v".$this->version."]".PHP_EOL;

        echo "Commands:".PHP_EOL;
        echo PHP_EOL;
        echo " make <Name> from <table>              -->   Create Model, Controller, Routes, using table in DB".PHP_EOL;
        echo " make_model <Name> from <table>        -->   Create Model, using table in DB".PHP_EOL;
        echo " make_controller <Name>                -->   Create Controller".PHP_EOL;
        echo " make_routes <Name>                    -->   Create admin and api routes".PHP_EOL;
        echo PHP_EOL;
        echo " migrate                               -->   Run migrations.".PHP_EOL;
        echo " make_migration <Migration_name>       -->   Create migration template.".PHP_EOL;
        echo " copy_table <table_name>               -->   Create migration form existing table with inserts".PHP_EOL;
        echo PHP_EOL;
        echo " make_seed <class_name> [<table_name>] -->   Create template for seed .".PHP_EOL;
        echo " seed <class_name> <count>             -->   Fill the table random values.".PHP_EOL;
        echo PHP_EOL;
        echo " help                                  -->   Displays the help menu.".PHP_EOL;

        echo PHP_EOL;
    }
}