{% extends "admin/layout/layout.twig" %}

{% block title %}//--//--//{% endblock %}

{%block page_title%}//--//--//{%endblock%}

{%block icons%}
<a href="{{ path_for('${class_name_low}.showAdmin${class_name}List') }}" class="btn btn-link btn-float text-default">
    <i class="icon-file-plus text-primary" style="font-size: 1.4rem"></i>
    <span>все //--//--//</span>
</a>
{%endblock%}

{% block content %}

    <form method="post"
          {% if ${class_name_low}.id %}
            action="{{ path_for('${class_name_low}.update${class_name}', {'id': ${class_name_low}.id}) }}"
          {% else %}
            action="{{ path_for('${class_name_low}.create${class_name}') }}"
          {% endif %}
    >
        <div class="row">

            <div class="col-md-12">

                <div class="card">

                    <div class="card-body">