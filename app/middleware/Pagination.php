<?php

namespace App\Middleware;

use Illuminate\Pagination\Paginator;

class Pagination
{
    public function __invoke($request, $response, $next)
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        Paginator::currentPageResolver(function() use ($page)
        {
            return $page;
        });
        return $response = $next($request, $response);
    }
}