<?php

namespace App\Models;


class NavigationItem extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'navigation_items';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
        'id' => 'integer',
        'navigation_id' => 'integer',
        'title' => 'string',
        'path' => 'string',
        'positin' => 'float',
        'parent_id' => 'integer'
    ];

    public function navigation()
    {
        return $this->hasOne(Navigation::class, 'id', 'navigation_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }

    public static function lastPosition($menu_id)
    {
        return self::where('navigation_id', $menu_id)->orderBy('position', 'desc')->first()->position;
    }
}