<?php

namespace App\Models;

class City extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'cities';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'name' => 'string',
	];

}