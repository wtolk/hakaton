<?php

namespace App\Models;

class Message extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'messages';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'user_id' => 'integer',
		'chat_id' => 'integer',
		'recipient_id' => 'integer',
		'text' => 'text',
		'created_at' => 'datetime',
	];

}