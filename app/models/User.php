<?php

namespace App\Models;

use App\Helpers\FilesTrait;
use Cartalyst\Sentinel\Users\EloquentUser;

class User extends EloquentUser
{
    use FilesTrait;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'company_id' => 'integer',
		'email' => 'string',
		'password' => 'string',
		'permissions' => 'text',
		'last_login' => 'datetime',
		'first_name' => 'string',
		'last_name' => 'string',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];
    protected $fillable = [
        'email',
        'company_id',
        'password',
        'permissions',
        'last_login',
        'first_name',
        'last_name'
    ];

    public function role() {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id', 'role_id');
    }

    public function photo()
    {
        return $this->hasOne(File::class, 'id', 'photo_id');
    }

    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function address()
    {
        return $this->hasOne(Adress::class, 'id', 'address_id');
    }
}