<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends CustomModel
{
    use SoftDeletes;
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
}