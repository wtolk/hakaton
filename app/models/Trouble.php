<?php

namespace App\Models;

class Trouble extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'troubles';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
//    protected $casts = [
//		'id' => 'integer',
//		'name' => 'string',
//	];


    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

}