<?php

namespace App\Models;

class Adress extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'adresses';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}