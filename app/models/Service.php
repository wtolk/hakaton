<?php

namespace App\Models;

class Service extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'services';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
//    protected $casts = [
//		'id' => 'integer',
//		'name' => 'string',
//	];



}