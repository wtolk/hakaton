<?php
namespace App\Controllers;
use App\Models\City;

class CityController extends Controller
{
    public function showAdminCityList($request, $response, $args)
    {
        $this->twig_vars['citys'] = City::paginate(50);
        $this->render('admin/citys/citys-list.twig');
    }

    public function showAdminCityEdit($request, $response, $args)
    {
        $this->twig_vars['city'] = City::find($args['id']);
        $this->render('admin/citys/city-form.twig');
    }

    public function showAdminCityAdd($request, $response, $args)
    {
        $this->render('admin/citys/city-form.twig');
    }

    public function createCity($request, $response, $args)
    {
        $data = $request->getParams();
        City::create($data['city']);
        return $response->withRedirect($this->ci->router->pathFor('city.showAdminCityList'));
    }

    public function updateCity($request, $response, $args)
    {
        $data = $request->getParams();
        City::find($args['id'])->update($data['city']);
        return $response->withRedirect($this->ci->router->pathFor('city.showAdminCityList'));
    }

    public function deleteCity($request, $response, $args)
    {
        City::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('city.showAdminCityList'));
    }

}