<?php


namespace App\Controllers;


use App\Helpers\Config;
use App\Helpers\Parser;
use App\Models\Adress;
use App\Models\Company;
use App\Models\Trouble;
use Slim\Http\Request;
use Slim\Http\Response;

class TroubleController extends Controller
{
    public function createTrouble($request, $response, $args)
    {
        $data = $request->getParams();
        $data['trouble']['user_id'] = Config::getInstance()['user']->id;
        $data['trouble']['company_id'] = Config::getInstance()['user']->address->company_id;
        $data['trouble']['status'] = 0;

        $trouble = Trouble::create($data['trouble']);
        if (!empty($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
            $file = $this->_uploadFiles('photo', '/files', 'trouble', $trouble->id)[0];
            $trouble->file_id = $file['id'];
            $trouble->save();
        }

        return $response->withStatus(301)->withHeader('Location', '/success');
    }
}