<?php

namespace App\Controllers;

use App\Models\NavigationItem;
use App\Models\Navigation;

class NavigationItemController extends Controller
{
    public function showAdminNavigationItemList($request, $response, $args)
    {
        $items = NavigationItem::where('navigation_id', $args['id'])->orderBy('position')->where('parent_id', null)->with('children')->get()->toArray();
        $this->twig_vars['items'] = $items;
        $this->twig_vars['menu'] = Navigation::find($args['id'])->toArray();
        $this->render('admin/navigation/items/item-list.twig');
    }

    public function showAdminNavigationItemAdd($request, $response, $args)
    {
        $this->twig_vars['menu'] = Navigation::find($args['id']);
        $this->twig_vars['messages'] = $this->ci['flash']->getMessages();
        $this->render('admin/navigation/items/item-form.twig');
    }

    public function showAdminNavigationItemEdit($request, $response, $args)
    {
        $this->twig_vars['menu'] = Navigation::find($args['id'])->toArray();
        $this->twig_vars['item'] = NavigationItem::find($args['item_id']);

        $this->render('admin/navigation/items/item-form.twig');
    }

    public function createNavigationItem($request, $response, $args)
    {
        $data = $request->getParams();

        try {
            $this->ci->router->getNamedRoute($data['item']['path']);
        } catch (\Exception $e) {
            $this->ci['flash']->addMessage('wrong', $e->getMessage());
            return $response->withStatus(301)->withHeader('Location', $_SERVER['HTTP_REFERER']);
        }

        $data = $this->convertData($data);
        $data['position'] = round(NavigationItem::lastPosition($args['id']), 0, PHP_ROUND_HALF_DOWN)+1;
        $data['navigation_id'] = $args['id'];
        NavigationItem::create($data);

        return $response->withRedirect($this->ci->router->pathFor('navigation.showAdminNavigationItemList', ['id'=>$args['id']]));
    }

    public function updateNavigationItem($request, $response, $args)
    {
        $data = $request->getParams();

        try {
            $this->ci->router->getNamedRoute($data['item']['path']);
        } catch (\Exception $e) {
            $this->ci['flash']->addMessage('wrong', $e->getMessage());
            return $response->withStatus(301)->withHeader('Location', $_SERVER['HTTP_REFERER']);
        }

        $data = $this->convertData($data);
        NavigationItem::where('navigation_id', $args['id'])->find($args['item_id'])->update($data);
        return $response->withRedirect($this->ci->router->pathFor('navigation.showAdminNavigationItemList', ['id'=>$args['id']]));
    }

    public function deleteNavigationItem($request, $response, $args)
    {
        $item = NavigationItem::with('parent', 'children')->find($args['item_id']);
        NavigationItem::destroy($args['item_id']);

        if ($item->parent) {
            $i = 0.01;
            foreach ($item->parent->children as $child) {
                $child->position = $item->parent->position + $i;
                $child->save();
                $i += 0.01;
            }
        } elseif ($item->children) {
            $last_position = round(NavigationItem::lastPosition($args['id']), 0, PHP_ROUND_HALF_DOWN);
            $i = 1;
            foreach ($item->children as $child) {
                $child->parent_id = null;
                $child->position = $last_position + $i;
                $child->save();
                $i++;
            }
        }

        return $response->withRedirect($this->ci->router->pathFor('navigation.showAdminNavigationItemList', ['id'=>$args['id']]));
    }

    public function getItemPermissions($request, $response, $args) {
        $data = $request->getParams();
        $items = NavigationItem::where('navigation_id', $data['menu_id'])->find($data['id'])->toJson();
        return $items;
    }

    public function convertData($data)
    {
        $data1 = $data['item'];
        unset($data['item']);
        $data1['permissions'] = json_encode($data);
        $data = $data1;
        unset($data1);
        return $data;
    }

    public function saveNavigationItems($request, $resonse, $args)
    {
        $data = $request->getParams();
        foreach($data['items'] as $pos=>$item)
        {
            $i  = NavigationItem::find($item['id']);
            $i->position = $pos+1;
            $i->parent_id = null;
            $i->save();

            if (isset($item['children']))
            {
                foreach($item['children'] as $sub_pos=>$child)
                {
                    $sub_item = NavigationItem::find($child['id']);//+
                    $sub_item->position = ($pos+1).'.0'.($sub_pos+1);//
                    $sub_item->parent_id = $item['id'];
                    $sub_item->save();
                }
            }
        }

        return json_encode($data);
    }
}