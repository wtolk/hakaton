<?php


namespace App\Controllers;


use App\Helpers\Config;
use App\Models\Message;
use App\Models\User;
use Carbon\Carbon;
use Slim\Http\Request;
use Slim\Http\Response;

class ChatController extends Controller
{
    public function showChatList(Request $request, Response $response, array $args)
    {
        $user = Config::getInstance()['user'];
        $messages = Message::where('user_id',$user->id)->orWhere('recipient_id', $user->id)->get()->pluck('user_id','recipient_id');
        $ids = array_merge($messages->values()->toArray(),$messages->keys()->toArray());
        $ids = array_filter($ids, function ($v) use ($user){
            return $v !== $user->id;
        });
        $ids = array_values($ids);
        $interlocutors = User::find($ids);

        $interlocutors->map(function($int) use ($user){
            $int->setAttribute('last_message', Message::where(
                [
                    ['user_id', $int->id],
                    ['recipient_id', $user->id]
                ]
            )->orWhere(
                [
                    ['user_id', $user->id],
                    ['recipient_id', $int->id]
                ]
            )->orderBy('created_at', 'desc')->first()->toArray());
        });


        $this->twig_vars['interlocutors'] = $interlocutors->toArray();

        return $this->render('public/chat/list.twig');
    }

    public function showDialog(Request $request, Response $response, $args)
    {
        $user = Config::getInstance()['user'];
        $interlocutor = User::find($args['user_id']);

        //Сделать прочитанными

        $messages = Message::where(
            [
                ['user_id', $user->id],
                ['recipient_id', $interlocutor->id]
            ]
        )->orWhere( [
            ['user_id', $interlocutor->id],
            ['recipient_id', $user->id]
        ])->get();

        $this->twig_vars['user'] = $user->toJson();
        $this->twig_vars['interlocutor'] = $interlocutor->toJson();
        $this->twig_vars['messages'] = $messages->toJson();

        $this->render('public/chat/dialog.twig');
    }


    public function showPolylog(Request $request, Response $response, array $args)
    {
        $user = Config::getInstance()['user'];
        $chat_id = $user->address->company->id;
        $messages = Message::where('chat_id', $chat_id)->orderBy('created_at', 'asc')->get();
        $this->twig_vars['messages'] = $messages->toJson();
        $this->twig_vars['user'] = $user->toJson();
        $this->twig_vars['chat_id'] = $chat_id;
        $this->render('public/chat/polylog.twig');
    }

    public function createMessage(Request $request, Response $response, array $args)
    {
        $text = $request->getParam('text');

        if ($args['user_id'] != $args['interlocutor_id']) {
            Message::create([
                'user_id' => $args['user_id'],
                'recipient_id' => $args['interlocutor_id'],
                'text' => $text,
                'created_at' => Carbon::now()
            ]);
        }

        return json_encode(Message::all()->last()->id);
    }

    public function createPolylogMessage(Request $request, Response $response, array $args)
    {
        $text = $request->getParam('text');

            $message = Message::create([
                'user_id' => Config::getInstance()['user']->id,
                'chat_id' => $args['chat_id'],
                'text' => $text,
                'created_at' => Carbon::now()
            ]);

        return json_encode($message->id);
    }
}