<?php


namespace App\Controllers;


use App\Helpers\Config;
use App\Models\Adress;
use App\Models\Trouble;
use Slim\Http\Request;
use Slim\Http\Response;

class MainController extends Controller
{
    public function showPublicMainPage(Request $request, Response $response, array $args)
    {
        if ($_SESSION['user']){
            return $response->withRedirect('/main');
        }
        $this->render('public/main.twig');
    }

    public function selector()
    {
        return $this->render('public/selector.twig');
    }

    public function showPayment($request, $response, $args)
    {
        return $this->render('public/payment.twig');
    }

    public function showChooseAddress(Request $request, Response $response, array $args)
    {
        return $this->render('public/choose_address.twig');
    }

    public function chooseAddress(Request $request, Response $response, array $args)
    {
        $data = $request->getParams();
        $user = Config::getInstance()['user'];
        d($user);
        $user->update(
            [
                'address_id' => $data['address_id'],
                'apartments' => $data['apartment']
            ]
        );
        return $response->withStatus(301)->withHeader('Location', $this->ci->router->pathFor('client.showClientMainPage'));
    }


    public function getSameAddresses(Request $request, Response $reponse, array $args)
    {
        $addresses = Adress::where('title', 'like', '%'.$request->getParams()['query'].'%')->take(10)->get();
        return $addresses->toJson();
    }

    public function showCompanyLogin($request, $response, $args)
    {
        return $this->render('public/login-company.twig');
    }

    public function showSuccess($request, $response, $args)
    {
        return $this->render('public/success.twig');
    }

    public function showKvartira($request, $response, $args)
    {
        $this->twig_vars['user'] = Config::getInstance()['user'];
        return $this->render('public/kvartira.twig');
    }

    public function showClientMainPage()
    {
        $this->twig_vars['user'] = Config::getInstance()['user'];
        return $this->render('public/general.twig');
    }

    public function showTroubles($request, $response, $args)
    {
        $this->twig_vars['user'] = Config::getInstance()['user'];
        $this->twig_vars['troubles'] = Trouble::where('user_id', '=', Config::getInstance()['user']->id)->get()->toArray();
        return $this->render('public/troubles.twig');
    }

    public function showTroublePage($request, $response, $args)
    {
        $this->twig_vars['user'] = Config::getInstance()['user'];
        $this->twig_vars['trouble'] = Trouble::find($args['id']);
        return $this->render('public/trouble.twig');
    }
}