<?php
namespace App\Controllers;

use App\Models\Navigation;
use App\Models\NavigationItem;

class NavigationController extends Controller
{
    public function showAdminNavigationList($request, $response, $args)
    {
        $this->twig_vars['menu_list'] = Navigation::paginate(50);
        $this->render('admin/navigation/navigation-list.twig');
    }

    public function showAdminNavigationEdit($request, $response, $args)
    {
        $this->twig_vars['menu'] = Navigation::find($args['id']);
        $this->render('admin/navigation/navigation-form.twig');
    }

    public function showAdminNavigationAdd($request, $response, $args)
    {
        $this->render('admin/navigation/navigation-form.twig');
    }

    public function createNavigation($request, $response, $args)
    {
        $data = $request->getParams();
        Navigation::create($data['menu']);
        return $response->withRedirect($this->ci->router->pathFor('navigation.showAdminNavigationList'));
    }

    public function updateNavigation($request, $response, $args)
    {
        $data = $request->getParams();
        Navigation::find($args['id'])->update($data['menu']);
        return $response->withRedirect($this->ci->router->pathFor('navigation.showAdminNavigationList'));
    }

    public function deleteNavigation($request, $response, $args)
    {
        Navigation::find($args['id'])->items()->delete();
        Navigation::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('navigation.showAdminNavigationList'));
    }

}