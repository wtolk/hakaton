<?php


namespace App\Controllers;


use App\Helpers\Config;
use App\Helpers\Parser;
use App\Models\Adress;
use App\Models\Company;
use App\Models\Trouble;
use App\Models\User;
use Slim\Http\Request;
use Slim\Http\Response;

class CompanyController extends Controller
{
    public function showAdminMyHouses($request, $response, $args)
    {
        $this->twig_vars['houses'] = Adress::where('company_id', '=', Config::getInstance()['user']->company_id)->paginate(200)->toArray();
        $this->render('admin/houses/house-list.twig');
    }

    public function showAdminHouseDetails($request, $response, $args)
    {
        $user_ids = User::where('address_id', '=', $args['id'])->pluck('id');
        $this->twig_vars['troubles'] = Trouble::with('service')->whereIn('user_id', $user_ids)->orderBy('created_at', 'desc')->get()->toArray();
        $this->twig_vars['house'] = Adress::find($args['id']);
        $this->render('admin/houses/house-details.twig');
    }

    public function showAdminMyTariffs($request, $response, $args)
    {
        $this->render('admin/users/tariffs.twig');
    }

    public function showAdminMyMap($request, $response, $args)
    {
        $this->render('admin/houses/map.twig');
    }
}