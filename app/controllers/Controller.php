<?php

namespace App\Controllers;

use App\Helpers\Upload;
use App\Models\File;

class Controller
{
    public $twig_vars;
    protected $ci;

    public function __construct($container)
    {
        $this->ci = $container;
        $this->twig_vars = [];
    }

    public static function dump($data, $comment)
    {
        ob_start();
        print_r($data);
        $text = "\\---------------------------- $comment \r\n";
        $text .= ob_get_contents();
        ob_end_clean();
        file_put_contents(__DIR__ . '/../../var_dump', $text, FILE_APPEND);
    }

    public function render($view)
    {
        $response = $this->ci->get('response');
        try {
            return $this->ci->view->render($response, $view, $this->twig_vars);
        } catch (\Twig_Error_Loader $e) {
            $text = explode('"', $e->getMessage());
            $template = $text[1];
            $this->slim->render('errors/error-not-found-template.html.twig',
                ['mes' => $e->getMessage(), 'template' => $template]);
        }
    }

    protected function _uploadFiles($input_name, $destination = '/files', $entity = null, $entity_id = null)
    {
        return self::__uploadFiles($input_name, $destination, $entity, $entity_id);
    }

    public static function __uploadFiles($input_name, $destination = '/files', $entity = null, $entity_id = null)
    {
        $upload = new Upload($input_name);
        $upload->move_uploaded_to = $_SERVER['DOCUMENT_ROOT'] . $destination;
        $upload_result = $upload->upload();
        $files = [];
        if ($upload_result === true) {
            $files_data = $upload->getUploadedData();
            foreach ($files_data as $data) {
                $file['filename'] = $data['new_name'];
                $file['extension'] = $data['extension'];
                $file['path'] = $data['full_path_new_name'];

                $file['size'] = $data['size'];

                $name = md5($file['filename'] . time()) . '.' . $file['extension'];
                $destination_ = $destination . '/' . substr($name, 0, 2) . '/' . substr($name, 2, 2) . '/' . substr($name, 4, 90);
                $file['md5_filename'] = $name;
                $subfolder = explode('/', $destination_);
                array_pop($subfolder);
                $subfolder = implode('/', $subfolder);
                if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $subfolder)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . $subfolder, 0777, true);
                }
                rename($file['path'], $_SERVER['DOCUMENT_ROOT'] . $destination_);
                $file['path'] = $destination_;

                $file['fileable_id'] = $entity_id;
                $file['fileable_type'] = $entity;

                $object = File::create($file);
                $files[] = $object->toArray();
            }
        }

        return $files;
    }
}