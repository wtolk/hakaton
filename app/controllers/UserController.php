<?php

namespace App\Controllers;

use App\Models\Company;
use App\Models\User;
use App\Models\Role;
use App\Models\File;

class UserController extends Controller
{
    public function showLoginPage($request, $response, $args)
    {
        $user = $this->ci['sentinel']->check();
        if($user && $this->ci['sentinel']->inRole('admin')) {
            return $response->withStatus(301)->withHeader('Location', '/admin');
        } else {
            $messages = $this->ci['flash']->getMessages();

            $this->twig_vars['messages'] = $messages;
            return $this->render('public/users/login.twig');
        }
    }

    public function doLogin($request, $response, $args)
    {
        $data = $request->getParams();
        $credentials = [
            'email'    => $data['login'],
            'password' => $data['password'],
        ];

        $user = $this->ci['sentinel']->authenticate($credentials);

        if ($user) {
            if ($user->role[0]->slug == 'client'){
                return $response->withStatus(301)->withHeader('Location', $this->ci->router->pathFor('client.showChooseAddress'));
            } elseif ($user->role[0]->slug == 'company') {
                return $response->withStatus(301)->withHeader('Location', '/admin/my/houses');
            } else{
                return $response->withStatus(301)->withHeader('Location', '/');
            }
        } else {
            $this->ci['flash']->addMessage('wrong', 'Неверные данные');
            return $response->withStatus(301)->withHeader('Location', '/login');
        }
    }

    public function doLogout($request, $response, $args)
    {
        $this->ci['sentinel']->logout();

        return $response->withStatus(301)->withHeader('Location', '/login');
    }

    public function noAccess($request, $response, $args)
    {
        return $this->render('public/errors/message-no-access.twig');
    }

    public function showAdminUserList($request, $response, $args)
    {
        $users = User::with('role')->get()->toArray();
        $this->twig_vars['users'] = $users;
        return $this->render('admin/users/users-list.twig');
    }

    public function showAdminUserEdit($request, $response, $args)
    {
        $this->twig_vars['roles'] = Role::orderBy('id', 'desc')->get();
        $this->twig_vars['user'] = User::with('role', 'photo')->find($args['id'])->toArray();
        $this->twig_vars['companies'] = Company::all()->toArray();
        return $this->render('admin/users/user-form.twig');
    }

    public function showAdminUserAdd($request, $response, $args)
    {
        $this->twig_vars['roles'] = Role::orderBy('id', 'desc')->get();;
        return $this->render('admin/users/user-form.twig');
    }

    public function createUser($request, $response, $args)
    {
        $data = $request->getParams();

        $r = $data['role'];
        unset($data['role']);

        $user = $this->ci['sentinel']->registerAndActivate($data);
        $role = $this->ci['sentinel']->findRoleBySlug($r);
        $role->users()->attach($user);

        if(!empty($_FILES['photo']) && $_FILES['photo']['size'] > 0)
        {
            $file = $this->_uploadFiles('photo')[0];
            $user->photo_id = $file['id'];
            $user->save();
        }

        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function updateUser($request, $response, $args)
    {
        $data = $request->getParams();
        //Если пароль пустой, то не нужно обновлять информацию о нем
        if (strlen($data['password']) == 0) {
            unset($data['password']);
        }
        $user = $this->ci['sentinel']->findById($args['id']);
        $this->ci['sentinel']->update($user, $data);

        $user = User::with('role')->find($args['id']);

        $prevRole = $user['role'][0]['id'];
        $prevRole = $this->ci['sentinel']->findRoleById($prevRole);
        if (!is_null($prevRole)) {
            $prevRole->users()->detach($user);
        }

        $r = $data['role'];
        $role = $this->ci['sentinel']->findRoleBySlug($r);
        $role->users()->attach($user);

        if(!empty($_FILES['photo']) && $_FILES['photo']['size'] > 0)
        {
            $file = $this->_uploadFiles('photo')[0];
            $user->photo_id = $file['id'];
            $user->save();
        }

        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function deleteUser($request, $response, $args) {
        if ($args['id'] == $_SESSION['user']['id']) {
            throw new \Exception('Вы не можете удалить свою учетную запись<br><a href="/admin/users">Вернуться назад</a>');
        }
        $user = User::find($args['id']);

        if ($user) {
            $user->role()->detach();
            $user->delete();
        }
        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function deleteUserAvatar($request, $response, $args)
    {
        $user = User::find($args['id']);
        $user->photo_id = null;
        File::destroy(User::find($args['id'])->photo->id);
        $user->save();
    }
}