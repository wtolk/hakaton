<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Tracy\Debugger;
use App\Middleware\Pagination;
use Psr7Middlewares\Middleware;
use App\Helpers\TwigCustomExtensions;
use App\Helpers\ModuleLoader;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new \Dotenv\Dotenv(dirname(__DIR__.'/../configs/.env'));
$dotenv->load();

// Eloquent
require __DIR__.'/../configs/config.php';
$capsule = new Capsule;
$capsule->addConnection($db);
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();

// Create app
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

// Get container
$container = $app->getContainer();

// Add View to Container
$container['view'] = function($container) {
    $path = ModuleLoader::getViewsPath();
    array_push($path, __DIR__.'/views');
    $view = new \Slim\Views\Twig($path);

    $view->addExtension(new TwigCustomExtensions($container));

    $view->addExtension(new Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    $twig = $view->getEnvironment();
    $twig->addGlobal("session", $_SESSION);
    $twig->addGlobal("app_version", getenv('APP_VERSION'));
    $twig->addGlobal('websoket_uri', getenv('WEBSOKET_URI'));
    return $view;
};


//Add Sentinel to Container
if (file_exists(__DIR__.'/../configs/config.php')) {
    $container['sentinel'] = function ($container) {
        $sentinel = (new \Cartalyst\Sentinel\Native\Facades\Sentinel())->getSentinel();
        $sentinel->getUserRepository()->setModel(\App\Models\User::class);
        $sentinel->getPersistenceRepository()->setUsersModel(\App\Models\User::class);
        return $sentinel;
    };
}

//Add Flash to Container
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container['emitter'] = function()
{
    $emitter = new \League\Event\Emitter();
    return $emitter;
};

foreach (ModuleLoader::allFilesPath() as $path)
{
    require_once $path;
}


//Config init
$config = \App\Helpers\Config::getInstance($container);



/*INCLUDING MIDDLEWARES*/
// To pagination right work
$app->add( new Pagination());

//Remove the last slash from the URL
$app->add(Middleware::trailingSlash()->redirect(301));
/*---------------------*/

//Tracy Debugger
Debugger::enable(false);

date_default_timezone_set('Asia/Krasnoyarsk');

require __DIR__.'/routes/admin.php';
require __DIR__.'/routes/api.php';
require __DIR__.'/routes/public.php';