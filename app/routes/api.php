<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Registred;
use App\Middleware\Access;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\ModuleController;
use App\controllers\CityController;
use App\Controllers\ParserController;
use App\Controllers\TroubleController;
//delimiter//

$app->get('/login', UserController::class.':showLoginPage')->setName('user.showLoginPage');
$app->post('/api/login', UserController::class.':doLogin')->setName('user.doLogin');
$app->get('/api/logout', UserController::class.':doLogout')->setName('user.doLogout');

$app->group('/api', function() use($app, $c) {

    $app->post('/trouble', TroubleController::class.':createTrouble');

    $app->post('/users', UserController::class . ':createUser')->setName('user.createUser')->add(new Access($c, ['user_create']));
    $app->post('/users/{id}', UserController::class . ':updateUser')->setName('user.updateUser')->add(new Access($c, ['user_update']));
    $app->post('/users/{id}/delete', UserController::class . ':deleteUser')->setName('user.deleteUser')->add(new Access($c, ['user_delete']));

    $app->post('/users/{id}/remove-avatar', UserController::class.':deleteUserAvatar')->setName('user.deleteAvatar')->add(new Access($c, ['user_update']));;

    $app->post('/roles', RoleController::class . ':createRole')->setName('role.createRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/get_role', RoleController::class . ':getRole')->setName('role.getRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/create', RoleController::class . ':createUserRoles')->setName('role.createRole')->add(new Access($c, ['roles_view']));

    $app->post('/navigation/items/save', NavigationItemController::class.':saveNavigationItems')->setName('navigation.saveNavigationItems');
    $app->post('/navigation/get-item-permissions', NavigationItemController::class.':getItemPermissions')->setName('navigation.getItemPermissions');

    $app->post('/navigation', NavigationController::class.':createNavigation')->setName('navigation.createNavigation');
    $app->post('/navigation/{id}', NavigationController::class.':updateNavigation')->setName('navigation.updateNavigation');
    $app->get('/navigation/{id}/delete', NavigationController::class.':deleteNavigation')->setName('navigation.deleteNavigation');

    $app->post('/navigation/{id}/item', NavigationItemController::class.':createNavigationItem')->setName('navigation.createNavigationItem');
    $app->post('/navigation/{id}/item/{item_id}', NavigationItemController::class.':updateNavigationItem')->setName('navigation.updateNavigationItem');
    $app->get('/navigation/{id}/item/{item_id}/delete', NavigationItemController::class.':deleteNavigationItem')->setName('navigation.deleteNavigationItem');

    $app->post('/modules/switch-module', ModuleController::class.':switchModule')->setName('module.switchModule');
    $app->post('/modules/add-module', ModuleController::class.':addModule')->setName('module.addModule');

})->add(new Registred($container));
$app->group('/api', function() use($app, $c) {

    $app->post('/citys', CityController::class.':createCity')->setName('city.createCity')->add(new Access($c, ['citys_create']));
    $app->post('/citys/{id}', CityController::class.':updateCity')->setName('city.updateCity')->add(new Access($c, ['citys_update']));
    $app->get('/citys/{id}/delete', CityController::class.':deleteCity')->setName('city.deleteCity')->add(new Access($c, ['citys_delete']));

})->add(new Registred($container));


$app->get('/api/parser/companies', ParserController::class.':parseCompanies');
