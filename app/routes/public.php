<?php

use App\Controllers\MainController;
use App\Controllers\ChatController;
use App\Controllers\CronController;

$app->get('/', MainController::class.':showPublicMainPage');
$app->get('/payment', MainController::class.':showPayment')->setName('client.showPayment');

//Выбор адреса
$app->get('/choose-address', MainController::class.':showChooseAddress')->setName('client.showChooseAddress');
$app->post('/choose-address', MainController::class.':chooseAddress')->setName('client.chooseAddress');
$app->post('/get-same-addresses', MainController::class.':getSameAddresses')->setName('client.getSameAddresses');

//Главная страница
$app->get('/main', MainController::class.':showClientMainPage')->setName('client.showClientMainPage');

//Чат
$app->get('/chat', ChatController::class.':showChatList')->setName('chat.showChatList');
$app->get('/chat/polylog', ChatController::class.':showPolylog')->setName('chat.showPolylog');
$app->get('/chat/{user_id}', ChatController::class.':showDialog')->setName('chat.showDialog');

$app->post('/message/{user_id}/{interlocutor_id}', ChatController::class.':createMessage');
$app->post('/polylog/{chat_id}', ChatController::class.':createPolylogMessage');

$app->get('/selector', MainController::class.':selector');
$app->get('/company/login', MainController::class.':showCompanyLogin');
$app->get('/success', MainController::class.':showSuccess');
$app->get('/troubles', MainController::class.':showTroubles');
$app->get('/troubles/{id}', MainController::class.':showTroublePage');
$app->get('/kvartira', MainController::class.':showKvartira');

//Проверка по крону
$app->post('/cron/check-troubles', CronController::class.':checkTroubles');