<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\TestController;
use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Access;
use App\Middleware\Registred;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\ModuleController;
use App\Controllers\CityController;
use App\Controllers\CompanyController;
//delimiter//

$c = $app->getContainer();

$app->get('/message/no-access', UserController::class.':noAccess')->setName('error.noAccess');


$app->get('/test', TestController::class.':test');

$app->group('/admin', function() use($app, $c) {

    $app->get('/users', UserController::class . ':showAdminUserList')->setName("user.showAdminUserList")->add(new Access($c, ['user_view']));
    $app->get('/users/add', UserController::class . ':showAdminUserAdd')->setName("user.showAdminUserAdd")->add(new Access($c, ['user_view', 'user_create']));
    $app->get('/users/{id}', UserController::class . ':showAdminUserEdit')->setName('user.showAdminUserEdit')->add(new Access($c, ['user_view', 'user_update']));

    $app->get('/roles', RoleController::class . ':showAdminRoleList')->setName('role.showAdminRoleList')->add(new Access($c, ['roles_view']));

    $app->get('/navigation', NavigationController::class . ':showAdminNavigationList')->setName('navigation.showAdminNavigationList');
    $app->get('/navigation/add', NavigationController::class . ':showAdminNavigationAdd')->setName('navigation.showAdminNavigationAdd');
    $app->get('/navigation/{id}', NavigationController::class . ':showAdminNavigationEdit')->setName('navigation.showAdminNavigationEdit');

    $app->get('/navigation/{id}/items', NavigationItemController::class.':showadminNavigationItemList')->setName('navigation.showAdminNavigationItemList');
    $app->get('/navigation/{id}/items/add', NavigationItemController::class.':showAdminNavigationItemAdd')->setName('navigation.showAdminNavigationItemAdd');
    $app->get('/navigation/{id}/items/{item_id}', NavigationItemController::class.':showAdminNavigationItemEdit')->setName('navigation.showAdminNavigationItemEdit');

    $app->get('/modules', ModuleController::class . ':showAdminModuleList')->setName('module.showAdminModuleList')->add(new Access($c, ['modules_view']));

    $app->get('/cities', CityController::class . ':showAdminCityList')->setName('city.showAdminCityList')->add(new Access($c, ['citys_view']));
    $app->get('/cities/add', CityController::class . ':showAdminCityAdd')->setName('city.showAdminCityAdd')->add(new Access($c, ['citys_create']));
    $app->get('/cities/{id}', CityController::class . ':showAdminCityEdit')->setName('city.showAdminCityEdit')->add(new Access($c, ['citys_update']));

    $app->get('/my/houses', CompanyController::class . ':showAdminMyHouses')->setName('company.showAdminMyHouses')->add(new Access($c, ['house_my']));
    $app->get('/my/houses/{id}', CompanyController::class . ':showAdminHouseDetails')->add(new Access($c, ['house_my']));
    $app->get('/my/tariffs', CompanyController::class . ':showAdminMyTariffs')->setName('company.showAdminMyTariffs')->add(new Access($c, ['house_my']));
    $app->get('/my/map', CompanyController::class . ':showAdminMyMap')->setName('company.showAdminMyMap')->add(new Access($c, ['house_my']));

})->add(new Registred($container));
