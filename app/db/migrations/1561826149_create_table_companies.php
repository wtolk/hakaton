<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_companies_1561826149 {
    public function up() {
        Capsule::schema()->create('companies', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->string('inn');
            $table->string('email');
            $table->string('phones');
            $table->integer('house_count');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
}
