<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class set_roles_1561813335 {
    public function up() {
        $dataset = [
            [
                'slug' => 'client',
                'name' => 'Клиент'
            ],
            [
                'slug' => 'company',
                'name' => 'Управляющая компания'
            ],
            [
                'slug' => 'root',
                'name' => 'Админ'
            ]
        ];

        foreach ($dataset as $data){
            \App\Models\Role::firstOrCreate(['slug'=> $data['slug']], $data);
        }
    }

}