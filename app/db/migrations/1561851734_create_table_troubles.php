<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_troubles_1561851734
{
    public function up()
    {
        Capsule::schema()->create('troubles', function ($table) {
            $table->increments('id');
            $table->string('service_id');
            $table->string('user_id');
            $table->string('comment');
            $table->integer('company_id');
            $table->integer('status');
            $table->integer('file_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->timestamp('called_at')->nullable();
        });
    }
}