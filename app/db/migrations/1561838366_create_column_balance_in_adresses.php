<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_column_balance_in_adresses_1561838366 {
    public function up() {
        Capsule::schema()->table('adresses', function($table) {
            $table->integer('balance')->default(1000);
        });
    }
}
