<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_adresses_1561835154 {
    public function up() {
        Capsule::schema()->create('adresses', function($table) {
            $table->increments('id');
			$table->string('title');
			$table->integer('company_id');
			
        });

        Capsule::table('adresses')->insert([
			'id' => '1',
			'title' => 'jkkjjkj',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2',
			'title' => 'б-р. Солнечный Бульвар, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3',
			'title' => 'пер. Автобусный, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4',
			'title' => 'пер. Водометный, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '5',
			'title' => 'пер. Водометный, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '6',
			'title' => 'пер. Вузовский, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '7',
			'title' => 'пер. Вузовский, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '8',
			'title' => 'пер. Вузовский, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '9',
			'title' => 'пер. Вузовский, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '10',
			'title' => 'пер. Вузовский, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '11',
			'title' => 'пер. Казарменный, д. 2/23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '12',
			'title' => 'пер. Кривоколенный, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '13',
			'title' => 'пер. Кривоколенный, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '14',
			'title' => 'пер. Маяковского, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '15',
			'title' => 'пер. Маяковского, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '16',
			'title' => 'пер. Маяковского, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '17',
			'title' => 'пер. Маяковского, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '18',
			'title' => 'пер. Маяковского, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '19',
			'title' => 'пер. Маяковского, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '20',
			'title' => 'пер. Маяковского, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '21',
			'title' => 'пер. Маяковского, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '22',
			'title' => 'пер. Маяковского, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '23',
			'title' => 'пер. Маяковского, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '24',
			'title' => 'пер. Маяковского, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '25',
			'title' => 'пер. Маяковского, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '26',
			'title' => 'пер. Медицинский, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '27',
			'title' => 'пер. Медицинский, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '28',
			'title' => 'пер. Медицинский, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '29',
			'title' => 'пер. Медицинский, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '30',
			'title' => 'пер. Медицинский, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '31',
			'title' => 'пер. Медицинский, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '32',
			'title' => 'пер. Медицинский, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '33',
			'title' => 'пер. Медицинский, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '34',
			'title' => 'пер. Медицинский, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '35',
			'title' => 'пер. Медицинский, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '36',
			'title' => 'пер. Медицинский, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '37',
			'title' => 'пер. Медицинский, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '38',
			'title' => 'пер. Медицинский, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '39',
			'title' => 'пер. Медицинский, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '40',
			'title' => 'пер. Медицинский, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '41',
			'title' => 'пер. Медицинский, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '42',
			'title' => 'пер. Медицинский, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '43',
			'title' => 'пер. Медицинский, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '44',
			'title' => 'пер. Медицинский, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '45',
			'title' => 'пер. Медицинский, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '46',
			'title' => 'пер. Медицинский, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '47',
			'title' => 'пер. Медицинский, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '48',
			'title' => 'пер. Медицинский, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '49',
			'title' => 'пер. Медицинский, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '50',
			'title' => 'пер. Медицинский, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '51',
			'title' => 'пер. Речной, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '52',
			'title' => 'пер. Речной, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '53',
			'title' => 'пер. Речной, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '54',
			'title' => 'пер. Сибирский, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '55',
			'title' => 'пер. Сибирский, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '56',
			'title' => 'пер. Тихий, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '57',
			'title' => 'пер. Тихий, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '58',
			'title' => 'пер. Тихий, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '59',
			'title' => 'пер. Тихий, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '60',
			'title' => 'пер. Тихий, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '61',
			'title' => 'пер. Тихий, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '62',
			'title' => 'пер. Школьный, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '63',
			'title' => 'пер. Ярцевский, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '64',
			'title' => 'пер. Ярцевский, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '65',
			'title' => 'пр-кт. 60 лет образования СССР, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '66',
			'title' => 'пр-кт. 60 лет образования СССР, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '67',
			'title' => 'пр-кт. 60 лет образования СССР, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '68',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 100',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '69',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 101',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '70',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 102/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '71',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 103А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '72',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 104А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '73',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 105',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '74',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 105А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '75',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 106',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '76',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 106А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '77',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 107',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '78',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 107А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '79',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 108',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '80',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 108А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '81',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 109',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '82',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 111',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '83',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 111А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '84',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 111Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '85',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 113',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '86',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 113А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '87',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 115',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '88',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 116А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '89',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 118',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '90',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 119',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '91',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 119, к. а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '92',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 121',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '93',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 123',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '94',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 124',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '95',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 124 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '96',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 125',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '97',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 127',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '98',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 127А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '99',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 128',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '100',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 129',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '101',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 131',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '102',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 133',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '103',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 135',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '104',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 135А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '105',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 137',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '106',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 139',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '107',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 139А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '108',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 141',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '109',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 147',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '110',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 149',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '111',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 151',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '112',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 155',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '113',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 157',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '114',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 159',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '115',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 161',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '116',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 161А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '117',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 162',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '118',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 164',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '119',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 165А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '120',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 167',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '121',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 167А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '122',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 169',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '123',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 171',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '124',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 172',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '125',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 173',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '126',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 173А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '127',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 175',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '128',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 175А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '129',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 177',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '130',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 177А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '131',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 179',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '132',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 179А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '133',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 181',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '134',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 183',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '135',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 183А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '136',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 184',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '137',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 185А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '138',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 187',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '139',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 191',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '140',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 191А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '141',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 193',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '142',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 195',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '143',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '144',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '145',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '146',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '147',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '148',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '149',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '150',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '151',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '152',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '153',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '154',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 45А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '155',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 47А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '156',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 48А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '157',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '158',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 49А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '159',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '160',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 50А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '161',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '162',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '163',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '164',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 53А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '165',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '166',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 55',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '167',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 55А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '168',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 57',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '169',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 57А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '170',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 57Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '171',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '172',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '173',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '174',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 61',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '175',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 61А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '176',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '177',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 63',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '178',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 63 б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '179',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 63А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '180',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 64',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '181',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 64А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '182',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 65',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '183',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 65Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '184',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '185',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 66А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '186',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 67А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '187',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '188',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 68А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '189',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 69А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '190',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '191',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 71А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '192',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '193',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 72А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '194',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 72Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '195',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 73А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '196',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '197',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 74А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '198',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 74Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '199',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 75',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '200',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 75Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '201',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 76',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '202',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 76А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '203',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 77Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '204',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '205',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 78А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '206',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 79',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '207',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 79А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '208',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '209',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 81',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '210',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 81А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '211',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 82',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '212',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 82А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '213',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 83',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '214',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 84',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '215',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 86',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '216',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 86А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '217',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 88',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '218',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 88А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '219',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 89',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '220',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 90',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '221',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 90А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '222',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 94',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '223',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 94А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '224',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 96А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '225',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 98',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '226',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 99',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '227',
			'title' => 'пр-кт. Комсомольский, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '228',
			'title' => 'пр-кт. Комсомольский, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '229',
			'title' => 'пр-кт. Комсомольский, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '230',
			'title' => 'пр-кт. Комсомольский, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '231',
			'title' => 'пр-кт. Комсомольский, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '232',
			'title' => 'пр-кт. Комсомольский, д. 3В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '233',
			'title' => 'пр-кт. Машиностроителей, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '234',
			'title' => 'пр-кт. Машиностроителей, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '235',
			'title' => 'пр-кт. Машиностроителей, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '236',
			'title' => 'пр-кт. Машиностроителей, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '237',
			'title' => 'пр-кт. Машиностроителей, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '238',
			'title' => 'пр-кт. Машиностроителей, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '239',
			'title' => 'пр-кт. Машиностроителей, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '240',
			'title' => 'пр-кт. Машиностроителей, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '241',
			'title' => 'пр-кт. Машиностроителей, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '242',
			'title' => 'пр-кт. Машиностроителей, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '243',
			'title' => 'пр-кт. Машиностроителей, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '244',
			'title' => 'пр-кт. Машиностроителей, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '245',
			'title' => 'пр-кт. Металлургов, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '246',
			'title' => 'пр-кт. Металлургов, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '247',
			'title' => 'пр-кт. Металлургов, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '248',
			'title' => 'пр-кт. Металлургов, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '249',
			'title' => 'пр-кт. Металлургов, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '250',
			'title' => 'пр-кт. Металлургов, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '251',
			'title' => 'пр-кт. Металлургов, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '252',
			'title' => 'пр-кт. Металлургов, д. 14В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '253',
			'title' => 'пр-кт. Металлургов, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '254',
			'title' => 'пр-кт. Металлургов, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '255',
			'title' => 'пр-кт. Металлургов, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '256',
			'title' => 'пр-кт. Металлургов, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '257',
			'title' => 'пр-кт. Металлургов, д. 1В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '258',
			'title' => 'пр-кт. Металлургов, д. 1Д',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '259',
			'title' => 'пр-кт. Металлургов, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '260',
			'title' => 'пр-кт. Металлургов, д. 20А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '261',
			'title' => 'пр-кт. Металлургов, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '262',
			'title' => 'пр-кт. Металлургов, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '263',
			'title' => 'пр-кт. Металлургов, д. 28Г',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '264',
			'title' => 'пр-кт. Металлургов, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '265',
			'title' => 'пр-кт. Металлургов, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '266',
			'title' => 'пр-кт. Металлургов, д. 30А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '267',
			'title' => 'пр-кт. Металлургов, д. 30Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '268',
			'title' => 'пр-кт. Металлургов, д. 30В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '269',
			'title' => 'пр-кт. Металлургов, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '270',
			'title' => 'пр-кт. Металлургов, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '271',
			'title' => 'пр-кт. Металлургов, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '272',
			'title' => 'пр-кт. Металлургов, д. 35А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '273',
			'title' => 'пр-кт. Металлургов, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '274',
			'title' => 'пр-кт. Металлургов, д. 37А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '275',
			'title' => 'пр-кт. Металлургов, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '276',
			'title' => 'пр-кт. Металлургов, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '277',
			'title' => 'пр-кт. Металлургов, д. 41А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '278',
			'title' => 'пр-кт. Металлургов, д. 41Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '279',
			'title' => 'пр-кт. Металлургов, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '280',
			'title' => 'пр-кт. Металлургов, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '281',
			'title' => 'пр-кт. Металлургов, д. 45А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '282',
			'title' => 'пр-кт. Металлургов, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '283',
			'title' => 'пр-кт. Металлургов, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '284',
			'title' => 'пр-кт. Металлургов, д. 49А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '285',
			'title' => 'пр-кт. Металлургов, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '286',
			'title' => 'пр-кт. Металлургов, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '287',
			'title' => 'пр-кт. Металлургов, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '288',
			'title' => 'пр-кт. Металлургов, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '289',
			'title' => 'пр-кт. Металлургов, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '290',
			'title' => 'пр-кт. Металлургов, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '291',
			'title' => 'пр-кт. Металлургов, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '292',
			'title' => 'пр-кт. Мира, д. 100',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '293',
			'title' => 'пр-кт. Мира, д. 104',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '294',
			'title' => 'пр-кт. Мира, д. 105а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '295',
			'title' => 'пр-кт. Мира, д. 106',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '296',
			'title' => 'пр-кт. Мира, д. 107А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '297',
			'title' => 'пр-кт. Мира, д. 111',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '298',
			'title' => 'пр-кт. Мира, д. 117',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '299',
			'title' => 'пр-кт. Мира, д. 118',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '300',
			'title' => 'пр-кт. Мира, д. 124',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '301',
			'title' => 'пр-кт. Мира, д. 128',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '302',
			'title' => 'пр-кт. Мира, д. 130',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '303',
			'title' => 'пр-кт. Мира, д. 132',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '304',
			'title' => 'пр-кт. Мира, д. 152/1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '305',
			'title' => 'пр-кт. Мира, д. 152/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '306',
			'title' => 'пр-кт. Мира, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '307',
			'title' => 'пр-кт. Мира, д. 22, стр. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '308',
			'title' => 'пр-кт. Мира, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '309',
			'title' => 'пр-кт. Мира, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '310',
			'title' => 'пр-кт. Мира, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '311',
			'title' => 'пр-кт. Мира, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '312',
			'title' => 'пр-кт. Мира, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '313',
			'title' => 'пр-кт. Мира, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '314',
			'title' => 'пр-кт. Мира, д. 65',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '315',
			'title' => 'пр-кт. Мира, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '316',
			'title' => 'пр-кт. Мира, д. 7а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '317',
			'title' => 'пр-кт. Мира, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '318',
			'title' => 'пр-кт. Мира, д. 89',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '319',
			'title' => 'пр-кт. Мира, д. 91а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '320',
			'title' => 'пр-кт. Молодежный, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '321',
			'title' => 'пр-кт. Молодежный, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '322',
			'title' => 'пр-кт. Молодежный, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '323',
			'title' => 'пр-кт. Свободный, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '324',
			'title' => 'пр-кт. Свободный, д. 23а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '325',
			'title' => 'пр-кт. Свободный, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '326',
			'title' => 'пр-кт. Свободный, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '327',
			'title' => 'пр-кт. Свободный, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '328',
			'title' => 'пр-кт. Свободный, д. 29А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '329',
			'title' => 'пр-кт. Свободный, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '330',
			'title' => 'пр-кт. Свободный, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '331',
			'title' => 'пр-кт. Свободный, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '332',
			'title' => 'пр-кт. Свободный, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '333',
			'title' => 'пр-кт. Свободный, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '334',
			'title' => 'пр-кт. Свободный, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '335',
			'title' => 'пр-кт. Свободный, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '336',
			'title' => 'пр-кт. Свободный, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '337',
			'title' => 'пр-кт. Свободный, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '338',
			'title' => 'пр-кт. Свободный, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '339',
			'title' => 'пр-кт. Свободный, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '340',
			'title' => 'пр-кт. Свободный, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '341',
			'title' => 'пр-кт. Свободный, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '342',
			'title' => 'пр-кт. Свободный, д. 53А, к. а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '343',
			'title' => 'пр-кт. Свободный, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '344',
			'title' => 'пр-кт. Свободный, д. 58А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '345',
			'title' => 'пр-кт. Свободный, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '346',
			'title' => 'пр-кт. Свободный, д. 63',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '347',
			'title' => 'пр-кт. Свободный, д. 64',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '348',
			'title' => 'пр-кт. Свободный, д. 65',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '349',
			'title' => 'пр-кт. Свободный, д. 69',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '350',
			'title' => 'пр-кт. Свободный, д. 75Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '351',
			'title' => 'пр-кт. Ульяновский, д. 10/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '352',
			'title' => 'пр-кт. Ульяновский, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '353',
			'title' => 'пр-кт. Ульяновский, д. 10Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '354',
			'title' => 'пр-кт. Ульяновский, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '355',
			'title' => 'пр-кт. Ульяновский, д. 14А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '356',
			'title' => 'пр-кт. Ульяновский, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '357',
			'title' => 'пр-кт. Ульяновский, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '358',
			'title' => 'пр-кт. Ульяновский, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '359',
			'title' => 'пр-кт. Ульяновский, д. 18А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '360',
			'title' => 'пр-кт. Ульяновский, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '361',
			'title' => 'пр-кт. Ульяновский, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '362',
			'title' => 'пр-кт. Ульяновский, д. 22А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '363',
			'title' => 'пр-кт. Ульяновский, д. 22Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '364',
			'title' => 'пр-кт. Ульяновский, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '365',
			'title' => 'пр-кт. Ульяновский, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '366',
			'title' => 'пр-кт. Ульяновский, д. 32А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '367',
			'title' => 'пр-кт. Ульяновский, д. 32В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '368',
			'title' => 'пр-кт. Ульяновский, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '369',
			'title' => 'пр-кт. Ульяновский, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '370',
			'title' => 'пр-кт. Ульяновский, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '371',
			'title' => 'пр-кт. Ульяновский, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '372',
			'title' => 'пр-кт. Ульяновский, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '373',
			'title' => 'проезд. Северный, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '374',
			'title' => 'проезд. Северный, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '375',
			'title' => 'проезд. Северный, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '376',
			'title' => 'проезд. Центральный, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '377',
			'title' => 'проезд. Центральный, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '378',
			'title' => 'проезд. Центральный, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '379',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '380',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '381',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '382',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '383',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '384',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 26А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '385',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '386',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '387',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 28А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '388',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '389',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '390',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 32А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '391',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 32Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '392',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 34А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '393',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '394',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 36А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '395',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '396',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 7Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '397',
			'title' => 'ул. 52 Квартал, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '398',
			'title' => 'ул. 52 Квартал, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '399',
			'title' => 'ул. 52 Квартал, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '400',
			'title' => 'ул. 52 Квартал, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '401',
			'title' => 'ул. 52 Квартал, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '402',
			'title' => 'ул. 52 Квартал, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '403',
			'title' => 'ул. 52 Квартал, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '404',
			'title' => 'ул. 52 Квартал, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '405',
			'title' => 'ул. 52 Квартал, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '406',
			'title' => 'ул. 60 лет Октября, д. 100',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '407',
			'title' => 'ул. 60 лет Октября, д. 102',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '408',
			'title' => 'ул. 60 лет Октября, д. 104',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '409',
			'title' => 'ул. 60 лет Октября, д. 106',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '410',
			'title' => 'ул. 60 лет Октября, д. 108',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '411',
			'title' => 'ул. 60 лет Октября, д. 108А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '412',
			'title' => 'ул. 60 лет Октября, д. 110',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '413',
			'title' => 'ул. 60 лет Октября, д. 112',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '414',
			'title' => 'ул. 60 лет Октября, д. 114',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '415',
			'title' => 'ул. 60 лет Октября, д. 149',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '416',
			'title' => 'ул. 60 лет Октября, д. 159/1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '417',
			'title' => 'ул. 60 лет Октября, д. 159/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '418',
			'title' => 'ул. 60 лет Октября, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '419',
			'title' => 'ул. 60 лет Октября, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '420',
			'title' => 'ул. 60 лет Октября, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '421',
			'title' => 'ул. 60 лет Октября, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '422',
			'title' => 'ул. 60 лет Октября, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '423',
			'title' => 'ул. 60 лет Октября, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '424',
			'title' => 'ул. 60 лет Октября, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '425',
			'title' => 'ул. 60 лет Октября, д. 26А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '426',
			'title' => 'ул. 60 лет Октября, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '427',
			'title' => 'ул. 60 лет Октября, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '428',
			'title' => 'ул. 60 лет Октября, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '429',
			'title' => 'ул. 60 лет Октября, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '430',
			'title' => 'ул. 60 лет Октября, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '431',
			'title' => 'ул. 60 лет Октября, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '432',
			'title' => 'ул. 60 лет Октября, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '433',
			'title' => 'ул. 60 лет Октября, д. 36А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '434',
			'title' => 'ул. 60 лет Октября, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '435',
			'title' => 'ул. 60 лет Октября, д. 38А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '436',
			'title' => 'ул. 60 лет Октября, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '437',
			'title' => 'ул. 60 лет Октября, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '438',
			'title' => 'ул. 60 лет Октября, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '439',
			'title' => 'ул. 60 лет Октября, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '440',
			'title' => 'ул. 60 лет Октября, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '441',
			'title' => 'ул. 60 лет Октября, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '442',
			'title' => 'ул. 60 лет Октября, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '443',
			'title' => 'ул. 60 лет Октября, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '444',
			'title' => 'ул. 60 лет Октября, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '445',
			'title' => 'ул. 60 лет Октября, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '446',
			'title' => 'ул. 60 лет Октября, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '447',
			'title' => 'ул. 60 лет Октября, д. 56А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '448',
			'title' => 'ул. 60 лет Октября, д. 57',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '449',
			'title' => 'ул. 60 лет Октября, д. 59',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '450',
			'title' => 'ул. 60 лет Октября, д. 59А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '451',
			'title' => 'ул. 60 лет Октября, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '452',
			'title' => 'ул. 60 лет Октября, д. 61',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '453',
			'title' => 'ул. 60 лет Октября, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '454',
			'title' => 'ул. 60 лет Октября, д. 67',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '455',
			'title' => 'ул. 60 лет Октября, д. 71',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '456',
			'title' => 'ул. 60 лет Октября, д. 75',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '457',
			'title' => 'ул. 60 лет Октября, д. 77',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '458',
			'title' => 'ул. 60 лет Октября, д. 79',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '459',
			'title' => 'ул. 60 лет Октября, д. 82',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '460',
			'title' => 'ул. 60 лет Октября, д. 83',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '461',
			'title' => 'ул. 60 лет Октября, д. 84',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '462',
			'title' => 'ул. 60 лет Октября, д. 85',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '463',
			'title' => 'ул. 60 лет Октября, д. 87',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '464',
			'title' => 'ул. 60 лет Октября, д. 89',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '465',
			'title' => 'ул. 60 лет Октября, д. 91',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '466',
			'title' => 'ул. 60 лет Октября, д. 93',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '467',
			'title' => 'ул. 60 лет Октября, д. 98',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '468',
			'title' => 'ул. 7-я Полярная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '469',
			'title' => 'ул. 78 Добровольческой бригады, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '470',
			'title' => 'ул. 78 Добровольческой бригады, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '471',
			'title' => 'ул. 8 Марта, д. 18г',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '472',
			'title' => 'ул. 8 Марта, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '473',
			'title' => 'ул. 8 Марта, д. 24а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '474',
			'title' => 'ул. 8 Марта, д. 24в',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '475',
			'title' => 'ул. 8 Марта, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '476',
			'title' => 'ул. 8 Марта, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '477',
			'title' => 'ул. 9 Мая, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '478',
			'title' => 'ул. 9 Мая, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '479',
			'title' => 'ул. 9 Мая, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '480',
			'title' => 'ул. 9 Мая, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '481',
			'title' => 'ул. 9 Мая, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '482',
			'title' => 'ул. 9 Мая, д. 31А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '483',
			'title' => 'ул. 9 Мая, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '484',
			'title' => 'ул. 9 Мая, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '485',
			'title' => 'ул. 9 Мая, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '486',
			'title' => 'ул. 9 Мая, д. 42Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '487',
			'title' => 'ул. 9 Мая, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '488',
			'title' => 'ул. 9 Мая, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '489',
			'title' => 'ул. 9 Мая, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '490',
			'title' => 'ул. 9 Января, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '491',
			'title' => 'ул. Ады Лебедевой, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '492',
			'title' => 'ул. Ады Лебедевой, д. 147А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '493',
			'title' => 'ул. Ады Лебедевой, д. 149',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '494',
			'title' => 'ул. Ады Лебедевой, д. 150',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '495',
			'title' => 'ул. Ады Лебедевой, д. 91',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '496',
			'title' => 'ул. Айвазовского, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '497',
			'title' => 'ул. Академгородок, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '498',
			'title' => 'ул. Академгородок, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '499',
			'title' => 'ул. Академгородок, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '500',
			'title' => 'ул. Академгородок, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '501',
			'title' => 'ул. Академгородок, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '502',
			'title' => 'ул. Академгородок, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '503',
			'title' => 'ул. Академика Киренского, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '504',
			'title' => 'ул. Академика Киренского, д. 116',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '505',
			'title' => 'ул. Академика Киренского, д. 118',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '506',
			'title' => 'ул. Академика Киренского, д. 122',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '507',
			'title' => 'ул. Академика Киренского, д. 126',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '508',
			'title' => 'ул. Академика Киренского, д. 126 А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '509',
			'title' => 'ул. Академика Киренского, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '510',
			'title' => 'ул. Академика Киренского, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '511',
			'title' => 'ул. Академика Киренского, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '512',
			'title' => 'ул. Академика Киренского, д. 25Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '513',
			'title' => 'ул. Академика Киренского, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '514',
			'title' => 'ул. Академика Киренского, д. 27А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '515',
			'title' => 'ул. Академика Киренского, д. 27Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '516',
			'title' => 'ул. Академика Киренского, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '517',
			'title' => 'ул. Академика Киренского, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '518',
			'title' => 'ул. Академика Киренского, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '519',
			'title' => 'ул. Академика Киренского, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '520',
			'title' => 'ул. Академика Киренского, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '521',
			'title' => 'ул. Академика Киренского, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '522',
			'title' => 'ул. Академика Киренского, д. 64',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '523',
			'title' => 'ул. Академика Киренского, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '524',
			'title' => 'ул. Академика Киренского, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '525',
			'title' => 'ул. Академика Павлова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '526',
			'title' => 'ул. Академика Павлова, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '527',
			'title' => 'ул. Академика Павлова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '528',
			'title' => 'ул. Академика Павлова, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '529',
			'title' => 'ул. Академика Павлова, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '530',
			'title' => 'ул. Академика Павлова, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '531',
			'title' => 'ул. Академика Павлова, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '532',
			'title' => 'ул. Академика Павлова, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '533',
			'title' => 'ул. Академика Павлова, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '534',
			'title' => 'ул. Академика Павлова, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '535',
			'title' => 'ул. Академика Павлова, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '536',
			'title' => 'ул. Академика Павлова, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '537',
			'title' => 'ул. Академика Павлова, д. 37А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '538',
			'title' => 'ул. Академика Павлова, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '539',
			'title' => 'ул. Академика Павлова, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '540',
			'title' => 'ул. Академика Павлова, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '541',
			'title' => 'ул. Академика Павлова, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '542',
			'title' => 'ул. Академика Павлова, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '543',
			'title' => 'ул. Академика Павлова, д. 47 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '544',
			'title' => 'ул. Академика Павлова, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '545',
			'title' => 'ул. Академика Павлова, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '546',
			'title' => 'ул. Академика Павлова, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '547',
			'title' => 'ул. Академика Павлова, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '548',
			'title' => 'ул. Академика Павлова, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '549',
			'title' => 'ул. Академика Павлова, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '550',
			'title' => 'ул. Академика Павлова, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '551',
			'title' => 'ул. Академика Павлова, д. 59',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '552',
			'title' => 'ул. Академика Павлова, д. 59 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '553',
			'title' => 'ул. Академика Павлова, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '554',
			'title' => 'ул. Академика Павлова, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '555',
			'title' => 'ул. Академика Павлова, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '556',
			'title' => 'ул. Академика Павлова, д. 63',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '557',
			'title' => 'ул. Академика Павлова, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '558',
			'title' => 'ул. Академика Павлова, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '559',
			'title' => 'ул. Академика Павлова, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '560',
			'title' => 'ул. Академика Павлова, д. 77',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '561',
			'title' => 'ул. Академика Павлова, д. 81',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '562',
			'title' => 'ул. Академика Павлова, д. 82',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '563',
			'title' => 'ул. Александра Матросова, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '564',
			'title' => 'ул. Александра Матросова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '565',
			'title' => 'ул. Александра Матросова, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '566',
			'title' => 'ул. Александра Матросова, д. 10Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '567',
			'title' => 'ул. Александра Матросова, д. 10В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '568',
			'title' => 'ул. Александра Матросова, д. 10Г',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '569',
			'title' => 'ул. Александра Матросова, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '570',
			'title' => 'ул. Александра Матросова, д. 12А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '571',
			'title' => 'ул. Александра Матросова, д. 12Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '572',
			'title' => 'ул. Александра Матросова, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '573',
			'title' => 'ул. Александра Матросова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '574',
			'title' => 'ул. Александра Матросова, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '575',
			'title' => 'ул. Александра Матросова, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '576',
			'title' => 'ул. Александра Матросова, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '577',
			'title' => 'ул. Александра Матросова, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '578',
			'title' => 'ул. Александра Матросова, д. 30 корпус 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '579',
			'title' => 'ул. Александра Матросова, д. 30, стр. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '580',
			'title' => 'ул. Александра Матросова, д. 30/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '581',
			'title' => 'ул. Александра Матросова, д. 30/5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '582',
			'title' => 'ул. Александра Матросова, д. 30/6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '583',
			'title' => 'ул. Александра Матросова, д. 30/7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '584',
			'title' => 'ул. Александра Матросова, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '585',
			'title' => 'ул. Александра Матросова, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '586',
			'title' => 'ул. Александра Матросова, д. 5Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '587',
			'title' => 'ул. Александра Матросова, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '588',
			'title' => 'ул. Александра Матросова, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '589',
			'title' => 'ул. Александра Матросова, д. 9Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '590',
			'title' => 'ул. Алеши Тимошенкова, д. 115',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '591',
			'title' => 'ул. Алеши Тимошенкова, д. 129',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '592',
			'title' => 'ул. Алеши Тимошенкова, д. 131',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '593',
			'title' => 'ул. Алеши Тимошенкова, д. 153',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '594',
			'title' => 'ул. Алеши Тимошенкова, д. 157',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '595',
			'title' => 'ул. Алеши Тимошенкова, д. 159',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '596',
			'title' => 'ул. Алеши Тимошенкова, д. 163',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '597',
			'title' => 'ул. Алеши Тимошенкова, д. 169',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '598',
			'title' => 'ул. Алеши Тимошенкова, д. 171',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '599',
			'title' => 'ул. Алеши Тимошенкова, д. 183',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '600',
			'title' => 'ул. Алеши Тимошенкова, д. 185',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '601',
			'title' => 'ул. Алеши Тимошенкова, д. 189',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '602',
			'title' => 'ул. Алеши Тимошенкова, д. 191',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '603',
			'title' => 'ул. Алеши Тимошенкова, д. 193',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '604',
			'title' => 'ул. Алеши Тимошенкова, д. 195',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '605',
			'title' => 'ул. Алеши Тимошенкова, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '606',
			'title' => 'ул. Алеши Тимошенкова, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '607',
			'title' => 'ул. Алеши Тимошенкова, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '608',
			'title' => 'ул. Алеши Тимошенкова, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '609',
			'title' => 'ул. Алеши Тимошенкова, д. 74А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '610',
			'title' => 'ул. Алеши Тимошенкова, д. 76',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '611',
			'title' => 'ул. Алеши Тимошенкова, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '612',
			'title' => 'ул. Алеши Тимошенкова, д. 78А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '613',
			'title' => 'ул. Алеши Тимошенкова, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '614',
			'title' => 'ул. Амурская, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '615',
			'title' => 'ул. Амурская, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '616',
			'title' => 'ул. Амурская, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '617',
			'title' => 'ул. Амурская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '618',
			'title' => 'ул. Амурская, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '619',
			'title' => 'ул. Амурская, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '620',
			'title' => 'ул. Амурская, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '621',
			'title' => 'ул. Амурская, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '622',
			'title' => 'ул. Амурская, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '623',
			'title' => 'ул. Амурская, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '624',
			'title' => 'ул. Анатолия Гладкова, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '625',
			'title' => 'ул. Анатолия Гладкова, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '626',
			'title' => 'ул. Анатолия Гладкова, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '627',
			'title' => 'ул. Анатолия Гладкова, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '628',
			'title' => 'ул. Анатолия Гладкова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '629',
			'title' => 'ул. Анатолия Гладкова, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '630',
			'title' => 'ул. Анатолия Гладкова, д. 17А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '631',
			'title' => 'ул. Анатолия Гладкова, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '632',
			'title' => 'ул. Анатолия Гладкова, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '633',
			'title' => 'ул. Анатолия Гладкова, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '634',
			'title' => 'ул. Анатолия Гладкова, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '635',
			'title' => 'ул. Анатолия Гладкова, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '636',
			'title' => 'ул. Анатолия Гладкова, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '637',
			'title' => 'ул. Анатолия Гладкова, д. 25А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '638',
			'title' => 'ул. Анатолия Гладкова, д. 25Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '639',
			'title' => 'ул. Анатолия Гладкова, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '640',
			'title' => 'ул. Анатолия Гладкова, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '641',
			'title' => 'ул. Анатолия Гладкова, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '642',
			'title' => 'ул. Анатолия Гладкова, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '643',
			'title' => 'ул. Анатолия Гладкова, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '644',
			'title' => 'ул. Анатолия Гладкова, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '645',
			'title' => 'ул. Ангарская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '646',
			'title' => 'ул. Аральская, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '647',
			'title' => 'ул. Аральская, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '648',
			'title' => 'ул. Армейская, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '649',
			'title' => 'ул. Армейская, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '650',
			'title' => 'ул. Армейская, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '651',
			'title' => 'ул. Армейская, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '652',
			'title' => 'ул. Астраханская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '653',
			'title' => 'ул. Астраханская, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '654',
			'title' => 'ул. Астраханская, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '655',
			'title' => 'ул. Астраханская, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '656',
			'title' => 'ул. Астраханская, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '657',
			'title' => 'ул. Астраханская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '658',
			'title' => 'ул. Астраханская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '659',
			'title' => 'ул. Астраханская, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '660',
			'title' => 'ул. Астраханская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '661',
			'title' => 'ул. Аэровокзальная, д. 2Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '662',
			'title' => 'ул. Аэровокзальная, д. 2В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '663',
			'title' => 'ул. Аэровокзальная, д. 2Д',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '664',
			'title' => 'ул. Аэровокзальная, д. 2К',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '665',
			'title' => 'ул. Аэровокзальная, д. 4Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '666',
			'title' => 'ул. Аэровокзальная, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '667',
			'title' => 'ул. Аэровокзальная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '668',
			'title' => 'ул. Аэровокзальная, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '669',
			'title' => 'ул. Аэровокзальная, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '670',
			'title' => 'ул. Аэровокзальная, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '671',
			'title' => 'ул. Аэровокзальная, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '672',
			'title' => 'ул. Аэровокзальная, д. 8В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '673',
			'title' => 'ул. Аэровокзальная, д. 8Д',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '674',
			'title' => 'ул. Аэровокзальная, д. 8Е',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '675',
			'title' => 'ул. Баумана, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '676',
			'title' => 'ул. Баумана, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '677',
			'title' => 'ул. Баумана, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '678',
			'title' => 'ул. Бебеля, д. 61',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '679',
			'title' => 'ул. Бебеля, д. 61А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '680',
			'title' => 'ул. Бебеля, д. 61Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '681',
			'title' => 'ул. Бебеля, д. 63',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '682',
			'title' => 'ул. Белинского, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '683',
			'title' => 'ул. Белинского, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '684',
			'title' => 'ул. Белопольского, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '685',
			'title' => 'ул. Бийская, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '686',
			'title' => 'ул. Бограда, д. 101',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '687',
			'title' => 'ул. Бограда, д. 108',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '688',
			'title' => 'ул. Бограда, д. 114',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '689',
			'title' => 'ул. Бограда, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '690',
			'title' => 'ул. Бограда, д. 65',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '691',
			'title' => 'ул. Бограда, д. 85',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '692',
			'title' => 'ул. Бограда, д. 87',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '693',
			'title' => 'ул. Бограда, д. 95',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '694',
			'title' => 'ул. Бограда, д. 97',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '695',
			'title' => 'ул. Борьбы, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '696',
			'title' => 'ул. Вейнбаума, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '697',
			'title' => 'ул. Весны, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '698',
			'title' => 'ул. Весны, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '699',
			'title' => 'ул. Весны, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '700',
			'title' => 'ул. Взлетная, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '701',
			'title' => 'ул. Взлетная, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '702',
			'title' => 'ул. Взлетная, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '703',
			'title' => 'ул. Взлетная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '704',
			'title' => 'ул. Взлетная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '705',
			'title' => 'ул. Водянникова, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '706',
			'title' => 'ул. Вокзальная, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '707',
			'title' => 'ул. Вокзальная, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '708',
			'title' => 'ул. Вокзальная, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '709',
			'title' => 'ул. Вокзальная, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '710',
			'title' => 'ул. Волгоградская, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '711',
			'title' => 'ул. Волгоградская, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '712',
			'title' => 'ул. Волгоградская, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '713',
			'title' => 'ул. Волгоградская, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '714',
			'title' => 'ул. Волгоградская, д. 15А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '715',
			'title' => 'ул. Волгоградская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '716',
			'title' => 'ул. Волгоградская, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '717',
			'title' => 'ул. Волгоградская, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '718',
			'title' => 'ул. Волгоградская, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '719',
			'title' => 'ул. Волгоградская, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '720',
			'title' => 'ул. Волгоградская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '721',
			'title' => 'ул. Волгоградская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '722',
			'title' => 'ул. Волгоградская, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '723',
			'title' => 'ул. Волгоградская, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '724',
			'title' => 'ул. Волгоградская, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '725',
			'title' => 'ул. Волгоградская, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '726',
			'title' => 'ул. Волгоградская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '727',
			'title' => 'ул. Волгоградская, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '728',
			'title' => 'ул. Волгоградская, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '729',
			'title' => 'ул. Волгоградская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '730',
			'title' => 'ул. Волгоградская, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '731',
			'title' => 'ул. Волжская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '732',
			'title' => 'ул. Волжская, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '733',
			'title' => 'ул. Волжская, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '734',
			'title' => 'ул. Волжская, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '735',
			'title' => 'ул. Волжская, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '736',
			'title' => 'ул. Волжская, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '737',
			'title' => 'ул. Волжская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '738',
			'title' => 'ул. Волжская, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '739',
			'title' => 'ул. Волжская, д. 55',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '740',
			'title' => 'ул. Волжская, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '741',
			'title' => 'ул. Волжская, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '742',
			'title' => 'ул. Волжская, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '743',
			'title' => 'ул. Волочаевская, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '744',
			'title' => 'ул. Воронова, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '745',
			'title' => 'ул. Воронова, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '746',
			'title' => 'ул. Воронова, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '747',
			'title' => 'ул. Воронова, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '748',
			'title' => 'ул. Воронова, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '749',
			'title' => 'ул. Воронова, д. 14/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '750',
			'title' => 'ул. Воронова, д. 14/3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '751',
			'title' => 'ул. Воронова, д. 14/4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '752',
			'title' => 'ул. Воронова, д. 14/5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '753',
			'title' => 'ул. Воронова, д. 14/6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '754',
			'title' => 'ул. Воронова, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '755',
			'title' => 'ул. Воронова, д. 15а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '756',
			'title' => 'ул. Воронова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '757',
			'title' => 'ул. Воронова, д. 16Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '758',
			'title' => 'ул. Воронова, д. 16В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '759',
			'title' => 'ул. Воронова, д. 16Г',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '760',
			'title' => 'ул. Воронова, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '761',
			'title' => 'ул. Воронова, д. 17А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '762',
			'title' => 'ул. Воронова, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '763',
			'title' => 'ул. Воронова, д. 18В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '764',
			'title' => 'ул. Воронова, д. 18Д',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '765',
			'title' => 'ул. Воронова, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '766',
			'title' => 'ул. Воронова, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '767',
			'title' => 'ул. Воронова, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '768',
			'title' => 'ул. Воронова, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '769',
			'title' => 'ул. Воронова, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '770',
			'title' => 'ул. Воронова, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '771',
			'title' => 'ул. Воронова, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '772',
			'title' => 'ул. Воронова, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '773',
			'title' => 'ул. Воронова, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '774',
			'title' => 'ул. Воронова, д. 37А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '775',
			'title' => 'ул. Воронова, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '776',
			'title' => 'ул. Воронова, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '777',
			'title' => 'ул. Воронова, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '778',
			'title' => 'ул. Воронова, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '779',
			'title' => 'ул. Воронова, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '780',
			'title' => 'ул. Воронова, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '781',
			'title' => 'ул. Воронова, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '782',
			'title' => 'ул. Воронова, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '783',
			'title' => 'ул. Высотная, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '784',
			'title' => 'ул. Высотная, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '785',
			'title' => 'ул. Высотная, д. 21Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '786',
			'title' => 'ул. Высотная, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '787',
			'title' => 'ул. Высотная, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '788',
			'title' => 'ул. Высотная, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '789',
			'title' => 'ул. Гастелло, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '790',
			'title' => 'ул. Гастелло, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '791',
			'title' => 'ул. Гастелло, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '792',
			'title' => 'ул. Гастелло, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '793',
			'title' => 'ул. Гастелло, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '794',
			'title' => 'ул. Гастелло, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '795',
			'title' => 'ул. Гастелло, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '796',
			'title' => 'ул. Гастелло, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '797',
			'title' => 'ул. Гастелло, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '798',
			'title' => 'ул. Гастелло, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '799',
			'title' => 'ул. Гастелло, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '800',
			'title' => 'ул. Глинки, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '801',
			'title' => 'ул. Горького, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '802',
			'title' => 'ул. Горького, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '803',
			'title' => 'ул. Горького, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '804',
			'title' => 'ул. Горького, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '805',
			'title' => 'ул. Горького, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '806',
			'title' => 'ул. Горького, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '807',
			'title' => 'ул. Горького, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '808',
			'title' => 'ул. Горького, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '809',
			'title' => 'ул. Горького, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '810',
			'title' => 'ул. Горького, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '811',
			'title' => 'ул. Горького, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '812',
			'title' => 'ул. Грунтовая, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '813',
			'title' => 'ул. Грунтовая, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '814',
			'title' => 'ул. Грунтовая, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '815',
			'title' => 'ул. Даурская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '816',
			'title' => 'ул. Декабристов, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '817',
			'title' => 'ул. Декабристов, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '818',
			'title' => 'ул. Декабристов, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '819',
			'title' => 'ул. Декабристов, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '820',
			'title' => 'ул. Декабристов, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '821',
			'title' => 'ул. Демьяна Бедного, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '822',
			'title' => 'ул. Джамбульская, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '823',
			'title' => 'ул. Джамбульская, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '824',
			'title' => 'ул. Джамбульская, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '825',
			'title' => 'ул. Джамбульская, д. 19Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '826',
			'title' => 'ул. Джамбульская, д. 19В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '827',
			'title' => 'ул. Джамбульская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '828',
			'title' => 'ул. Джамбульская, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '829',
			'title' => 'ул. Джамбульская, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '830',
			'title' => 'ул. Джамбульская, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '831',
			'title' => 'ул. Джамбульская, д. 23А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '832',
			'title' => 'ул. Джамбульская, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '833',
			'title' => 'ул. Джамбульская, д. 2Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '834',
			'title' => 'ул. Джамбульская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '835',
			'title' => 'ул. Джамбульская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '836',
			'title' => 'ул. Джамбульская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '837',
			'title' => 'ул. Джамбульская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '838',
			'title' => 'ул. Диктатуры пролетариата, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '839',
			'title' => 'ул. Диктатуры пролетариата, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '840',
			'title' => 'ул. Диктатуры пролетариата, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '841',
			'title' => 'ул. Диктатуры пролетариата, д. 32А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '842',
			'title' => 'ул. Диктатуры пролетариата, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '843',
			'title' => 'ул. Диктатуры пролетариата, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '844',
			'title' => 'ул. Диктатуры пролетариата, д. 40А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '845',
			'title' => 'ул. Диктатуры пролетариата, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '846',
			'title' => 'ул. Диктатуры пролетариата, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '847',
			'title' => 'ул. Добролюбова, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '848',
			'title' => 'ул. Дорожная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '849',
			'title' => 'ул. Дорожная, д. 6б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '850',
			'title' => 'ул. Дубенского, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '851',
			'title' => 'ул. Дубровинского, д. 104',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '852',
			'title' => 'ул. Дубровинского, д. 106',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '853',
			'title' => 'ул. Дубровинского, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '854',
			'title' => 'ул. Дубровинского, д. 52А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '855',
			'title' => 'ул. Дубровинского, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '856',
			'title' => 'ул. Дубровинского, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '857',
			'title' => 'ул. Дубровинского, д. 76',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '858',
			'title' => 'ул. Дубровинского, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '859',
			'title' => 'ул. Дубровинского, д. 78А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '860',
			'title' => 'ул. Железнодорожников, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '861',
			'title' => 'ул. Железнодорожников, д. 14а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '862',
			'title' => 'ул. Железнодорожников, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '863',
			'title' => 'ул. Железнодорожников, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '864',
			'title' => 'ул. Железнодорожников, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '865',
			'title' => 'ул. Железнодорожников, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '866',
			'title' => 'ул. Железнодорожников, д. 20а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '867',
			'title' => 'ул. Железнодорожников, д. 20б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '868',
			'title' => 'ул. Железнодорожников, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '869',
			'title' => 'ул. Железнодорожников, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '870',
			'title' => 'ул. Железнодорожников, д. 24а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '871',
			'title' => 'ул. Железнодорожников, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '872',
			'title' => 'ул. Железнодорожников, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '873',
			'title' => 'ул. Железнодорожников, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '874',
			'title' => 'ул. Заводская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '875',
			'title' => 'ул. Заводская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '876',
			'title' => 'ул. Заводская, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '877',
			'title' => 'ул. Западная, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '878',
			'title' => 'ул. Западная, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '879',
			'title' => 'ул. Западная, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '880',
			'title' => 'ул. Западная, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '881',
			'title' => 'ул. Западная, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '882',
			'title' => 'ул. Западная, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '883',
			'title' => 'ул. Западная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '884',
			'title' => 'ул. Западная, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '885',
			'title' => 'ул. Западная, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '886',
			'title' => 'ул. Затонская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '887',
			'title' => 'ул. Затонская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '888',
			'title' => 'ул. Затонская, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '889',
			'title' => 'ул. Затонская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '890',
			'title' => 'ул. Затонская, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '891',
			'title' => 'ул. Затонская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '892',
			'title' => 'ул. Затонская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '893',
			'title' => 'ул. Затонская, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '894',
			'title' => 'ул. Затонская, д. 7Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '895',
			'title' => 'ул. Затонская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '896',
			'title' => 'ул. Измайлова, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '897',
			'title' => 'ул. Измайлова, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '898',
			'title' => 'ул. им А.С.Попова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '899',
			'title' => 'ул. им А.С.Попова, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '900',
			'title' => 'ул. им А.С.Попова, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '901',
			'title' => 'ул. им А.С.Попова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '902',
			'title' => 'ул. им А.С.Попова, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '903',
			'title' => 'ул. им А.С.Попова, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '904',
			'title' => 'ул. им А.С.Попова, д. 8В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '905',
			'title' => 'ул. им Академика Вавилова, д. 100',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '906',
			'title' => 'ул. им Академика Вавилова, д. 102',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '907',
			'title' => 'ул. им Академика Вавилова, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '908',
			'title' => 'ул. им Академика Вавилова, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '909',
			'title' => 'ул. им Академика Вавилова, д. 23 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '910',
			'title' => 'ул. им Академика Вавилова, д. 25 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '911',
			'title' => 'ул. им Академика Вавилова, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '912',
			'title' => 'ул. им Академика Вавилова, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '913',
			'title' => 'ул. им Академика Вавилова, д. 37 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '914',
			'title' => 'ул. им Академика Вавилова, д. 39, к. а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '915',
			'title' => 'ул. им Академика Вавилова, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '916',
			'title' => 'ул. им Академика Вавилова, д. 43 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '917',
			'title' => 'ул. им Академика Вавилова, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '918',
			'title' => 'ул. им Академика Вавилова, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '919',
			'title' => 'ул. им Академика Вавилова, д. 47 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '920',
			'title' => 'ул. им Академика Вавилова, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '921',
			'title' => 'ул. им Академика Вавилова, д. 49 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '922',
			'title' => 'ул. им Академика Вавилова, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '923',
			'title' => 'ул. им Академика Вавилова, д. 52 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '924',
			'title' => 'ул. им Академика Вавилова, д. 52 б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '925',
			'title' => 'ул. им Академика Вавилова, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '926',
			'title' => 'ул. им Академика Вавилова, д. 54 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '927',
			'title' => 'ул. им Академика Вавилова, д. 54 б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '928',
			'title' => 'ул. им Академика Вавилова, д. 55',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '929',
			'title' => 'ул. им Академика Вавилова, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '930',
			'title' => 'ул. им Академика Вавилова, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '931',
			'title' => 'ул. им Академика Вавилова, д. 86 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '932',
			'title' => 'ул. им Академика Вавилова, д. 96',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '933',
			'title' => 'ул. им Академика Вавилова, д. 96 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '934',
			'title' => 'ул. им Академика Вавилова, д. 98',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '935',
			'title' => 'ул. им Академика Вавилова, д. 98 а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '936',
			'title' => 'ул. им Б.З.Шумяцкого, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '937',
			'title' => 'ул. им Б.З.Шумяцкого, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '938',
			'title' => 'ул. им Б.З.Шумяцкого, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '939',
			'title' => 'ул. им В.М.Комарова, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '940',
			'title' => 'ул. им газеты Пионерская Правда, д. 3А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '941',
			'title' => 'ул. им газеты Пионерская Правда, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '942',
			'title' => 'ул. им Героя Советского Союза Б.А.Микуцкого, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '943',
			'title' => 'ул. им Героя Советского Союза Б.А.Микуцкого, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '944',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '945',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '946',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '947',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 10Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '948',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '949',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 14Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '950',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '951',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '952',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '953',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 24Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '954',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '955',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '956',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '957',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '958',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '959',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 4Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '960',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '961',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '962',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '963',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '964',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '965',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 10Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '966',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 12А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '967',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 14А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '968',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '969',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '970',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 16Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '971',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 18А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '972',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '973',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 20А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '974',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '975',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '976',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 28А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '977',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 28Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '978',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '979',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '980',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '981',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '982',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 36А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '983',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 38А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '984',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '985',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '986',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '987',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '988',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '989',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '990',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '991',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '992',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 15А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '993',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '994',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '995',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 25Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '996',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '997',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '998',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '999',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 8Г',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1000',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1001',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1002',
			'title' => 'ул. им Говорова, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1003',
			'title' => 'ул. им Говорова, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1004',
			'title' => 'ул. им Говорова, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1005',
			'title' => 'ул. им Говорова, д. 40А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1006',
			'title' => 'ул. им Говорова, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1007',
			'title' => 'ул. им Говорова, д. 42А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1008',
			'title' => 'ул. им Говорова, д. 48А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1009',
			'title' => 'ул. им Говорова, д. 50А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1010',
			'title' => 'ул. им Говорова, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1011',
			'title' => 'ул. им Говорова, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1012',
			'title' => 'ул. им И.С.Никитина, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1013',
			'title' => 'ул. им И.С.Никитина, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1014',
			'title' => 'ул. им И.С.Никитина, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1015',
			'title' => 'ул. им И.С.Никитина, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1016',
			'title' => 'ул. им И.С.Никитина, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1017',
			'title' => 'ул. им И.С.Никитина, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1018',
			'title' => 'ул. им И.С.Никитина, д. 3Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1019',
			'title' => 'ул. им И.С.Никитина, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1020',
			'title' => 'ул. им И.С.Никитина, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1021',
			'title' => 'ул. им И.С.Никитина, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1022',
			'title' => 'ул. им И.С.Никитина, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1023',
			'title' => 'ул. им Корнеева, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1024',
			'title' => 'ул. им Корнеева, д. 48А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1025',
			'title' => 'ул. им Корнеева, д. 61А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1026',
			'title' => 'ул. им Сергея Лазо, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1027',
			'title' => 'ул. им Сергея Лазо, д. 12А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1028',
			'title' => 'ул. им Сергея Лазо, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1029',
			'title' => 'ул. им Сергея Лазо, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1030',
			'title' => 'ул. им Сергея Лазо, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1031',
			'title' => 'ул. им Сергея Лазо, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1032',
			'title' => 'ул. им Сергея Лазо, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1033',
			'title' => 'ул. им Сергея Лазо, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1034',
			'title' => 'ул. им Шевченко, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1035',
			'title' => 'ул. им Шевченко, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1036',
			'title' => 'ул. им Шевченко, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1037',
			'title' => 'ул. им Шевченко, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1038',
			'title' => 'ул. им Шевченко, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1039',
			'title' => 'ул. им Шевченко, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1040',
			'title' => 'ул. им Шевченко, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1041',
			'title' => 'ул. им Шевченко, д. 28А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1042',
			'title' => 'ул. им Шевченко, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1043',
			'title' => 'ул. им Шевченко, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1044',
			'title' => 'ул. им Шевченко, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1045',
			'title' => 'ул. им Шевченко, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1046',
			'title' => 'ул. им Шевченко, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1047',
			'title' => 'ул. им Шевченко, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1048',
			'title' => 'ул. им Шевченко, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1049',
			'title' => 'ул. им Шевченко, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1050',
			'title' => 'ул. им Шевченко, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1051',
			'title' => 'ул. им Шевченко, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1052',
			'title' => 'ул. им Шевченко, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1053',
			'title' => 'ул. им Шевченко, д. 64',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1054',
			'title' => 'ул. им Шевченко, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1055',
			'title' => 'ул. им Шевченко, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1056',
			'title' => 'ул. им Шевченко, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1057',
			'title' => 'ул. им Шевченко, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1058',
			'title' => 'ул. им Шевченко, д. 80А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1059',
			'title' => 'ул. им Шевченко, д. 82',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1060',
			'title' => 'ул. им Шевченко, д. 84',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1061',
			'title' => 'ул. им Шевченко, д. 86',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1062',
			'title' => 'ул. им Шевченко, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1063',
			'title' => 'ул. им Шевченко, д. 90',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1064',
			'title' => 'ул. Инструментальная, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1065',
			'title' => 'ул. Инструментальная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1066',
			'title' => 'ул. Инструментальная, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1067',
			'title' => 'ул. Инструментальная, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1068',
			'title' => 'ул. Иркутская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1069',
			'title' => 'ул. Иркутская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1070',
			'title' => 'ул. Иркутская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1071',
			'title' => 'ул. Иркутская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1072',
			'title' => 'ул. Историческая, д. 90',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1073',
			'title' => 'ул. Калинина, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1074',
			'title' => 'ул. Калинина, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1075',
			'title' => 'ул. Калинина, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1076',
			'title' => 'ул. Калинина, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1077',
			'title' => 'ул. Калинина, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1078',
			'title' => 'ул. Калинина, д. 2В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1079',
			'title' => 'ул. Калинина, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1080',
			'title' => 'ул. Калинина, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1081',
			'title' => 'ул. Калинина, д. 35А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1082',
			'title' => 'ул. Калинина, д. 3В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1083',
			'title' => 'ул. Калинина, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1084',
			'title' => 'ул. Калинина, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1085',
			'title' => 'ул. Калинина, д. 47 И',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1086',
			'title' => 'ул. Калинина, д. 47 К',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1087',
			'title' => 'ул. Калинина, д. 47А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1088',
			'title' => 'ул. Калинина, д. 5Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1089',
			'title' => 'ул. Калинина, д. 5В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1090',
			'title' => 'ул. Калинина, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1091',
			'title' => 'ул. Калинина, д. 70А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1092',
			'title' => 'ул. Калинина, д. 70б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1093',
			'title' => 'ул. Калинина, д. 71',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1094',
			'title' => 'ул. Калинина, д. 72/5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1095',
			'title' => 'ул. Калинина, д. 80Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1096',
			'title' => 'ул. Калинина, д. 80В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1097',
			'title' => 'ул. Калинина, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1098',
			'title' => 'ул. Каратанова, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1099',
			'title' => 'ул. Карла Маркса, д. 112а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1100',
			'title' => 'ул. Карла Маркса, д. 126',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1101',
			'title' => 'ул. Карла Маркса, д. 129',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1102',
			'title' => 'ул. Карла Маркса, д. 131',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1103',
			'title' => 'ул. Карла Маркса, д. 133',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1104',
			'title' => 'ул. Карла Маркса, д. 135',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1105',
			'title' => 'ул. Карла Маркса, д. 137',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1106',
			'title' => 'ул. Карла Маркса, д. 146',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1107',
			'title' => 'ул. Карла Маркса, д. 150',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1108',
			'title' => 'ул. Карла Маркса, д. 155',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1109',
			'title' => 'ул. Карла Маркса, д. 157',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1110',
			'title' => 'ул. Карла Маркса, д. 157а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1111',
			'title' => 'ул. Карла Маркса, д. 177',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1112',
			'title' => 'ул. Карла Маркса, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1113',
			'title' => 'ул. Карла Маркса, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1114',
			'title' => 'ул. Карла Маркса, д. 60А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1115',
			'title' => 'ул. Карла Маркса, д. 88',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1116',
			'title' => 'ул. Карла Маркса, д. 90',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1117',
			'title' => 'ул. Карла Маркса, д. 92',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1118',
			'title' => 'ул. Кирова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1119',
			'title' => 'ул. Кирова, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1120',
			'title' => 'ул. Кирова, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1121',
			'title' => 'ул. Кирова, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1122',
			'title' => 'ул. Ключевская, д. 101',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1123',
			'title' => 'ул. Ключевская, д. 87',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1124',
			'title' => 'ул. Ключевская, д. 89',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1125',
			'title' => 'ул. Ключевская, д. 91',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1126',
			'title' => 'ул. Ключевская, д. 93',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1127',
			'title' => 'ул. Ключевская, д. 95',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1128',
			'title' => 'ул. Ключевская, д. 97',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1129',
			'title' => 'ул. Коломенская, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1130',
			'title' => 'ул. Коломенская, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1131',
			'title' => 'ул. Коломенская, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1132',
			'title' => 'ул. Коломенская, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1133',
			'title' => 'ул. Коломенская, д. 17А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1134',
			'title' => 'ул. Коломенская, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1135',
			'title' => 'ул. Коломенская, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1136',
			'title' => 'ул. Коломенская, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1137',
			'title' => 'ул. Коломенская, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1138',
			'title' => 'ул. Коломенская, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1139',
			'title' => 'ул. Коломенская, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1140',
			'title' => 'ул. Коломенская, д. 23А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1141',
			'title' => 'ул. Коломенская, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1142',
			'title' => 'ул. Кольцевая, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1143',
			'title' => 'ул. Кольцевая, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1144',
			'title' => 'ул. Кольцевая, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1145',
			'title' => 'ул. Кольцевая, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1146',
			'title' => 'ул. Кольцевая, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1147',
			'title' => 'ул. Кольцевая, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1148',
			'title' => 'ул. Кольцевая, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1149',
			'title' => 'ул. Кольцевая, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1150',
			'title' => 'ул. Кольцевая, д. 18А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1151',
			'title' => 'ул. Кольцевая, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1152',
			'title' => 'ул. Кольцевая, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1153',
			'title' => 'ул. Кольцевая, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1154',
			'title' => 'ул. Кольцевая, д. 22а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1155',
			'title' => 'ул. Кольцевая, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1156',
			'title' => 'ул. Кольцевая, д. 24А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1157',
			'title' => 'ул. Кольцевая, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1158',
			'title' => 'ул. Кольцевая, д. 26А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1159',
			'title' => 'ул. Кольцевая, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1160',
			'title' => 'ул. Кольцевая, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1161',
			'title' => 'ул. Кольцевая, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1162',
			'title' => 'ул. Кольцевая, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1163',
			'title' => 'ул. Кольцевая, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1164',
			'title' => 'ул. Кольцевая, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1165',
			'title' => 'ул. Кольцевая, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1166',
			'title' => 'ул. Кольцевая, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1167',
			'title' => 'ул. Кольцевая, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1168',
			'title' => 'ул. Кольцевая, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1169',
			'title' => 'ул. Комбайностроителей, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1170',
			'title' => 'ул. Комбайностроителей, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1171',
			'title' => 'ул. Комбайностроителей, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1172',
			'title' => 'ул. Комбайностроителей, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1173',
			'title' => 'ул. Комбайностроителей, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1174',
			'title' => 'ул. Комбайностроителей, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1175',
			'title' => 'ул. Комбайностроителей, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1176',
			'title' => 'ул. Комбайностроителей, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1177',
			'title' => 'ул. Комбайностроителей, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1178',
			'title' => 'ул. Комбайностроителей, д. 3А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1179',
			'title' => 'ул. Комбайностроителей, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1180',
			'title' => 'ул. Коммунальная, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1181',
			'title' => 'ул. Коммунальная, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1182',
			'title' => 'ул. Коммунальная, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1183',
			'title' => 'ул. Коммунальная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1184',
			'title' => 'ул. Коммунальная, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1185',
			'title' => 'ул. Коммунальная, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1186',
			'title' => 'ул. Конституции СССР, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1187',
			'title' => 'ул. Конституции СССР, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1188',
			'title' => 'ул. Конституции СССР, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1189',
			'title' => 'ул. Конституции СССР, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1190',
			'title' => 'ул. Копылова, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1191',
			'title' => 'ул. Копылова, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1192',
			'title' => 'ул. Копылова, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1193',
			'title' => 'ул. Копылова, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1194',
			'title' => 'ул. Копылова, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1195',
			'title' => 'ул. Копылова, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1196',
			'title' => 'ул. Копылова, д. 76',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1197',
			'title' => 'ул. Копылова, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1198',
			'title' => 'ул. Корнетова Дружинника, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1199',
			'title' => 'ул. Корнетова Дружинника, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1200',
			'title' => 'ул. Корнетова Дружинника, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1201',
			'title' => 'ул. Корнетова Дружинника, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1202',
			'title' => 'ул. Королева, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1203',
			'title' => 'ул. Королева, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1204',
			'title' => 'ул. Королева, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1205',
			'title' => 'ул. Королева, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1206',
			'title' => 'ул. Королева, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1207',
			'title' => 'ул. Королева, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1208',
			'title' => 'ул. Королева, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1209',
			'title' => 'ул. Королева, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1210',
			'title' => 'ул. Королева, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1211',
			'title' => 'ул. Королева, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1212',
			'title' => 'ул. Королева, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1213',
			'title' => 'ул. Космонавта Быковского, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1214',
			'title' => 'ул. Космонавта Быковского, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1215',
			'title' => 'ул. Космонавта Быковского, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1216',
			'title' => 'ул. Космонавта Быковского, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1217',
			'title' => 'ул. Космонавта Быковского, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1218',
			'title' => 'ул. Космонавта Быковского, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1219',
			'title' => 'ул. Космонавта Быковского, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1220',
			'title' => 'ул. Космонавта Быковского, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1221',
			'title' => 'ул. Космонавта Быковского, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1222',
			'title' => 'ул. Космонавта Быковского, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1223',
			'title' => 'ул. Космонавта Быковского, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1224',
			'title' => 'ул. Космонавта Быковского, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1225',
			'title' => 'ул. Космонавта Николаева, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1226',
			'title' => 'ул. Космонавта Николаева, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1227',
			'title' => 'ул. Космонавта Николаева, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1228',
			'title' => 'ул. Космонавта Николаева, д. 11Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1229',
			'title' => 'ул. Космонавта Николаева, д. 11В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1230',
			'title' => 'ул. Космонавта Николаева, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1231',
			'title' => 'ул. Космонавта Николаева, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1232',
			'title' => 'ул. Космонавта Николаева, д. 15А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1233',
			'title' => 'ул. Космонавта Николаева, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1234',
			'title' => 'ул. Космонавта Николаева, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1235',
			'title' => 'ул. Космонавта Николаева, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1236',
			'title' => 'ул. Космонавта Николаева, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1237',
			'title' => 'ул. Космонавта Терешковой, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1238',
			'title' => 'ул. Космонавта Терешковой, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1239',
			'title' => 'ул. Космонавта Терешковой, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1240',
			'title' => 'ул. Космонавта Терешковой, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1241',
			'title' => 'ул. Космонавта Терешковой, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1242',
			'title' => 'ул. Космонавта Терешковой, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1243',
			'title' => 'ул. Космонавта Терешковой, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1244',
			'title' => 'ул. Космонавта Терешковой, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1245',
			'title' => 'ул. Кочубея, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1246',
			'title' => 'ул. Крайняя, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1247',
			'title' => 'ул. Крайняя, д. 14А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1248',
			'title' => 'ул. Красная Площадь, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1249',
			'title' => 'ул. Красная Площадь, д. 3А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1250',
			'title' => 'ул. Краснодарская, д. 13А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1251',
			'title' => 'ул. Краснодарская, д. 13Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1252',
			'title' => 'ул. Краснодарская, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1253',
			'title' => 'ул. Краснодарская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1254',
			'title' => 'ул. Краснодарская, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1255',
			'title' => 'ул. Краснодарская, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1256',
			'title' => 'ул. Краснодарская, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1257',
			'title' => 'ул. Краснодарская, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1258',
			'title' => 'ул. Краснодарская, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1259',
			'title' => 'ул. Краснодарская, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1260',
			'title' => 'ул. Краснодарская, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1261',
			'title' => 'ул. Краснодарская, д. 2б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1262',
			'title' => 'ул. Краснодарская, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1263',
			'title' => 'ул. Краснодарская, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1264',
			'title' => 'ул. Краснодарская, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1265',
			'title' => 'ул. Краснодарская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1266',
			'title' => 'ул. Краснодарская, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1267',
			'title' => 'ул. Краснодарская, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1268',
			'title' => 'ул. Краснодарская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1269',
			'title' => 'ул. Краснодарская, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1270',
			'title' => 'ул. Краснодарская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1271',
			'title' => 'ул. Красной Армии, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1272',
			'title' => 'ул. Красной Армии, д. 20А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1273',
			'title' => 'ул. Красной Армии, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1274',
			'title' => 'ул. Красной Армии, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1275',
			'title' => 'ул. Красной Армии, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1276',
			'title' => 'ул. Красной Армии, д. 9/11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1277',
			'title' => 'ул. Красномосковская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1278',
			'title' => 'ул. Красномосковская, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1279',
			'title' => 'ул. Красномосковская, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1280',
			'title' => 'ул. Красномосковская, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1281',
			'title' => 'ул. Красномосковская, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1282',
			'title' => 'ул. Красномосковская, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1283',
			'title' => 'ул. Красномосковская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1284',
			'title' => 'ул. Красномосковская, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1285',
			'title' => 'ул. Красномосковская, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1286',
			'title' => 'ул. Красномосковская, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1287',
			'title' => 'ул. Красномосковская, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1288',
			'title' => 'ул. Красномосковская, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1289',
			'title' => 'ул. Красномосковская, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1290',
			'title' => 'ул. Краснофлотская 2-я, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1291',
			'title' => 'ул. Краснофлотская 2-я, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1292',
			'title' => 'ул. Краснофлотская 2-я, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1293',
			'title' => 'ул. Краснофлотская 2-я, д. 15А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1294',
			'title' => 'ул. Краснофлотская 2-я, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1295',
			'title' => 'ул. Краснофлотская 2-я, д. 17А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1296',
			'title' => 'ул. Краснофлотская 2-я, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1297',
			'title' => 'ул. Краснофлотская 2-я, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1298',
			'title' => 'ул. Краснофлотская 2-я, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1299',
			'title' => 'ул. Краснофлотская 2-я, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1300',
			'title' => 'ул. Краснофлотская 2-я, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1301',
			'title' => 'ул. Краснофлотская 2-я, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1302',
			'title' => 'ул. Крупской, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1303',
			'title' => 'ул. Крупской, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1304',
			'title' => 'ул. Крупской, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1305',
			'title' => 'ул. Крупской, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1306',
			'title' => 'ул. Крупской, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1307',
			'title' => 'ул. Крупской, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1308',
			'title' => 'ул. Крупской, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1309',
			'title' => 'ул. Крупской, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1310',
			'title' => 'ул. Крылова, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1311',
			'title' => 'ул. Крылова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1312',
			'title' => 'ул. Крылова, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1313',
			'title' => 'ул. Крылова, д. 3а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1314',
			'title' => 'ул. Крылова, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1315',
			'title' => 'ул. Крылова, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1316',
			'title' => 'ул. Куйбышева, д. 85',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1317',
			'title' => 'ул. Куйбышева, д. 95',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1318',
			'title' => 'ул. Курчатова, д. 15В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1319',
			'title' => 'ул. Курчатова, д. 7Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1320',
			'title' => 'ул. Курчатова, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1321',
			'title' => 'ул. Курчатова, д. 9Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1322',
			'title' => 'ул. Кутузова, д. 103',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1323',
			'title' => 'ул. Кутузова, д. 105',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1324',
			'title' => 'ул. Кутузова, д. 107',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1325',
			'title' => 'ул. Кутузова, д. 111',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1326',
			'title' => 'ул. Кутузова, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1327',
			'title' => 'ул. Кутузова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1328',
			'title' => 'ул. Кутузова, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1329',
			'title' => 'ул. Кутузова, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1330',
			'title' => 'ул. Кутузова, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1331',
			'title' => 'ул. Кутузова, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1332',
			'title' => 'ул. Кутузова, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1333',
			'title' => 'ул. Кутузова, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1334',
			'title' => 'ул. Кутузова, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1335',
			'title' => 'ул. Кутузова, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1336',
			'title' => 'ул. Кутузова, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1337',
			'title' => 'ул. Кутузова, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1338',
			'title' => 'ул. Кутузова, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1339',
			'title' => 'ул. Кутузова, д. 76',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1340',
			'title' => 'ул. Кутузова, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1341',
			'title' => 'ул. Кутузова, д. 82',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1342',
			'title' => 'ул. Кутузова, д. 86',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1343',
			'title' => 'ул. Кутузова, д. 88',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1344',
			'title' => 'ул. Кутузова, д. 89а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1345',
			'title' => 'ул. Кутузова, д. 92',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1346',
			'title' => 'ул. Кутузова, д. 94',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1347',
			'title' => 'ул. Кутузова, д. 96',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1348',
			'title' => 'ул. Ладо Кецховели, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1349',
			'title' => 'ул. Ладо Кецховели, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1350',
			'title' => 'ул. Ладо Кецховели, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1351',
			'title' => 'ул. Ладо Кецховели, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1352',
			'title' => 'ул. Ладо Кецховели, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1353',
			'title' => 'ул. Ладо Кецховели, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1354',
			'title' => 'ул. Ладо Кецховели, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1355',
			'title' => 'ул. Ладо Кецховели, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1356',
			'title' => 'ул. Ладо Кецховели, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1357',
			'title' => 'ул. Ладо Кецховели, д. 57',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1358',
			'title' => 'ул. Ладо Кецховели, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1359',
			'title' => 'ул. Ладо Кецховели, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1360',
			'title' => 'ул. Ладо Кецховели, д. 60А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1361',
			'title' => 'ул. Ладо Кецховели, д. 65А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1362',
			'title' => 'ул. Ладо Кецховели, д. 65Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1363',
			'title' => 'ул. Ладо Кецховели, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1364',
			'title' => 'ул. Ладо Кецховели, д. 67, к. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1365',
			'title' => 'ул. Ладо Кецховели, д. 67, к. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1366',
			'title' => 'ул. Ладо Кецховели, д. 69',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1367',
			'title' => 'ул. Ладо Кецховели, д. 71А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1368',
			'title' => 'ул. Ладо Кецховели, д. 71Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1369',
			'title' => 'ул. Ладо Кецховели, д. 75',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1370',
			'title' => 'ул. Ладо Кецховели, д. 75А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1371',
			'title' => 'ул. Ладо Кецховели, д. 77',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1372',
			'title' => 'ул. Ладо Кецховели, д. 91',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1373',
			'title' => 'ул. Ладо Кецховели, д. 95',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1374',
			'title' => 'ул. Ладо Кецховели, д. 97',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1375',
			'title' => 'ул. Ленина, д. 102',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1376',
			'title' => 'ул. Ленина, д. 104',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1377',
			'title' => 'ул. Ленина, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1378',
			'title' => 'ул. Ленина, д. 110',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1379',
			'title' => 'ул. Ленина, д. 112',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1380',
			'title' => 'ул. Ленина, д. 115',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1381',
			'title' => 'ул. Ленина, д. 118',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1382',
			'title' => 'ул. Ленина, д. 120',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1383',
			'title' => 'ул. Ленина, д. 121',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1384',
			'title' => 'ул. Ленина, д. 122',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1385',
			'title' => 'ул. Ленина, д. 126',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1386',
			'title' => 'ул. Ленина, д. 148',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1387',
			'title' => 'ул. Ленина, д. 153',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1388',
			'title' => 'ул. Ленина, д. 155',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1389',
			'title' => 'ул. Ленина, д. 169',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1390',
			'title' => 'ул. Ленина, д. 177А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1391',
			'title' => 'ул. Ленина, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1392',
			'title' => 'ул. Ленина, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1393',
			'title' => 'ул. Ленина, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1394',
			'title' => 'ул. Ленина, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1395',
			'title' => 'ул. Ленина, д. 29/30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1396',
			'title' => 'ул. Ленина, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1397',
			'title' => 'ул. Ленина, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1398',
			'title' => 'ул. Ленина, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1399',
			'title' => 'ул. Ленина, д. 62а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1400',
			'title' => 'ул. Ленина, д. 67/34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1401',
			'title' => 'ул. Ленина, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1402',
			'title' => 'ул. Ленина, д. 91',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1403',
			'title' => 'ул. Ленина, д. 92',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1404',
			'title' => 'ул. Ленина, д. 97',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1405',
			'title' => 'ул. Ленинградская 1-я, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1406',
			'title' => 'ул. Ленинградская 1-я, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1407',
			'title' => 'ул. Ленинградская 1-я, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1408',
			'title' => 'ул. Ленинградская 1-я, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1409',
			'title' => 'ул. Ленинградская 1-я, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1410',
			'title' => 'ул. Ленинградская 1-я, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1411',
			'title' => 'ул. Ленинградская 1-я, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1412',
			'title' => 'ул. Ленинградская 1-я, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1413',
			'title' => 'ул. Ленинградская 1-я, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1414',
			'title' => 'ул. Ленинградская 1-я, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1415',
			'title' => 'ул. Ленинградская 1-я, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1416',
			'title' => 'ул. Ленинградская 1-я, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1417',
			'title' => 'ул. Ленинградская 1-я, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1418',
			'title' => 'ул. Ленинградская 1-я, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1419',
			'title' => 'ул. Ленинградская 1-я, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1420',
			'title' => 'ул. Ленинградская 1-я, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1421',
			'title' => 'ул. Ленинградская 1-я, д. 28А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1422',
			'title' => 'ул. Ленинградская 1-я, д. 28Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1423',
			'title' => 'ул. Ленинградская 1-я, д. 28В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1424',
			'title' => 'ул. Ленинградская 1-я, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1425',
			'title' => 'ул. Ленинградская 1-я, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1426',
			'title' => 'ул. Ленинградская 1-я, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1427',
			'title' => 'ул. Ленинградская 1-я, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1428',
			'title' => 'ул. Ленинградская 1-я, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1429',
			'title' => 'ул. Ленинградская, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1430',
			'title' => 'ул. Ленинградская, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1431',
			'title' => 'ул. Ломоносова, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1432',
			'title' => 'ул. Ломоносова, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1433',
			'title' => 'ул. Ломоносова, д. 102',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1434',
			'title' => 'ул. Ломоносова, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1435',
			'title' => 'ул. Ломоносова, д. 94В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1436',
			'title' => 'ул. Ломоносова, д. 98',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1437',
			'title' => 'ул. Львовская, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1438',
			'title' => 'ул. Львовская, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1439',
			'title' => 'ул. Львовская, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1440',
			'title' => 'ул. Львовская, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1441',
			'title' => 'ул. Львовская, д. 21А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1442',
			'title' => 'ул. Львовская, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1443',
			'title' => 'ул. Львовская, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1444',
			'title' => 'ул. Львовская, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1445',
			'title' => 'ул. Львовская, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1446',
			'title' => 'ул. Львовская, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1447',
			'title' => 'ул. Львовская, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1448',
			'title' => 'ул. Львовская, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1449',
			'title' => 'ул. Львовская, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1450',
			'title' => 'ул. Львовская, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1451',
			'title' => 'ул. Львовская, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1452',
			'title' => 'ул. Львовская, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1453',
			'title' => 'ул. Львовская, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1454',
			'title' => 'ул. Львовская, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1455',
			'title' => 'ул. Львовская, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1456',
			'title' => 'ул. Львовская, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1457',
			'title' => 'ул. Львовская, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1458',
			'title' => 'ул. Львовская, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1459',
			'title' => 'ул. Львовская, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1460',
			'title' => 'ул. Маерчака, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1461',
			'title' => 'ул. Маерчака, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1462',
			'title' => 'ул. Маерчака, д. 43А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1463',
			'title' => 'ул. Маерчака, д. 43В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1464',
			'title' => 'ул. Маерчака, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1465',
			'title' => 'ул. Малаховская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1466',
			'title' => 'ул. Малаховская, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1467',
			'title' => 'ул. Марковского, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1468',
			'title' => 'ул. Марковского, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1469',
			'title' => 'ул. Марковского, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1470',
			'title' => 'ул. Марковского, д. 55',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1471',
			'title' => 'ул. Марковского, д. 73/37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1472',
			'title' => 'ул. Марковского, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1473',
			'title' => 'ул. Марковского, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1474',
			'title' => 'ул. Маршала К.Рокоссовского, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1475',
			'title' => 'ул. Маршала Малиновского, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1476',
			'title' => 'ул. Маршала Малиновского, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1477',
			'title' => 'ул. Маршала Малиновского, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1478',
			'title' => 'ул. Маршала Малиновского, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1479',
			'title' => 'ул. Маршала Малиновского, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1480',
			'title' => 'ул. Маршала Малиновского, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1481',
			'title' => 'ул. Маршала Малиновского, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1482',
			'title' => 'ул. Маршала Малиновского, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1483',
			'title' => 'ул. Маршала Малиновского, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1484',
			'title' => 'ул. Маршала Малиновского, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1485',
			'title' => 'ул. Маршала Малиновского, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1486',
			'title' => 'ул. Маршала Малиновского, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1487',
			'title' => 'ул. Маршала Малиновского, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1488',
			'title' => 'ул. Маршала Малиновского, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1489',
			'title' => 'ул. Мате Залки, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1490',
			'title' => 'ул. Мате Залки, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1491',
			'title' => 'ул. Мате Залки, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1492',
			'title' => 'ул. Мате Залки, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1493',
			'title' => 'ул. Менжинского, д. 10А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1494',
			'title' => 'ул. Менжинского, д. 12Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1495',
			'title' => 'ул. Менжинского, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1496',
			'title' => 'ул. Менжинского, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1497',
			'title' => 'ул. Менжинского, д. 14А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1498',
			'title' => 'ул. Менжинского, д. 14Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1499',
			'title' => 'ул. Менжинского, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1500',
			'title' => 'ул. Менжинского, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1501',
			'title' => 'ул. Менжинского, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1502',
			'title' => 'ул. Менжинского, д. 18А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1503',
			'title' => 'ул. Менжинского, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1504',
			'title' => 'ул. Менжинского, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1505',
			'title' => 'ул. Мечникова, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1506',
			'title' => 'ул. Мечникова, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1507',
			'title' => 'ул. Мечникова, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1508',
			'title' => 'ул. Мечникова, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1509',
			'title' => 'ул. Мечникова, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1510',
			'title' => 'ул. Мечникова, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1511',
			'title' => 'ул. Мечникова, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1512',
			'title' => 'ул. Мечникова, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1513',
			'title' => 'ул. Мечникова, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1514',
			'title' => 'ул. Мечникова, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1515',
			'title' => 'ул. Мечникова, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1516',
			'title' => 'ул. Мечникова, д. 46А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1517',
			'title' => 'ул. Мечникова, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1518',
			'title' => 'ул. Мечникова, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1519',
			'title' => 'ул. Мечникова, д. 55',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1520',
			'title' => 'ул. Мечникова, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1521',
			'title' => 'ул. Минина, д. 123',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1522',
			'title' => 'ул. Минина, д. 125',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1523',
			'title' => 'ул. Мичурина, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1524',
			'title' => 'ул. Мичурина, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1525',
			'title' => 'ул. Мичурина, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1526',
			'title' => 'ул. Мичурина, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1527',
			'title' => 'ул. Мичурина, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1528',
			'title' => 'ул. Мичурина, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1529',
			'title' => 'ул. Мичурина, д. 1В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1530',
			'title' => 'ул. Мичурина, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1531',
			'title' => 'ул. Мичурина, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1532',
			'title' => 'ул. Мичурина, д. 23А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1533',
			'title' => 'ул. Мичурина, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1534',
			'title' => 'ул. Мичурина, д. 25А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1535',
			'title' => 'ул. Мичурина, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1536',
			'title' => 'ул. Мичурина, д. 27А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1537',
			'title' => 'ул. Мичурина, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1538',
			'title' => 'ул. Мичурина, д. 29А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1539',
			'title' => 'ул. Мичурина, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1540',
			'title' => 'ул. Мичурина, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1541',
			'title' => 'ул. Мичурина, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1542',
			'title' => 'ул. Мичурина, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1543',
			'title' => 'ул. Мичурина, д. 3А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1544',
			'title' => 'ул. Мичурина, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1545',
			'title' => 'ул. Мичурина, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1546',
			'title' => 'ул. Мичурина, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1547',
			'title' => 'ул. Мичурина, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1548',
			'title' => 'ул. Мичурина, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1549',
			'title' => 'ул. Мичурина, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1550',
			'title' => 'ул. Мичурина, д. 5Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1551',
			'title' => 'ул. Мичурина, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1552',
			'title' => 'ул. Мичурина, д. 61',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1553',
			'title' => 'ул. Мичурина, д. 63',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1554',
			'title' => 'ул. Мичурина, д. 65',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1555',
			'title' => 'ул. Мичурина, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1556',
			'title' => 'ул. Мичурина, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1557',
			'title' => 'ул. Мичурина, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1558',
			'title' => 'ул. Мичурина, д. 9а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1559',
			'title' => 'ул. Можайского, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1560',
			'title' => 'ул. Можайского, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1561',
			'title' => 'ул. Можайского, д. 21а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1562',
			'title' => 'ул. Можайского, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1563',
			'title' => 'ул. Можайского, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1564',
			'title' => 'ул. Можайского, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1565',
			'title' => 'ул. Можайского, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1566',
			'title' => 'ул. Московская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1567',
			'title' => 'ул. Московская, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1568',
			'title' => 'ул. Московская, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1569',
			'title' => 'ул. Московская, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1570',
			'title' => 'ул. Московская, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1571',
			'title' => 'ул. Московская, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1572',
			'title' => 'ул. Московская, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1573',
			'title' => 'ул. Московская, д. 22А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1574',
			'title' => 'ул. Московская, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1575',
			'title' => 'ул. Московская, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1576',
			'title' => 'ул. Московская, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1577',
			'title' => 'ул. Московская, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1578',
			'title' => 'ул. Московская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1579',
			'title' => 'ул. Московская, д. 3А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1580',
			'title' => 'ул. Московская, д. 3Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1581',
			'title' => 'ул. Московская, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1582',
			'title' => 'ул. Московская, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1583',
			'title' => 'ул. Московская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1584',
			'title' => 'ул. Московская, д. 5А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1585',
			'title' => 'ул. Московская, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1586',
			'title' => 'ул. Московская, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1587',
			'title' => 'ул. Московская, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1588',
			'title' => 'ул. Московская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1589',
			'title' => 'ул. Нерчинская, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1590',
			'title' => 'ул. Новая Заря, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1591',
			'title' => 'ул. Новая Заря, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1592',
			'title' => 'ул. Новая Заря, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1593',
			'title' => 'ул. Новая Заря, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1594',
			'title' => 'ул. Новая Заря, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1595',
			'title' => 'ул. Новая Заря, д. 2Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1596',
			'title' => 'ул. Новая Заря, д. 2В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1597',
			'title' => 'ул. Новая Заря, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1598',
			'title' => 'ул. Новая Заря, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1599',
			'title' => 'ул. Новая Заря, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1600',
			'title' => 'ул. Новая Заря, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1601',
			'title' => 'ул. Новая Заря, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1602',
			'title' => 'ул. Новая, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1603',
			'title' => 'ул. Новая, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1604',
			'title' => 'ул. Новая, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1605',
			'title' => 'ул. Новая, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1606',
			'title' => 'ул. Новая, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1607',
			'title' => 'ул. Новая, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1608',
			'title' => 'ул. Новая, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1609',
			'title' => 'ул. Новая, д. 60',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1610',
			'title' => 'ул. Новая, д. 62',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1611',
			'title' => 'ул. Новгородская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1612',
			'title' => 'ул. Новгородская, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1613',
			'title' => 'ул. Новгородская, д. 12А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1614',
			'title' => 'ул. Новгородская, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1615',
			'title' => 'ул. Новгородская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1616',
			'title' => 'ул. Новгородская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1617',
			'title' => 'ул. Новгородская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1618',
			'title' => 'ул. Новгородская, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1619',
			'title' => 'ул. Новгородская, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1620',
			'title' => 'ул. Новосибирская, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1621',
			'title' => 'ул. Новосибирская, д. 37А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1622',
			'title' => 'ул. Новосибирская, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1623',
			'title' => 'ул. Новосибирская, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1624',
			'title' => 'ул. Обороны, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1625',
			'title' => 'ул. Огородная 2-я, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1626',
			'title' => 'ул. Огородная 2-я, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1627',
			'title' => 'ул. Огородная 2-я, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1628',
			'title' => 'ул. Одесская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1629',
			'title' => 'ул. Одесская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1630',
			'title' => 'ул. Одесская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1631',
			'title' => 'ул. Одесская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1632',
			'title' => 'ул. Октябрьская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1633',
			'title' => 'ул. Октябрьская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1634',
			'title' => 'ул. Омская, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1635',
			'title' => 'ул. Омская, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1636',
			'title' => 'ул. Омская, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1637',
			'title' => 'ул. Омская, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1638',
			'title' => 'ул. Охраны Труда, д. 1а/6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1639',
			'title' => 'ул. Парашютная, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1640',
			'title' => 'ул. Парашютная, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1641',
			'title' => 'ул. Парашютная, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1642',
			'title' => 'ул. Парашютная, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1643',
			'title' => 'ул. Парашютная, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1644',
			'title' => 'ул. Парашютная, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1645',
			'title' => 'ул. Парашютная, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1646',
			'title' => 'ул. Парашютная, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1647',
			'title' => 'ул. Парашютная, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1648',
			'title' => 'ул. Парашютная, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1649',
			'title' => 'ул. Парашютная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1650',
			'title' => 'ул. Парашютная, д. 64',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1651',
			'title' => 'ул. Парашютная, д. 64а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1652',
			'title' => 'ул. Парашютная, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1653',
			'title' => 'ул. Парашютная, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1654',
			'title' => 'ул. Парашютная, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1655',
			'title' => 'ул. Парашютная, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1656',
			'title' => 'ул. Парашютная, д. 72А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1657',
			'title' => 'ул. Парашютная, д. 72Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1658',
			'title' => 'ул. Парашютная, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1659',
			'title' => 'ул. Парашютная, д. 74а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1660',
			'title' => 'ул. Парашютная, д. 76',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1661',
			'title' => 'ул. Парашютная, д. 76А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1662',
			'title' => 'ул. Парашютная, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1663',
			'title' => 'ул. Парашютная, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1664',
			'title' => 'ул. Парашютная, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1665',
			'title' => 'ул. Парашютная, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1666',
			'title' => 'ул. Парижской Коммуны, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1667',
			'title' => 'ул. Парижской Коммуны, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1668',
			'title' => 'ул. Парижской Коммуны, д. 42А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1669',
			'title' => 'ул. Парижской Коммуны, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1670',
			'title' => 'ул. Парижской Коммуны, д. 5/39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1671',
			'title' => 'ул. Паровозная, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1672',
			'title' => 'ул. Паровозная, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1673',
			'title' => 'ул. Паровозная, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1674',
			'title' => 'ул. Паровозная, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1675',
			'title' => 'ул. Паровозная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1676',
			'title' => 'ул. Паровозная, д. 4а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1677',
			'title' => 'ул. Паровозная, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1678',
			'title' => 'ул. Паровозная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1679',
			'title' => 'ул. Паровозная, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1680',
			'title' => 'ул. Паровозная, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1681',
			'title' => 'ул. Паровозная, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1682',
			'title' => 'ул. Партизана Железняка, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1683',
			'title' => 'ул. Партизана Железняка, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1684',
			'title' => 'ул. Партизана Железняка, д. 20А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1685',
			'title' => 'ул. Партизана Железняка, д. 22А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1686',
			'title' => 'ул. Партизана Железняка, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1687',
			'title' => 'ул. Партизана Железняка, д. 24А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1688',
			'title' => 'ул. Партизана Железняка, д. 24Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1689',
			'title' => 'ул. Партизана Железняка, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1690',
			'title' => 'ул. Партизана Железняка, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1691',
			'title' => 'ул. Партизана Железняка, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1692',
			'title' => 'ул. Партизана Железняка, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1693',
			'title' => 'ул. Партизана Железняка, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1694',
			'title' => 'ул. Партизана Железняка, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1695',
			'title' => 'ул. Партизана Железняка, д. 8А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1696',
			'title' => 'ул. Партизана Железняка, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1697',
			'title' => 'ул. Партизана Железняка, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1698',
			'title' => 'ул. Партизана Железняка, д. 9Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1699',
			'title' => 'ул. Партизанская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1700',
			'title' => 'ул. Партизанская, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1701',
			'title' => 'ул. Партизанская, д. 70А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1702',
			'title' => 'ул. Партизанская, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1703',
			'title' => 'ул. Пархоменко, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1704',
			'title' => 'ул. Пастеровская, д. 25А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1705',
			'title' => 'ул. Пастеровская, д. 25Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1706',
			'title' => 'ул. Первых пионеров, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1707',
			'title' => 'ул. Перенсона, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1708',
			'title' => 'ул. Песочная, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1709',
			'title' => 'ул. Песочная, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1710',
			'title' => 'ул. Песочная, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1711',
			'title' => 'ул. Песочная, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1712',
			'title' => 'ул. Песочная, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1713',
			'title' => 'ул. Песочная, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1714',
			'title' => 'ул. Песочная, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1715',
			'title' => 'ул. Песочная, д. 20Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1716',
			'title' => 'ул. Песочная, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1717',
			'title' => 'ул. Песочная, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1718',
			'title' => 'ул. Песочная, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1719',
			'title' => 'ул. Песочная, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1720',
			'title' => 'ул. Песочная, д. 2А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1721',
			'title' => 'ул. Песочная, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1722',
			'title' => 'ул. Песочная, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1723',
			'title' => 'ул. Песочная, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1724',
			'title' => 'ул. Песочная, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1725',
			'title' => 'ул. Песочная, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1726',
			'title' => 'ул. Пирогова, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1727',
			'title' => 'ул. Пирогова, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1728',
			'title' => 'ул. Пирогова, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1729',
			'title' => 'ул. Пирогова, д. 7А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1730',
			'title' => 'ул. Пирогова, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1731',
			'title' => 'ул. Писателя Н.Устиновича, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1732',
			'title' => 'ул. Писателя Н.Устиновича, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1733',
			'title' => 'ул. Писателя Н.Устиновича, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1734',
			'title' => 'ул. Писателя Н.Устиновича, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1735',
			'title' => 'ул. Писателя Н.Устиновича, д. 1А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1736',
			'title' => 'ул. Писателя Н.Устиновича, д. 1Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1737',
			'title' => 'ул. Писателя Н.Устиновича, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1738',
			'title' => 'ул. Писателя Н.Устиновича, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1739',
			'title' => 'ул. Писателя Н.Устиновича, д. 22А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1740',
			'title' => 'ул. Писателя Н.Устиновича, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1741',
			'title' => 'ул. Писателя Н.Устиновича, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1742',
			'title' => 'ул. Писателя Н.Устиновича, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1743',
			'title' => 'ул. Писателя Н.Устиновича, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1744',
			'title' => 'ул. Писателя Н.Устиновича, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1745',
			'title' => 'ул. Писателя Н.Устиновича, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1746',
			'title' => 'ул. Писателя Н.Устиновича, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1747',
			'title' => 'ул. Побежимова, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1748',
			'title' => 'ул. Побежимова, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1749',
			'title' => 'ул. Побежимова, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1750',
			'title' => 'ул. Побежимова, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1751',
			'title' => 'ул. Побежимова, д. 44А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1752',
			'title' => 'ул. Побежимова, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1753',
			'title' => 'ул. Побежимова, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1754',
			'title' => 'ул. Пожарского, д. 168',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1755',
			'title' => 'ул. Ползунова, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1756',
			'title' => 'ул. Ползунова, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1757',
			'title' => 'ул. Ползунова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1758',
			'title' => 'ул. Пролетарская, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1759',
			'title' => 'ул. Пролетарская, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1760',
			'title' => 'ул. Пролетарская, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1761',
			'title' => 'ул. Профсоюзов, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1762',
			'title' => 'ул. Профсоюзов, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1763',
			'title' => 'ул. Профсоюзов, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1764',
			'title' => 'ул. Профсоюзов, д. 30',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1765',
			'title' => 'ул. Профсоюзов, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1766',
			'title' => 'ул. Профсоюзов, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1767',
			'title' => 'ул. Профсоюзов, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1768',
			'title' => 'ул. Рейдовая, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1769',
			'title' => 'ул. Рейдовая, д. 44А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1770',
			'title' => 'ул. Рейдовая, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1771',
			'title' => 'ул. Рейдовая, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1772',
			'title' => 'ул. Рейдовая, д. 57Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1773',
			'title' => 'ул. Рейдовая, д. 57В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1774',
			'title' => 'ул. Рейдовая, д. 65А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1775',
			'title' => 'ул. Рейдовая, д. 74В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1776',
			'title' => 'ул. Республики, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1777',
			'title' => 'ул. Республики, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1778',
			'title' => 'ул. Республики, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1779',
			'title' => 'ул. Республики, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1780',
			'title' => 'ул. Республики, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1781',
			'title' => 'ул. Республики, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1782',
			'title' => 'ул. Республики, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1783',
			'title' => 'ул. Республики, д. 49А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1784',
			'title' => 'ул. Республики, д. 66',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1785',
			'title' => 'ул. Робеспьера, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1786',
			'title' => 'ул. Робеспьера, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1787',
			'title' => 'ул. Робеспьера, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1788',
			'title' => 'ул. Робеспьера, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1789',
			'title' => 'ул. Робеспьера, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1790',
			'title' => 'ул. Робеспьера, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1791',
			'title' => 'ул. Сады, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1792',
			'title' => 'ул. Сады, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1793',
			'title' => 'ул. Саянская, д. 245',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1794',
			'title' => 'ул. Саянская, д. 247',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1795',
			'title' => 'ул. Саянская, д. 249',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1796',
			'title' => 'ул. Саянская, д. 251',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1797',
			'title' => 'ул. Саянская, д. 253',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1798',
			'title' => 'ул. Саянская, д. 257',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1799',
			'title' => 'ул. Свердловская, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1800',
			'title' => 'ул. Свердловская, д. 11Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1801',
			'title' => 'ул. Свердловская, д. 124',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1802',
			'title' => 'ул. Свердловская, д. 129',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1803',
			'title' => 'ул. Свердловская, д. 13а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1804',
			'title' => 'ул. Свердловская, д. 13Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1805',
			'title' => 'ул. Свердловская, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1806',
			'title' => 'ул. Свердловская, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1807',
			'title' => 'ул. Свердловская, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1808',
			'title' => 'ул. Свердловская, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1809',
			'title' => 'ул. Свердловская, д. 247',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1810',
			'title' => 'ул. Свердловская, д. 249',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1811',
			'title' => 'ул. Свердловская, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1812',
			'title' => 'ул. Свердловская, д. 253',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1813',
			'title' => 'ул. Свердловская, д. 255',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1814',
			'title' => 'ул. Свердловская, д. 257',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1815',
			'title' => 'ул. Свердловская, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1816',
			'title' => 'ул. Свердловская, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1817',
			'title' => 'ул. Свердловская, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1818',
			'title' => 'ул. Свердловская, д. 31А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1819',
			'title' => 'ул. Свердловская, д. 33А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1820',
			'title' => 'ул. Свердловская, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1821',
			'title' => 'ул. Свердловская, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1822',
			'title' => 'ул. Свердловская, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1823',
			'title' => 'ул. Свердловская, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1824',
			'title' => 'ул. Свердловская, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1825',
			'title' => 'ул. Свердловская, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1826',
			'title' => 'ул. Свердловская, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1827',
			'title' => 'ул. Свердловская, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1828',
			'title' => 'ул. Свердловская, д. 49А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1829',
			'title' => 'ул. Свердловская, д. 51',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1830',
			'title' => 'ул. Свердловская, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1831',
			'title' => 'ул. Свердловская, д. 57',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1832',
			'title' => 'ул. Свердловская, д. 6 д',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1833',
			'title' => 'ул. Свердловская, д. 61',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1834',
			'title' => 'ул. Свердловская, д. 6Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1835',
			'title' => 'ул. Свердловская, д. 6В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1836',
			'title' => 'ул. Свердловская, д. 9А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1837',
			'title' => 'ул. Светлова, д. 7/2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1838',
			'title' => 'ул. Свободная, д. 5Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1839',
			'title' => 'ул. Северная, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1840',
			'title' => 'ул. Северо-Енисейская, д. 50А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1841',
			'title' => 'ул. Семафорная, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1842',
			'title' => 'ул. Семафорная, д. 181',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1843',
			'title' => 'ул. Семафорная, д. 185',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1844',
			'title' => 'ул. Семафорная, д. 187',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1845',
			'title' => 'ул. Семафорная, д. 189',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1846',
			'title' => 'ул. Семафорная, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1847',
			'title' => 'ул. Семафорная, д. 191',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1848',
			'title' => 'ул. Семафорная, д. 193',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1849',
			'title' => 'ул. Семафорная, д. 199',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1850',
			'title' => 'ул. Семафорная, д. 201',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1851',
			'title' => 'ул. Семафорная, д. 205',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1852',
			'title' => 'ул. Семафорная, д. 205А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1853',
			'title' => 'ул. Семафорная, д. 207',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1854',
			'title' => 'ул. Семафорная, д. 209',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1855',
			'title' => 'ул. Семафорная, д. 211',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1856',
			'title' => 'ул. Семафорная, д. 213',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1857',
			'title' => 'ул. Семафорная, д. 215',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1858',
			'title' => 'ул. Семафорная, д. 217',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1859',
			'title' => 'ул. Семафорная, д. 225',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1860',
			'title' => 'ул. Семафорная, д. 231',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1861',
			'title' => 'ул. Семафорная, д. 233',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1862',
			'title' => 'ул. Семафорная, д. 235',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1863',
			'title' => 'ул. Семафорная, д. 235А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1864',
			'title' => 'ул. Семафорная, д. 237',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1865',
			'title' => 'ул. Семафорная, д. 239',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1866',
			'title' => 'ул. Семафорная, д. 239А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1867',
			'title' => 'ул. Семафорная, д. 241',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1868',
			'title' => 'ул. Семафорная, д. 243',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1869',
			'title' => 'ул. Семафорная, д. 243А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1870',
			'title' => 'ул. Семафорная, д. 245',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1871',
			'title' => 'ул. Семафорная, д. 245А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1872',
			'title' => 'ул. Семафорная, д. 249',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1873',
			'title' => 'ул. Семафорная, д. 255',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1874',
			'title' => 'ул. Семафорная, д. 257',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1875',
			'title' => 'ул. Семафорная, д. 259',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1876',
			'title' => 'ул. Семафорная, д. 397',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1877',
			'title' => 'ул. Семафорная, д. 399',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1878',
			'title' => 'ул. Семафорная, д. 431',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1879',
			'title' => 'ул. Семафорная, д. 441',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1880',
			'title' => 'ул. Серова, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1881',
			'title' => 'ул. Складская, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1882',
			'title' => 'ул. Солнечная, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1883',
			'title' => 'ул. Солнечная, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1884',
			'title' => 'ул. Солнечная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1885',
			'title' => 'ул. Солнечная, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1886',
			'title' => 'ул. Софьи Ковалевской, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1887',
			'title' => 'ул. Софьи Ковалевской, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1888',
			'title' => 'ул. Софьи Ковалевской, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1889',
			'title' => 'ул. Софьи Ковалевской, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1890',
			'title' => 'ул. Софьи Ковалевской, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1891',
			'title' => 'ул. Софьи Ковалевской, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1892',
			'title' => 'ул. Софьи Ковалевской, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1893',
			'title' => 'ул. Софьи Ковалевской, д. 6А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1894',
			'title' => 'ул. Софьи Ковалевской, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1895',
			'title' => 'ул. Спандаряна, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1896',
			'title' => 'ул. Спандаряна, д. 21',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1897',
			'title' => 'ул. Спандаряна, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1898',
			'title' => 'ул. Спандаряна, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1899',
			'title' => 'ул. Спандаряна, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1900',
			'title' => 'ул. Спартаковцев, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1901',
			'title' => 'ул. Спартаковцев, д. 73А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1902',
			'title' => 'ул. Спартаковцев, д. 77А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1903',
			'title' => 'ул. Спортивная, д. 174',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1904',
			'title' => 'ул. Спортивная, д. 178',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1905',
			'title' => 'ул. Спортивная, д. 184',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1906',
			'title' => 'ул. Спортивная, д. 186',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1907',
			'title' => 'ул. Спортивная, д. 188',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1908',
			'title' => 'ул. Спортивная, д. 190',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1909',
			'title' => 'ул. Станочная, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1910',
			'title' => 'ул. Станочная, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1911',
			'title' => 'ул. Станочная, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1912',
			'title' => 'ул. Судостроительная, д. 101',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1913',
			'title' => 'ул. Судостроительная, д. 109',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1914',
			'title' => 'ул. Судостроительная, д. 133',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1915',
			'title' => 'ул. Судостроительная, д. 171',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1916',
			'title' => 'ул. Судостроительная, д. 177А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1917',
			'title' => 'ул. Судостроительная, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1918',
			'title' => 'ул. Судостроительная, д. 38',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1919',
			'title' => 'ул. Судостроительная, д. 40',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1920',
			'title' => 'ул. Судостроительная, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1921',
			'title' => 'ул. Судостроительная, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1922',
			'title' => 'ул. Судостроительная, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1923',
			'title' => 'ул. Судостроительная, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1924',
			'title' => 'ул. Судостроительная, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1925',
			'title' => 'ул. Судостроительная, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1926',
			'title' => 'ул. Судостроительная, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1927',
			'title' => 'ул. Судостроительная, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1928',
			'title' => 'ул. Судостроительная, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1929',
			'title' => 'ул. Судостроительная, д. 93',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1930',
			'title' => 'ул. Судостроительная, д. 95',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1931',
			'title' => 'ул. Судостроительная, д. 97А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1932',
			'title' => 'ул. Сурикова, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1933',
			'title' => 'ул. Сурикова, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1934',
			'title' => 'ул. Сурикова, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1935',
			'title' => 'ул. Сурикова, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1936',
			'title' => 'ул. Сурикова, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1937',
			'title' => 'ул. Сурикова, д. 54А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1938',
			'title' => 'ул. Текстильщиков, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1939',
			'title' => 'ул. Текстильщиков, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1940',
			'title' => 'ул. Текстильщиков, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1941',
			'title' => 'ул. Текстильщиков, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1942',
			'title' => 'ул. Текстильщиков, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1943',
			'title' => 'ул. Текстильщиков, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1944',
			'title' => 'ул. Текстильщиков, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1945',
			'title' => 'ул. Текстильщиков, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1946',
			'title' => 'ул. Тельмана, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1947',
			'title' => 'ул. Тельмана, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1948',
			'title' => 'ул. Тельмана, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1949',
			'title' => 'ул. Тельмана, д. 15А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1950',
			'title' => 'ул. Тельмана, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1951',
			'title' => 'ул. Тельмана, д. 16А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1952',
			'title' => 'ул. Тельмана, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1953',
			'title' => 'ул. Тельмана, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1954',
			'title' => 'ул. Тельмана, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1955',
			'title' => 'ул. Тельмана, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1956',
			'title' => 'ул. Тельмана, д. 28В',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1957',
			'title' => 'ул. Тельмана, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1958',
			'title' => 'ул. Тельмана, д. 30А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1959',
			'title' => 'ул. Тельмана, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1960',
			'title' => 'ул. Тельмана, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1961',
			'title' => 'ул. Тельмана, д. 33А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1962',
			'title' => 'ул. Тельмана, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1963',
			'title' => 'ул. Тельмана, д. 43А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1964',
			'title' => 'ул. Тельмана, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1965',
			'title' => 'ул. Тельмана, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1966',
			'title' => 'ул. Тельмана, д. 47А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1967',
			'title' => 'ул. Тельмана, д. 47Б',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1968',
			'title' => 'ул. Тельмана, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1969',
			'title' => 'ул. Тельмана, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1970',
			'title' => 'ул. Тельмана, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1971',
			'title' => 'ул. Тельмана, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1972',
			'title' => 'ул. Тимирязева, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1973',
			'title' => 'ул. Тимирязева, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1974',
			'title' => 'ул. Тимирязева, д. 43',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1975',
			'title' => 'ул. Тимирязева, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1976',
			'title' => 'ул. Тимирязева, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1977',
			'title' => 'ул. Тимирязева, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1978',
			'title' => 'ул. Тобольская, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1979',
			'title' => 'ул. Тобольская, д. 19А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1980',
			'title' => 'ул. Тобольская, д. 23',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1981',
			'title' => 'ул. Тобольская, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1982',
			'title' => 'ул. Тобольская, д. 25А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1983',
			'title' => 'ул. Тобольская, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1984',
			'title' => 'ул. Тобольская, д. 27А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1985',
			'title' => 'ул. Тобольская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1986',
			'title' => 'ул. Тобольская, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1987',
			'title' => 'ул. Тобольская, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1988',
			'title' => 'ул. Тобольская, д. 33А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1989',
			'title' => 'ул. Тобольская, д. 37А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1990',
			'title' => 'ул. Толстого, д. 32',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1991',
			'title' => 'ул. Толстого, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1992',
			'title' => 'ул. Толстого, д. 45',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1993',
			'title' => 'ул. Толстого, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1994',
			'title' => 'ул. Толстого, д. 49',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1995',
			'title' => 'ул. Толстого, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1996',
			'title' => 'ул. Толстого, д. 54',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1997',
			'title' => 'ул. Толстого, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1998',
			'title' => 'ул. Транзитная, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '1999',
			'title' => 'ул. Транзитная, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2000',
			'title' => 'ул. Транзитная, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2001',
			'title' => 'ул. Транзитная, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2002',
			'title' => 'ул. Транзитная, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2003',
			'title' => 'ул. Транзитная, д. 28',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2004',
			'title' => 'ул. Транзитная, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2005',
			'title' => 'ул. Транзитная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2006',
			'title' => 'ул. Транзитная, д. 44',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2007',
			'title' => 'ул. Транзитная, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2008',
			'title' => 'ул. Транзитная, д. 48',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2009',
			'title' => 'ул. Транзитная, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2010',
			'title' => 'ул. Турбинная, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2011',
			'title' => 'ул. Турбинная, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2012',
			'title' => 'ул. Урицкого, д. 100',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2013',
			'title' => 'ул. Урицкого, д. 108',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2014',
			'title' => 'ул. Урицкого, д. 115',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2015',
			'title' => 'ул. Урицкого, д. 125',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2016',
			'title' => 'ул. Урицкого, д. 125а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2017',
			'title' => 'ул. Урицкого, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2018',
			'title' => 'ул. Урицкого, д. 41',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2019',
			'title' => 'ул. Урицкого, д. 47',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2020',
			'title' => 'ул. Урицкого, д. 98',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2021',
			'title' => 'ул. Учумская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2022',
			'title' => 'ул. Ферганская, д. 21/10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2023',
			'title' => 'ул. Ферганская, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2024',
			'title' => 'ул. Ферганская, д. 4А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2025',
			'title' => 'ул. Ферганская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2026',
			'title' => 'ул. Ферганская, д. 6',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2027',
			'title' => 'ул. Ферганская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2028',
			'title' => 'ул. Ферганская, д. 9а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2029',
			'title' => 'ул. Фестивальная, д. 4',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2030',
			'title' => 'ул. Хабаровская 2-я, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2031',
			'title' => 'ул. Хабаровская 2-я, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2032',
			'title' => 'ул. Цимлянская, д. 39А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2033',
			'title' => 'ул. Чайковского, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2034',
			'title' => 'ул. Чайковского, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2035',
			'title' => 'ул. Чайковского, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2036',
			'title' => 'ул. Чайковского, д. 11А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2037',
			'title' => 'ул. Чайковского, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2038',
			'title' => 'ул. Чайковского, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2039',
			'title' => 'ул. Чайковского, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2040',
			'title' => 'ул. Чайковского, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2041',
			'title' => 'ул. Чайковского, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2042',
			'title' => 'ул. Чайковского, д. 8а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2043',
			'title' => 'ул. Чайковского, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2044',
			'title' => 'ул. Читинская, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2045',
			'title' => 'ул. Читинская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2046',
			'title' => 'ул. Читинская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2047',
			'title' => 'ул. Шахтеров, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2048',
			'title' => 'ул. Шахтеров, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2049',
			'title' => 'ул. Шелковая, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2050',
			'title' => 'ул. Шелковая, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2051',
			'title' => 'ул. Шелковая, д. 3а',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2052',
			'title' => 'ул. Ширинская, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2053',
			'title' => 'ул. Ширинская, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2054',
			'title' => 'ул. Ширинская, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2055',
			'title' => 'ул. Ширинская, д. 17',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2056',
			'title' => 'ул. Ширинская, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2057',
			'title' => 'ул. Ширинская, д. 3А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2058',
			'title' => 'ул. Ширинская, д. 5',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2059',
			'title' => 'ул. Ширинская, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2060',
			'title' => 'ул. Ширинская, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2061',
			'title' => 'ул. Щербакова, д. 16',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2062',
			'title' => 'ул. Щербакова, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2063',
			'title' => 'ул. Щербакова, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2064',
			'title' => 'ул. Щорса, д. 1',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2065',
			'title' => 'ул. Щорса, д. 10',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2066',
			'title' => 'ул. Щорса, д. 101',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2067',
			'title' => 'ул. Щорса, д. 11',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2068',
			'title' => 'ул. Щорса, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2069',
			'title' => 'ул. Щорса, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2070',
			'title' => 'ул. Щорса, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2071',
			'title' => 'ул. Щорса, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2072',
			'title' => 'ул. Щорса, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2073',
			'title' => 'ул. Щорса, д. 3',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2074',
			'title' => 'ул. Щорса, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2075',
			'title' => 'ул. Щорса, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2076',
			'title' => 'ул. Щорса, д. 50',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2077',
			'title' => 'ул. Щорса, д. 52',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2078',
			'title' => 'ул. Щорса, д. 53',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2079',
			'title' => 'ул. Щорса, д. 55',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2080',
			'title' => 'ул. Щорса, д. 56',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2081',
			'title' => 'ул. Щорса, д. 58',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2082',
			'title' => 'ул. Щорса, д. 61',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2083',
			'title' => 'ул. Щорса, д. 63',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2084',
			'title' => 'ул. Щорса, д. 67',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2085',
			'title' => 'ул. Щорса, д. 68',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2086',
			'title' => 'ул. Щорса, д. 7',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2087',
			'title' => 'ул. Щорса, д. 70',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2088',
			'title' => 'ул. Щорса, д. 72',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2089',
			'title' => 'ул. Щорса, д. 73',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2090',
			'title' => 'ул. Щорса, д. 74',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2091',
			'title' => 'ул. Щорса, д. 78',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2092',
			'title' => 'ул. Щорса, д. 80',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2093',
			'title' => 'ул. Щорса, д. 85',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2094',
			'title' => 'ул. Щорса, д. 86',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2095',
			'title' => 'ул. Щорса, д. 88',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2096',
			'title' => 'ул. Щорса, д. 89',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2097',
			'title' => 'ул. Щорса, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2098',
			'title' => 'ул. Щорса, д. 90',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2099',
			'title' => 'ул. Щорса, д. 93',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2100',
			'title' => 'ул. Щорса, д. 95',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2101',
			'title' => 'ул. Щорса, д. 99',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2102',
			'title' => 'ул. Энергетиков, д. 18',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2103',
			'title' => 'ул. Энергетиков, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2104',
			'title' => 'ул. Энергетиков, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2105',
			'title' => 'ул. Энергетиков, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2106',
			'title' => 'ул. Энергетиков, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2107',
			'title' => 'ул. Энергетиков, д. 34',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2108',
			'title' => 'ул. Энергетиков, д. 36',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2109',
			'title' => 'ул. Энергетиков, д. 42',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2110',
			'title' => 'ул. Энергетиков, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2111',
			'title' => 'ул. Энергетиков, д. 65',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2112',
			'title' => 'ул. Юности, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2113',
			'title' => 'ул. Юности, д. 13',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2114',
			'title' => 'ул. Юности, д. 14',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2115',
			'title' => 'ул. Юности, д. 15',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2116',
			'title' => 'ул. Юности, д. 19',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2117',
			'title' => 'ул. Юности, д. 2',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2118',
			'title' => 'ул. Юности, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2119',
			'title' => 'ул. Юности, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2120',
			'title' => 'ул. Юности, д. 24',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2121',
			'title' => 'ул. Юности, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2122',
			'title' => 'ул. Юности, д. 26',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2123',
			'title' => 'ул. Юности, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2124',
			'title' => 'ул. Юности, д. 29',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2125',
			'title' => 'ул. Юности, д. 31',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2126',
			'title' => 'ул. Юности, д. 33',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2127',
			'title' => 'ул. Юности, д. 35',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2128',
			'title' => 'ул. Юности, д. 37',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2129',
			'title' => 'ул. Юности, д. 37А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2130',
			'title' => 'ул. Юности, д. 39',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2131',
			'title' => 'ул. Юности, д. 39А',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2132',
			'title' => 'ул. Юности, д. 9',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2133',
			'title' => 'ул. Яковлева, д. 20',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2134',
			'title' => 'ул. Яковлева, д. 22',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2135',
			'title' => 'ул. Яковлева, д. 25',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2136',
			'title' => 'ул. Яковлева, д. 27',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2137',
			'title' => 'ул. Яковлева, д. 46',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2138',
			'title' => 'ул. Яковлева, д. 57',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2139',
			'title' => 'ул. Яковлева, д. 59',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2140',
			'title' => 'ул. Ястынская, д. 12',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2141',
			'title' => 'ул. Ястынская, д. 8',
			'company_id' => '1',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2142',
			'title' => 'б-р. Ботанический, д. 15',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2143',
			'title' => 'пер. Заводской, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2144',
			'title' => 'пер. Казарменный, д. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2145',
			'title' => 'пер. Тихий, д. 20',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2146',
			'title' => 'пер. Тихий, д. 22',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2147',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 163',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2148',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 67',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2149',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 88Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2150',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 91А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2151',
			'title' => 'пр-кт. Мира, д. 101',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2152',
			'title' => 'пр-кт. Мира, д. 105Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2153',
			'title' => 'пр-кт. Мира, д. 107',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2154',
			'title' => 'пр-кт. Мира, д. 109',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2155',
			'title' => 'пр-кт. Мира, д. 112',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2156',
			'title' => 'пр-кт. Мира, д. 140',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2157',
			'title' => 'пр-кт. Мира, д. 3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2158',
			'title' => 'пр-кт. Свободный, д. 42',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2159',
			'title' => 'пр-кт. Свободный, д. 50',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2160',
			'title' => 'пр-кт. Свободный, д. 50А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2161',
			'title' => 'пр-кт. Свободный, д. 52',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2162',
			'title' => 'пр-кт. Свободный, д. 54',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2163',
			'title' => 'пр-кт. Свободный, д. 58',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2164',
			'title' => 'пр-кт. Свободный, д. 62',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2165',
			'title' => 'пр-кт. Свободный, д. 64а',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2166',
			'title' => 'ул. 60 лет Октября, д. 40',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2167',
			'title' => 'ул. 60 лет Октября, д. 64',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2168',
			'title' => 'ул. 8 Марта, д. 18',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2169',
			'title' => 'ул. 9 Мая, д. 83, к. 1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2170',
			'title' => 'ул. 9 Мая, д. 83/2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2171',
			'title' => 'ул. Академика Киренского, д. 25А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2172',
			'title' => 'ул. Александра Матросова, д. 11',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2173',
			'title' => 'ул. Александра Матросова, д. 8А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2174',
			'title' => 'ул. Аэровокзальная, д. 1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2175',
			'title' => 'ул. Аэровокзальная, д. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2176',
			'title' => 'ул. Аэровокзальная, д. 2А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2177',
			'title' => 'ул. Аэровокзальная, д. 2Г',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2178',
			'title' => 'ул. Аэровокзальная, д. 2Е',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2179',
			'title' => 'ул. Аэровокзальная, д. 2Ж',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2180',
			'title' => 'ул. Аэровокзальная, д. 2з',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2181',
			'title' => 'ул. Аэровокзальная, д. 3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2182',
			'title' => 'ул. Аэровокзальная, д. 3А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2183',
			'title' => 'ул. Аэровокзальная, д. 3Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2184',
			'title' => 'ул. Аэровокзальная, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2185',
			'title' => 'ул. Аэровокзальная, д. 8Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2186',
			'title' => 'ул. Аэровокзальная, д. 8з',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2187',
			'title' => 'ул. Аэровокзальная, д. 8И',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2188',
			'title' => 'ул. Баумана, д. 24',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2189',
			'title' => 'ул. Баумана, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2190',
			'title' => 'ул. Баумана, д. 8',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2191',
			'title' => 'ул. Бограда, д. 116',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2192',
			'title' => 'ул. Бограда, д. 118',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2193',
			'title' => 'ул. Бограда, д. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2194',
			'title' => 'ул. Бограда, д. 89',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2195',
			'title' => 'ул. Братьев Абалаковых, д. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2196',
			'title' => 'ул. Волгоградская, д. 25',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2197',
			'title' => 'ул. Волгоградская, д. 27',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2198',
			'title' => 'ул. Волгоградская, д. 29',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2199',
			'title' => 'ул. Волгоградская, д. 31А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2200',
			'title' => 'ул. Воронова, д. 37',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2201',
			'title' => 'ул. Высотная, д. 19',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2202',
			'title' => 'ул. Высотная, д. 4А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2203',
			'title' => 'ул. Горького, д. 10',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2204',
			'title' => 'ул. Горького, д. 31',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2205',
			'title' => 'ул. Горького, д. 33',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2206',
			'title' => 'ул. Даурская, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2207',
			'title' => 'ул. Декабристов, д. 23',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2208',
			'title' => 'ул. Декабристов, д. 24/1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2209',
			'title' => 'ул. Декабристов, д. 24/2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2210',
			'title' => 'ул. Декабристов, д. 24/3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2211',
			'title' => 'ул. Декабристов, д. 36',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2212',
			'title' => 'ул. Декабристов, д. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2213',
			'title' => 'ул. Дорожная, д. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2214',
			'title' => 'ул. Железнодорожников, д. 10',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2215',
			'title' => 'ул. Железнодорожников, д. 10А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2216',
			'title' => 'ул. Железнодорожников, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2217',
			'title' => 'ул. Железнодорожников, д. 22А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2218',
			'title' => 'ул. Западная, д. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2219',
			'title' => 'ул. Ивана Забобонова, д. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2220',
			'title' => 'ул. Ивана Забобонова, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2221',
			'title' => 'ул. Ивана Забобонова, д. 8',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2222',
			'title' => 'ул. им А.С.Попова, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2223',
			'title' => 'ул. им А.С.Попова, д. 22',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2224',
			'title' => 'ул. им А.С.Попова, д. 6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2225',
			'title' => 'ул. им А.С.Попова, д. 8',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2226',
			'title' => 'ул. им А.С.Попова, д. 8А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2227',
			'title' => 'ул. им А.С.Попова, д. 8Г',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2228',
			'title' => 'ул. им Академика Вавилова, д. 39',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2229',
			'title' => 'ул. им Академика Вавилова, д. 48',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2230',
			'title' => 'ул. им Академика Вавилова, д. 86',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2231',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2232',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2233',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 14',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2234',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 28В',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2235',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2236',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 40',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2237',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 44А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2238',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 48',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2239',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 10А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2240',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 13',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2241',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 1А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2242',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 21',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2243',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 21А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2244',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 27А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2245',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 35А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2246',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2247',
			'title' => 'ул. им Шевченко, д. 88',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2248',
			'title' => 'ул. Калинина, д. 47',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2249',
			'title' => 'ул. Камская, д. 1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2250',
			'title' => 'ул. Камская, д. 3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2251',
			'title' => 'ул. Камская, д. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2252',
			'title' => 'ул. Карла Маркса, д. 127',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2253',
			'title' => 'ул. Карла Маркса, д. 132',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2254',
			'title' => 'ул. Карла Маркса, д. 14',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2255',
			'title' => 'ул. Карла Маркса, д. 172',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2256',
			'title' => 'ул. Карла Маркса, д. 175',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2257',
			'title' => 'ул. Карла Маркса, д. 49',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2258',
			'title' => 'ул. Кольцевая, д. 10Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2259',
			'title' => 'ул. Конституции СССР, д. 23',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2260',
			'title' => 'ул. Копылова, д. 36',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2261',
			'title' => 'ул. Копылова, д. 42',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2262',
			'title' => 'ул. Космонавта Быковского, д. 5Д',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2263',
			'title' => 'ул. Красной Армии, д. 14',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2264',
			'title' => 'ул. Красной Армии, д. 15',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2265',
			'title' => 'ул. Красномосковская, д. 34',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2266',
			'title' => 'ул. Краснофлотская 2-я, д. 7',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2267',
			'title' => 'ул. Краснофлотская 2-я, д. 9',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2268',
			'title' => 'ул. Крупской, д. 10Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2269',
			'title' => 'ул. Крупской, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2270',
			'title' => 'ул. Крупской, д. 12А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2271',
			'title' => 'ул. Крупской, д. 14',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2272',
			'title' => 'ул. Крупской, д. 24',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2273',
			'title' => 'ул. Крупской, д. 24А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2274',
			'title' => 'ул. Крупской, д. 26',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2275',
			'title' => 'ул. Крупской, д. 28А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2276',
			'title' => 'ул. Крупской, д. 34',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2277',
			'title' => 'ул. Крупской, д. 38',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2278',
			'title' => 'ул. Крупской, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2279',
			'title' => 'ул. Крупской, д. 40',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2280',
			'title' => 'ул. Крупской, д. 44',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2281',
			'title' => 'ул. Крупской, д. 4А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2282',
			'title' => 'ул. Куйбышева, д. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2283',
			'title' => 'ул. Курчатова, д. 10',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2284',
			'title' => 'ул. Кутузова, д. 101',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2285',
			'title' => 'ул. Кутузова, д. 54',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2286',
			'title' => 'ул. Кутузова, д. 91б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2287',
			'title' => 'ул. Кутузова, д. 99',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2288',
			'title' => 'ул. Ладо Кецховели, д. 24а, к. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2289',
			'title' => 'ул. Ладо Кецховели, д. 24а, к. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2290',
			'title' => 'ул. Ладо Кецховели, д. 24а, к. 6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2291',
			'title' => 'ул. Ладо Кецховели, д. 24а, к. 8',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2292',
			'title' => 'ул. Ладо Кецховели, д. 24а, к. 9',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2293',
			'title' => 'ул. Ленина, д. 133',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2294',
			'title' => 'ул. Ленина, д. 137',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2295',
			'title' => 'ул. Ленина, д. 143',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2296',
			'title' => 'ул. Ленина, д. 218',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2297',
			'title' => 'ул. Ленина, д. 43',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2298',
			'title' => 'ул. Лесная, д. 31',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2299',
			'title' => 'ул. Лесная, д. 33',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2300',
			'title' => 'ул. Лесная, д. 37',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2301',
			'title' => 'ул. Лесная, д. 41',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2302',
			'title' => 'ул. Лиды Прушинской, д. 5А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2303',
			'title' => 'ул. Ломоносова, д. 110',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2304',
			'title' => 'ул. Ломоносова, д. 112',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2305',
			'title' => 'ул. Ломоносова, д. 32',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2306',
			'title' => 'ул. Ломоносова, д. 41',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2307',
			'title' => 'ул. Ломоносова, д. 43',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2308',
			'title' => 'ул. Ломоносова, д. 64',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2309',
			'title' => 'ул. Ломоносова, д. 8',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2310',
			'title' => 'ул. Ломоносова, д. 86',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2311',
			'title' => 'ул. Ломоносова, д. 94, к. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2312',
			'title' => 'ул. Ломоносова, д. 94Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2313',
			'title' => 'ул. Маерчака, д. 31',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2314',
			'title' => 'ул. Маерчака, д. 33',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2315',
			'title' => 'ул. Маерчака, д. 35',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2316',
			'title' => 'ул. Маерчака, д. 37',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2317',
			'title' => 'ул. Маерчака, д. 43',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2318',
			'title' => 'ул. Маерчака, д. 45А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2319',
			'title' => 'ул. Менжинского, д. 6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2320',
			'title' => 'ул. Менжинского, д. 8',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2321',
			'title' => 'ул. Менжинского, д. 8А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2322',
			'title' => 'ул. Мечникова, д. 40',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2323',
			'title' => 'ул. Мичурина, д. 10',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2324',
			'title' => 'ул. Мичурина, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2325',
			'title' => 'ул. Можайского, д. 13',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2326',
			'title' => 'ул. Можайского, д. 15',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2327',
			'title' => 'ул. Можайского, д. 16',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2328',
			'title' => 'ул. Можайского, д. 17',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2329',
			'title' => 'ул. Можайского, д. 23',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2330',
			'title' => 'ул. Можайского, д. 5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2331',
			'title' => 'ул. Можайского, д. 6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2332',
			'title' => 'ул. Московская, д. 13',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2333',
			'title' => 'ул. Московская, д. 7',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2334',
			'title' => 'ул. Новая Заря, д. 11',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2335',
			'title' => 'ул. Новая Заря, д. 17',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2336',
			'title' => 'ул. Новая Заря, д. 9',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2337',
			'title' => 'ул. Озерная, д. 30/2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2338',
			'title' => 'ул. Озерная, д. 30/3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2339',
			'title' => 'ул. Озерная, д. 30/4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2340',
			'title' => 'ул. Озерная, д. 30/5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2341',
			'title' => 'ул. Озерная, д. 30/6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2342',
			'title' => 'ул. Озерная, д. 30/7',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2343',
			'title' => 'ул. Охраны Труда, д. 1а/1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2344',
			'title' => 'ул. Охраны Труда, д. 1а/5',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2345',
			'title' => 'ул. Парашютная, д. 70А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2346',
			'title' => 'ул. Парашютная, д. 9',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2347',
			'title' => 'ул. Партизана Железняка, д. 12',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2348',
			'title' => 'ул. Партизана Железняка, д. 12А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2349',
			'title' => 'ул. Партизана Железняка, д. 16А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2350',
			'title' => 'ул. Первых пионеров, д. 15',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2351',
			'title' => 'ул. Перенсона, д. 5а',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2352',
			'title' => 'ул. Путевая, д. 20',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2353',
			'title' => 'ул. Путевая, д. 3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2354',
			'title' => 'ул. Революции, д. 44',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2355',
			'title' => 'ул. Робеспьера, д. 20',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2356',
			'title' => 'ул. Робеспьера, д. 30',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2357',
			'title' => 'ул. Свердловская, д. 13',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2358',
			'title' => 'ул. Свердловская, д. 33',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2359',
			'title' => 'ул. Семафорная, д. 227',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2360',
			'title' => 'ул. Семафорная, д. 229',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2361',
			'title' => 'ул. Семафорная, д. 251',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2362',
			'title' => 'ул. Софьи Ковалевской, д. 3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2363',
			'title' => 'ул. Спортивная, д. 176',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2364',
			'title' => 'ул. Судостроительная, д. 123',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2365',
			'title' => 'ул. Техническая, д. 2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2366',
			'title' => 'ул. Техническая, д. 2А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2367',
			'title' => 'ул. Техническая, д. 2Б',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2368',
			'title' => 'ул. Урицкого, д. 50',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2369',
			'title' => 'ул. Фурманова, д. 100',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2370',
			'title' => 'ул. Фурманова, д. 96',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2371',
			'title' => 'ул. Фурманова, д. 98/1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2372',
			'title' => 'ул. Фурманова, д. 98/2',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2373',
			'title' => 'ул. Фурманова, д. 98/3',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2374',
			'title' => 'ул. Фурманова, д. 98/4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2375',
			'title' => 'ул. Хабаровская 1-я, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2376',
			'title' => 'ул. Хабаровская 1-я, д. 9',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2377',
			'title' => 'ул. Хабаровская 2-я, д. 1',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2378',
			'title' => 'ул. Хабаровская 2-я, д. 10',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2379',
			'title' => 'ул. Хабаровская 2-я, д. 12А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2380',
			'title' => 'ул. Хабаровская 2-я, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2381',
			'title' => 'ул. Хабаровская 2-я, д. 8А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2382',
			'title' => 'ул. Чкалова, д. 41А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2383',
			'title' => 'ул. Шахтеров, д. 6',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2384',
			'title' => 'ул. Шахтеров, д. 6А',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2385',
			'title' => 'ул. Щорса, д. 23',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2386',
			'title' => 'ул. Щорса, д. 4',
			'company_id' => '2',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2387',
			'title' => 'мкр. 4-й Центральный, д. 19, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2388',
			'title' => 'мкр. 6-й Северо-западный, д. 4, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2389',
			'title' => 'мкр. Северный, д. 13, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2390',
			'title' => 'мкр. Северный, д. 17, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2391',
			'title' => 'мкр. Северо-западный, д. 4, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2392',
			'title' => 'мкр. Северо-западный, д. 55, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2393',
			'title' => 'мкр. Солнечный, д. 54/1, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2394',
			'title' => 'мкр. Солнечный, д. 54/12, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2395',
			'title' => 'мкр. Солнечный, д. 54/13, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2396',
			'title' => 'мкр. Солнечный, д. 54/17, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2397',
			'title' => 'мкр. Солнечный, д. 54/24, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2398',
			'title' => 'мкр. Солнечный, д. 54/25, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2399',
			'title' => 'мкр. Солнечный, д. 54/3, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2400',
			'title' => 'мкр. Солнечный, д. 54/7, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2401',
			'title' => 'мкр. Солнечный, д. 54/9, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2402',
			'title' => 'п. Мелькомбината, д. 33, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2403',
			'title' => 'п. Ремзавода, д. 16, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2404',
			'title' => 'п. Ремзавода, д. 19, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2405',
			'title' => 'п. Ремзавода, д. 3, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2406',
			'title' => 'п. Ремзавода, д. 6, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2407',
			'title' => 'пер. Больничный, д. 6, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2408',
			'title' => 'пер. Индустриальный, д. 2/1, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2409',
			'title' => 'ул. 30 лет ВЛКСМ, д. 20/1, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2410',
			'title' => 'ул. 30 лет ВЛКСМ, д. 21, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2411',
			'title' => 'ул. 30 лет ВЛКСМ, д. 28, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2412',
			'title' => 'ул. 40 лет Октября, д. 54, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2413',
			'title' => 'ул. 40 лет Октября, д. 74, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2414',
			'title' => 'ул. Ангарская, д. 12, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2415',
			'title' => 'ул. Гаражная, д. 20/12, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2416',
			'title' => 'ул. Гаражная, д. 20/9, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2417',
			'title' => 'ул. Герцена 9-я, д. 1, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2418',
			'title' => 'ул. Герцена 9-я, д. 10, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2419',
			'title' => 'ул. Герцена 9-я, д. 14, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2420',
			'title' => 'ул. Герцена 9-я, д. 15, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2421',
			'title' => 'ул. Герцена 9-я, д. 16, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2422',
			'title' => 'ул. Герцена 9-я, д. 19, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2423',
			'title' => 'ул. Герцена 9-я, д. 2, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2424',
			'title' => 'ул. Герцена 9-я, д. 20, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2425',
			'title' => 'ул. Герцена 9-я, д. 21, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2426',
			'title' => 'ул. Герцена 9-я, д. 22, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2427',
			'title' => 'ул. Герцена 9-я, д. 24, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2428',
			'title' => 'ул. Герцена 9-я, д. 25, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2429',
			'title' => 'ул. Герцена 9-я, д. 28, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2430',
			'title' => 'ул. Герцена 9-я, д. 3, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2431',
			'title' => 'ул. Герцена 9-я, д. 4, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2432',
			'title' => 'ул. Герцена 9-я, д. 5, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2433',
			'title' => 'ул. Герцена 9-я, д. 6, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2434',
			'title' => 'ул. Герцена 9-я, д. 8, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2435',
			'title' => 'ул. Герцена, д. 7, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2436',
			'title' => 'ул. Горького, д. 40, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2437',
			'title' => 'ул. Горького, д. 46, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2438',
			'title' => 'ул. Иланская, д. 3, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2439',
			'title' => 'ул. Красноярская, д. 27, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2440',
			'title' => 'ул. Красноярская, д. 33, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2441',
			'title' => 'ул. Красноярская, д. 4, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2442',
			'title' => 'ул. Красноярская, д. 5, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2443',
			'title' => 'ул. Красноярская, д. 6, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2444',
			'title' => 'ул. Ленина, д. 11, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2445',
			'title' => 'ул. Ленина, д. 9, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2446',
			'title' => 'ул. Парижской Коммуны, д. 43, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2447',
			'title' => 'ул. Садовая, д. 1/1, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2448',
			'title' => 'ул. Сибирская, д. 11, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2449',
			'title' => 'ул. Сибирская, д. 12, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2450',
			'title' => 'ул. Сибирская, д. 14, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2451',
			'title' => 'ул. Сибирская, д. 16, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2452',
			'title' => 'ул. Сибирская, д. 18, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2453',
			'title' => 'ул. Ушакова, д. 4, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2454',
			'title' => 'ул. Эйдемана, д. 10, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2455',
			'title' => 'ул. Эйдемана, д. 12, Канск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2456',
			'title' => 'мкр. 1-й, д. 1, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2457',
			'title' => 'мкр. 1-й, д. 10, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2458',
			'title' => 'мкр. 1-й, д. 11, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2459',
			'title' => 'мкр. 1-й, д. 12, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2460',
			'title' => 'мкр. 1-й, д. 13, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2461',
			'title' => 'мкр. 1-й, д. 14, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2462',
			'title' => 'мкр. 1-й, д. 18, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2463',
			'title' => 'мкр. 1-й, д. 2, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2464',
			'title' => 'мкр. 1-й, д. 25, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2465',
			'title' => 'мкр. 1-й, д. 26, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2466',
			'title' => 'мкр. 1-й, д. 27, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2467',
			'title' => 'мкр. 1-й, д. 3, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2468',
			'title' => 'мкр. 1-й, д. 4, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2469',
			'title' => 'мкр. 1-й, д. 5, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2470',
			'title' => 'мкр. 1-й, д. 6, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2471',
			'title' => 'мкр. 1-й, д. 7, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2472',
			'title' => 'мкр. 1-й, д. 9, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2473',
			'title' => 'мкр. 3-й, д. 1, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2474',
			'title' => 'мкр. 3-й, д. 10, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2475',
			'title' => 'мкр. 3-й, д. 11, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2476',
			'title' => 'мкр. 3-й, д. 13, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2477',
			'title' => 'мкр. 3-й, д. 14, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2478',
			'title' => 'мкр. 3-й, д. 16, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2479',
			'title' => 'мкр. 3-й, д. 17, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2480',
			'title' => 'мкр. 3-й, д. 18, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2481',
			'title' => 'мкр. 3-й, д. 19, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2482',
			'title' => 'мкр. 3-й, д. 2, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2483',
			'title' => 'мкр. 3-й, д. 20, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2484',
			'title' => 'мкр. 3-й, д. 21, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2485',
			'title' => 'мкр. 3-й, д. 22, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2486',
			'title' => 'мкр. 3-й, д. 23, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2487',
			'title' => 'мкр. 3-й, д. 24, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2488',
			'title' => 'мкр. 3-й, д. 25, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2489',
			'title' => 'мкр. 3-й, д. 26, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2490',
			'title' => 'мкр. 3-й, д. 27, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2491',
			'title' => 'мкр. 3-й, д. 3, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2492',
			'title' => 'мкр. 3-й, д. 4, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2493',
			'title' => 'мкр. 3-й, д. 5, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2494',
			'title' => 'мкр. 3-й, д. 6, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2495',
			'title' => 'мкр. 3-й, д. 7, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2496',
			'title' => 'мкр. 3-й, д. 8, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2497',
			'title' => 'мкр. 3-й, д. 8а, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2498',
			'title' => 'мкр. 4-й, д. 22, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2499',
			'title' => 'мкр. 4-й, д. 23, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2500',
			'title' => 'мкр. 4-й, д. 24, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2501',
			'title' => 'мкр. 4-й, д. 28, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2502',
			'title' => 'мкр. 4-й, д. 29, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2503',
			'title' => 'мкр. 5-й, д. 1, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2504',
			'title' => 'мкр. 5-й, д. 2, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2505',
			'title' => 'мкр. 7-й, д. 10, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2506',
			'title' => 'мкр. 7-й, д. 12, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2507',
			'title' => 'мкр. 7-й, д. 15, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2508',
			'title' => 'мкр. 7-й, д. 9, Шарыпово',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2509',
			'title' => 'пер. Молодежный, д. 1, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2510',
			'title' => 'ул. 19 съезда ВЛКСМ, д. 1, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2511',
			'title' => 'ул. 19 съезда ВЛКСМ, д. 5, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2512',
			'title' => 'ул. 19 съезда ВЛКСМ, д. 7, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2513',
			'title' => 'ул. 19 съезда ВЛКСМ, д. 9, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2514',
			'title' => 'ул. 9 Мая, д. 2, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2515',
			'title' => 'ул. Кишиневская, д. 1, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2516',
			'title' => 'ул. Кишиневская, д. 11, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2517',
			'title' => 'ул. Кишиневская, д. 13, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2518',
			'title' => 'ул. Кишиневская, д. 15, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2519',
			'title' => 'ул. Кишиневская, д. 3, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2520',
			'title' => 'ул. Кишиневская, д. 5, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2521',
			'title' => 'ул. Кишиневская, д. 7, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2522',
			'title' => 'ул. Кишиневская, д. 9, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2523',
			'title' => 'ул. Комсомольская, д. 16, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2524',
			'title' => 'ул. Комсомольская, д. 18, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2525',
			'title' => 'ул. Комсомольская, д. 20, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2526',
			'title' => 'ул. Молодогвардейцев, д. 10, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2527',
			'title' => 'ул. Молодогвардейцев, д. 12, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2528',
			'title' => 'ул. Молодогвардейцев, д. 2, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2529',
			'title' => 'ул. Молодогвардейцев, д. 4, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2530',
			'title' => 'ул. Молодогвардейцев, д. 6, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2531',
			'title' => 'ул. Пионеров КАТЭКа, д. 11, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2532',
			'title' => 'ул. Пионеров КАТЭКа, д. 4, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2533',
			'title' => 'ул. Пионеров КАТЭКа, д. 6, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2534',
			'title' => 'ул. Пионеров КАТЭКа, д. 6а, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2535',
			'title' => 'ул. Пионеров КАТЭКа, д. 9, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2536',
			'title' => 'ул. Труда, д. 2, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2537',
			'title' => 'ул. Шахтерская, д. 1, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2538',
			'title' => 'ул. Шахтерская, д. 2а, Дубинино',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2539',
			'title' => 'пр-кт. Ленинского Комсомола, д. 12, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2540',
			'title' => 'пр-кт. Ленинского Комсомола, д. 14, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2541',
			'title' => 'пр-кт. Ленинского Комсомола, д. 14/1, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2542',
			'title' => 'пр-кт. Ленинского Комсомола, д. 18, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2543',
			'title' => 'пр-кт. Ленинского Комсомола, д. 22, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2544',
			'title' => 'пр-кт. Ленинского Комсомола, д. 22/1, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2545',
			'title' => 'пр-кт. Ленинского Комсомола, д. 3, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2546',
			'title' => 'пр-кт. Ленинского Комсомола, д. 4, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2547',
			'title' => 'пр-кт. Ленинского Комсомола, д. 7, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2548',
			'title' => 'ул. Гайнулина, д. 2, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2549',
			'title' => 'ул. Гайнулина, д. 2А, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2550',
			'title' => 'ул. Гайнулина, д. 4, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2551',
			'title' => 'ул. Гайнулина, д. 8, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2552',
			'title' => 'ул. Гидростроителей, д. 11, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2553',
			'title' => 'ул. Гидростроителей, д. 11а, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2554',
			'title' => 'ул. Гидростроителей, д. 11б, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2555',
			'title' => 'ул. Гидростроителей, д. 13, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2556',
			'title' => 'ул. Гидростроителей, д. 15, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2557',
			'title' => 'ул. Гидростроителей, д. 3, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2558',
			'title' => 'ул. Гидростроителей, д. 5, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2559',
			'title' => 'ул. Колесниченко, д. 10, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2560',
			'title' => 'ул. Колесниченко, д. 12, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2561',
			'title' => 'ул. Колесниченко, д. 14, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2562',
			'title' => 'ул. Колесниченко, д. 2, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2563',
			'title' => 'ул. Колесниченко, д. 22, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2564',
			'title' => 'ул. Колесниченко, д. 4, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2565',
			'title' => 'ул. Колесниченко, д. 4а, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2566',
			'title' => 'ул. Колесниченко, д. 8, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2567',
			'title' => 'ул. Михайлова, д. 1, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2568',
			'title' => 'ул. Михайлова, д. 11, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2569',
			'title' => 'ул. Михайлова, д. 12, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2570',
			'title' => 'ул. Михайлова, д. 3, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2571',
			'title' => 'ул. Михайлова, д. 5, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2572',
			'title' => 'ул. Михайлова, д. 6, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2573',
			'title' => 'ул. Усенко, д. 2, Кодинск',
			'company_id' => '3',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2574',
			'title' => 'пер. Тихий, д. 11',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2575',
			'title' => 'пр-кт. Ульяновский, д. 32Б',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2576',
			'title' => 'ул. 60 лет Октября, д. 22',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2577',
			'title' => 'ул. 60 лет Октября, д. 65',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2578',
			'title' => 'ул. Алеши Тимошенкова, д. 155',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2579',
			'title' => 'ул. Базарная, д. 128',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2580',
			'title' => 'ул. Бийская, д. 5',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2581',
			'title' => 'ул. Бийская, д. 6',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2582',
			'title' => 'ул. Волжская, д. 34',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2583',
			'title' => 'ул. Волжская, д. 53',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2584',
			'title' => 'ул. Гастелло, д. 34',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2585',
			'title' => 'ул. Грунтовая, д. 10',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2586',
			'title' => 'ул. Грунтовая, д. 12',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2587',
			'title' => 'ул. Грунтовая, д. 4',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2588',
			'title' => 'ул. Грунтовая, д. 6',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2589',
			'title' => 'ул. Грунтовая, д. 8',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2590',
			'title' => 'ул. Добролюбова, д. 13',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2591',
			'title' => 'ул. Добролюбова, д. 15',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2592',
			'title' => 'ул. Добролюбова, д. 17',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2593',
			'title' => 'ул. Добролюбова, д. 23',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2594',
			'title' => 'ул. Инициаторов, д. 13',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2595',
			'title' => 'ул. Квартальная, д. 2',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2596',
			'title' => 'ул. Квартальная, д. 7',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2597',
			'title' => 'ул. Коммунальная, д. 6',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2598',
			'title' => 'ул. Коммунальная, д. 8',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2599',
			'title' => 'ул. Котовского, д. 18',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2600',
			'title' => 'ул. Котовского, д. 20',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2601',
			'title' => 'ул. Котовского, д. 22',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2602',
			'title' => 'ул. Котовского, д. 23',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2603',
			'title' => 'ул. Котовского, д. 24',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2604',
			'title' => 'ул. Котовского, д. 25',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2605',
			'title' => 'ул. Котовского, д. 26',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2606',
			'title' => 'ул. Котовского, д. 27',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2607',
			'title' => 'ул. Котовского, д. 28',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2608',
			'title' => 'ул. Кутузова, д. 25',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2609',
			'title' => 'ул. Кутузова, д. 35',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2610',
			'title' => 'ул. Кутузова, д. 37',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2611',
			'title' => 'ул. Кутузова, д. 39',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2612',
			'title' => 'ул. Кутузова, д. 41',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2613',
			'title' => 'ул. Кутузова, д. 43',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2614',
			'title' => 'ул. Кутузова, д. 45',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2615',
			'title' => 'ул. Кутузова, д. 47',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2616',
			'title' => 'ул. Кутузова, д. 49',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2617',
			'title' => 'ул. Кутузова, д. 51',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2618',
			'title' => 'ул. Кутузова, д. 53',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2619',
			'title' => 'ул. Кутузова, д. 55',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2620',
			'title' => 'ул. Кутузова, д. 57',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2621',
			'title' => 'ул. Кутузова, д. 59',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2622',
			'title' => 'ул. Кутузова, д. 61',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2623',
			'title' => 'ул. Кутузова, д. 63',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2624',
			'title' => 'ул. Кутузова, д. 65',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2625',
			'title' => 'ул. Кутузова, д. 69',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2626',
			'title' => 'ул. Линейная, д. 82',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2627',
			'title' => 'ул. Львовская, д. 15А',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2628',
			'title' => 'ул. Львовская, д. 40',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2629',
			'title' => 'ул. Львовская, д. 47А',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2630',
			'title' => 'ул. Монтажников, д. 10',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2631',
			'title' => 'ул. Монтажников, д. 12',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2632',
			'title' => 'ул. Монтажников, д. 14',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2633',
			'title' => 'ул. Монтажников, д. 18',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2634',
			'title' => 'ул. Монтажников, д. 4',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2635',
			'title' => 'ул. Монтажников, д. 6',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2636',
			'title' => 'ул. Монтажников, д. 8',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2637',
			'title' => 'ул. Нерчинская, д. 1',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2638',
			'title' => 'ул. Нерчинская, д. 2',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2639',
			'title' => 'ул. Нерчинская, д. 4',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2640',
			'title' => 'ул. Новая, д. 42',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2641',
			'title' => 'ул. Новая, д. 44',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2642',
			'title' => 'ул. Новая, д. 54',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2643',
			'title' => 'ул. Новая, д. 56',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2644',
			'title' => 'ул. Новая, д. 64',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2645',
			'title' => 'ул. Новая, д. 66',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2646',
			'title' => 'ул. Побежимова, д. 47',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2647',
			'title' => 'ул. Свердловская, д. 203',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2648',
			'title' => 'ул. Свободная, д. 11',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2649',
			'title' => 'ул. Свободная, д. 1А',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2650',
			'title' => 'ул. Свободная, д. 3',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2651',
			'title' => 'ул. Свободная, д. 3А',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2652',
			'title' => 'ул. Свободная, д. 5',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2653',
			'title' => 'ул. Свободная, д. 7',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2654',
			'title' => 'ул. Семафорная, д. 119',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2655',
			'title' => 'ул. Семафорная, д. 221',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2656',
			'title' => 'ул. Семафорная, д. 223',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2657',
			'title' => 'ул. Семафорная, д. 295',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2658',
			'title' => 'ул. Семафорная, д. 299',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2659',
			'title' => 'ул. Семафорная, д. 331',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2660',
			'title' => 'ул. Семафорная, д. 333',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2661',
			'title' => 'ул. Семафорная, д. 341',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2662',
			'title' => 'ул. Семафорная, д. 343',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2663',
			'title' => 'ул. Семафорная, д. 347',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2664',
			'title' => 'ул. Семафорная, д. 349',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2665',
			'title' => 'ул. Семафорная, д. 351',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2666',
			'title' => 'ул. Семафорная, д. 353',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2667',
			'title' => 'ул. Семафорная, д. 371',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2668',
			'title' => 'ул. Семафорная, д. 373',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2669',
			'title' => 'ул. Семафорная, д. 375',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2670',
			'title' => 'ул. Семафорная, д. 377',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2671',
			'title' => 'ул. Семафорная, д. 379',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2672',
			'title' => 'ул. Семафорная, д. 383',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2673',
			'title' => 'ул. Семафорная, д. 385',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2674',
			'title' => 'ул. Семафорная, д. 387',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2675',
			'title' => 'ул. Семафорная, д. 389',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2676',
			'title' => 'ул. Семафорная, д. 391',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2677',
			'title' => 'ул. Семафорная, д. 393',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2678',
			'title' => 'ул. Семафорная, д. 421',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2679',
			'title' => 'ул. Семафорная, д. 439, к. 1',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2680',
			'title' => 'ул. Семафорная, д. 439, к. 2',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2681',
			'title' => 'ул. Семафорная, д. 439, к. 4',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2682',
			'title' => 'ул. Станочная, д. 10',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2683',
			'title' => 'ул. Станочная, д. 11',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2684',
			'title' => 'ул. Станочная, д. 2',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2685',
			'title' => 'ул. Станочная, д. 4',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2686',
			'title' => 'ул. Станочная, д. 6',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2687',
			'title' => 'ул. Станочная, д. 7',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2688',
			'title' => 'ул. Станочная, д. 8',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2689',
			'title' => 'ул. Станочная, д. 9',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2690',
			'title' => 'ул. Стрелочная, д. 15',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2691',
			'title' => 'ул. Транзитная, д. 43',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2692',
			'title' => 'ул. Турбинная, д. 12',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2693',
			'title' => 'ул. Читинская, д. 1',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2694',
			'title' => 'ул. Щербакова, д. 13',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2695',
			'title' => 'ул. Щербакова, д. 21',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2696',
			'title' => 'ул. Щербакова, д. 29',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2697',
			'title' => 'ул. Щербакова, д. 39',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2698',
			'title' => 'ул. Щербакова, д. 41',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2699',
			'title' => 'ул. Щербакова, д. 44',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2700',
			'title' => 'ул. Щербакова, д. 47',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2701',
			'title' => 'ул. Щербакова, д. 51',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2702',
			'title' => 'ул. Щорса, д. 6',
			'company_id' => '4',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2703',
			'title' => 'пер. Водометный, д. 2',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2704',
			'title' => 'пер. Водометный, д. 4',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2705',
			'title' => 'пер. Водометный, д. 5',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2706',
			'title' => 'пер. Кривоколенный, д. 3',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2707',
			'title' => 'пер. Кривоколенный, д. 5',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2708',
			'title' => 'пер. Медицинский, д. 14',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2709',
			'title' => 'пер. Медицинский, д. 16',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2710',
			'title' => 'пер. Медицинский, д. 16А',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2711',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 110',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2712',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 133А',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2713',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 154, к. 1',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2714',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 154, к. 2',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2715',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 166',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2716',
			'title' => 'пр-кт. Металлургов, д. 28В',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2717',
			'title' => 'проезд. Центральный, д. 10',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2718',
			'title' => 'проезд. Центральный, д. 12',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2719',
			'title' => 'проезд. Центральный, д. 14',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2720',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 19',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2721',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 25',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2722',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 27а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2723',
			'title' => 'ул. 5 Участок, д. 1',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2724',
			'title' => 'ул. 5 Участок, д. 2',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2725',
			'title' => 'ул. 52 Квартал, д. 1',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2726',
			'title' => 'ул. 52 Квартал, д. 2',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2727',
			'title' => 'ул. 60 лет Октября, д. 11',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2728',
			'title' => 'ул. 60 лет Октября, д. 13',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2729',
			'title' => 'ул. 60 лет Октября, д. 139',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2730',
			'title' => 'ул. 60 лет Октября, д. 14',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2731',
			'title' => 'ул. 60 лет Октября, д. 145',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2732',
			'title' => 'ул. 60 лет Октября, д. 15',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2733',
			'title' => 'ул. 60 лет Октября, д. 151',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2734',
			'title' => 'ул. 60 лет Октября, д. 55',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2735',
			'title' => 'ул. 60 лет Октября, д. 69',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2736',
			'title' => 'ул. 60 лет Октября, д. 80',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2737',
			'title' => 'ул. 60 лет Октября, д. 9',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2738',
			'title' => 'ул. Автомобилистов, д. 68',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2739',
			'title' => 'ул. Автомобилистов, д. 70а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2740',
			'title' => 'ул. Автомобилистов, д. 70а, к. 1',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2741',
			'title' => 'ул. Александра Матросова, д. 30/3',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2742',
			'title' => 'ул. Аральская, д. 16',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2743',
			'title' => 'ул. Аэровокзальная, д. 8ж',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2744',
			'title' => 'ул. Васнецова, д. 25',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2745',
			'title' => 'ул. Верхняя, д. 3б',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2746',
			'title' => 'ул. Воронова, д. 12Г',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2747',
			'title' => 'ул. Гастелло, д. 27',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2748',
			'title' => 'ул. Джамбульская, д. 2В',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2749',
			'title' => 'ул. Джамбульская, д. 2Д',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2750',
			'title' => 'ул. Джамбульская, д. 4А',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2751',
			'title' => 'ул. Западная, д. 3',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2752',
			'title' => 'ул. им Академика Вавилова, д. 94',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2753',
			'title' => 'ул. им Шевченко, д. 68',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2754',
			'title' => 'ул. им Шевченко, д. 70',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2755',
			'title' => 'ул. им Шевченко, д. 70а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2756',
			'title' => 'ул. Инструментальная, д. 3',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2757',
			'title' => 'ул. Ключевская, д. 57',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2758',
			'title' => 'ул. Ключевская, д. 59',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2759',
			'title' => 'ул. Корнетова Дружинника, д. 10',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2760',
			'title' => 'ул. Корнетова Дружинника, д. 12',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2761',
			'title' => 'ул. Королева, д. 11',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2762',
			'title' => 'ул. Королева, д. 14',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2763',
			'title' => 'ул. Красногорская 2-я, д. 21д',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2764',
			'title' => 'ул. Краснопресненская, д. 10',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2765',
			'title' => 'ул. Краснопресненская, д. 13',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2766',
			'title' => 'ул. Краснопресненская, д. 14',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2767',
			'title' => 'ул. Краснопресненская, д. 15',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2768',
			'title' => 'ул. Краснопресненская, д. 17',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2769',
			'title' => 'ул. Краснопресненская, д. 18',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2770',
			'title' => 'ул. Краснопресненская, д. 22',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2771',
			'title' => 'ул. Краснопресненская, д. 24',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2772',
			'title' => 'ул. Краснопресненская, д. 25',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2773',
			'title' => 'ул. Краснопресненская, д. 26',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2774',
			'title' => 'ул. Краснопресненская, д. 29',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2775',
			'title' => 'ул. Краснопресненская, д. 30',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2776',
			'title' => 'ул. Краснопресненская, д. 31',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2777',
			'title' => 'ул. Краснопресненская, д. 34',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2778',
			'title' => 'ул. Краснопресненская, д. 35',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2779',
			'title' => 'ул. Краснопресненская, д. 36',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2780',
			'title' => 'ул. Кутузова, д. 19',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2781',
			'title' => 'ул. Кутузова, д. 21',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2782',
			'title' => 'ул. Кутузова, д. 23',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2783',
			'title' => 'ул. Ломоносова, д. 42',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2784',
			'title' => 'ул. Ломоносова, д. 68',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2785',
			'title' => 'ул. Маерчака, д. 5',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2786',
			'title' => 'ул. Маерчака, д. 7',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2787',
			'title' => 'ул. Маршала К.Рокоссовского, д. 18',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2788',
			'title' => 'ул. Мичурина, д. 5В',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2789',
			'title' => 'ул. Новая, д. 10',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2790',
			'title' => 'ул. Новая, д. 12',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2791',
			'title' => 'ул. Новая, д. 16',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2792',
			'title' => 'ул. Новая, д. 18',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2793',
			'title' => 'ул. Новая, д. 22',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2794',
			'title' => 'ул. Новая, д. 24',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2795',
			'title' => 'ул. Новая, д. 26',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2796',
			'title' => 'ул. Новая, д. 28',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2797',
			'title' => 'ул. Новая, д. 32',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2798',
			'title' => 'ул. Новая, д. 34',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2799',
			'title' => 'ул. Новгородская, д. 1А',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2800',
			'title' => 'ул. Парашютная, д. 21',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2801',
			'title' => 'ул. Парашютная, д. 23',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2802',
			'title' => 'ул. Писателя Н.Устиновича, д. 9а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2803',
			'title' => 'ул. Ползунова, д. 18',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2804',
			'title' => 'ул. Рейдовая, д. 46',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2805',
			'title' => 'ул. Рейдовая, д. 57А',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2806',
			'title' => 'ул. Рейдовая, д. 74',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2807',
			'title' => 'ул. Рейдовая, д. 74, к. 1',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2808',
			'title' => 'ул. Северо-Енисейская, д. 48а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2809',
			'title' => 'ул. Тамбовская, д. 23а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2810',
			'title' => 'ул. Тельмана, д. 24',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2811',
			'title' => 'ул. Тельмана, д. 32а',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2812',
			'title' => 'ул. Тобольская, д. 5',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2813',
			'title' => 'ул. Щербакова, д. 55',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2814',
			'title' => 'ул. Щорса, д. 60',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2815',
			'title' => 'ул. Щорса, д. 62',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2816',
			'title' => 'ул. Щорса, д. 66',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2817',
			'title' => 'ул. Энергетиков, д. 56',
			'company_id' => '5',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2818',
			'title' => 'ул. 11-я Продольная, д. 21',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2819',
			'title' => 'ул. 2-я Линейная, д. 14',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2820',
			'title' => 'ул. 2-я Линейная, д. 7',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2821',
			'title' => 'ул. 3-я Линейная, д. 11',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2822',
			'title' => 'ул. 3-я Линейная, д. 2',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2823',
			'title' => 'ул. 5-я Линейная, д. 11',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2824',
			'title' => 'ул. 5-я Линейная, д. 15',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2825',
			'title' => 'ул. 5-я Полярная, д. 15',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2826',
			'title' => 'ул. 5-я Полярная, д. 17',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2827',
			'title' => 'ул. Ады Лебедевой, д. 50',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2828',
			'title' => 'ул. Базайская, д. 102',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2829',
			'title' => 'ул. Базайская, д. 106',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2830',
			'title' => 'ул. Базайская, д. 108',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2831',
			'title' => 'ул. Базайская, д. 112',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2832',
			'title' => 'ул. Базайская, д. 64',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2833',
			'title' => 'ул. Базайская, д. 74',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2834',
			'title' => 'ул. Базайская, д. 84',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2835',
			'title' => 'ул. Базайская, д. 90',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2836',
			'title' => 'ул. Базайская, д. 94',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2837',
			'title' => 'ул. Базайская, д. 96',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2838',
			'title' => 'ул. Березина, д. 152',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2839',
			'title' => 'ул. Березина, д. 154',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2840',
			'title' => 'ул. Березина, д. 170',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2841',
			'title' => 'ул. Березина, д. 172',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2842',
			'title' => 'ул. Березина, д. 174',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2843',
			'title' => 'ул. Васнецова, д. 33',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2844',
			'title' => 'ул. Веселая, д. 2Б',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2845',
			'title' => 'ул. Енисейская, д. 6, к. а',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2846',
			'title' => 'ул. Загородная, д. 35',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2847',
			'title' => 'ул. Загородная, д. 53а',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2848',
			'title' => 'ул. Загородная, д. 53д',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2849',
			'title' => 'ул. Зои Космодемьянской, д. 78',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2850',
			'title' => 'ул. Зои Космодемьянской, д. 82',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2851',
			'title' => 'ул. Красноградская, д. 12',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2852',
			'title' => 'ул. Красноградская, д. 19',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2853',
			'title' => 'ул. Красноградская, д. 21',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2854',
			'title' => 'ул. Ладо Кецховели, д. 58Б',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2855',
			'title' => 'ул. Ленина, д. 74',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2856',
			'title' => 'ул. МПС, д. 17',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2857',
			'title' => 'ул. МПС, д. 18',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2858',
			'title' => 'ул. МПС, д. 27',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2859',
			'title' => 'ул. МПС, д. 39',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2860',
			'title' => 'ул. Парижской Коммуны, д. 42',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2861',
			'title' => 'ул. Побежимова, д. 48',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2862',
			'title' => 'ул. Полтавская, д. 30',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2863',
			'title' => 'ул. Полтавская, д. 32',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2864',
			'title' => 'ул. Полтавская, д. 34',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2865',
			'title' => 'ул. Полярная, д. 100',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2866',
			'title' => 'ул. Полярная, д. 103',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2867',
			'title' => 'ул. Полярная, д. 115',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2868',
			'title' => 'ул. Полярная, д. 123',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2869',
			'title' => 'ул. Полярная, д. 125',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2870',
			'title' => 'ул. Полярная, д. 137',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2871',
			'title' => 'ул. Полярная, д. 139',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2872',
			'title' => 'ул. Рогова, д. 17',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2873',
			'title' => 'ул. Свердловская, д. 104',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2874',
			'title' => 'ул. Свердловская, д. 106',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2875',
			'title' => 'ул. Свердловская, д. 106/2',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2876',
			'title' => 'ул. Свердловская, д. 108',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2877',
			'title' => 'ул. Свердловская, д. 110',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2878',
			'title' => 'ул. Свердловская, д. 112',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2879',
			'title' => 'ул. Свердловская, д. 114А',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2880',
			'title' => 'ул. Свердловская, д. 116',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2881',
			'title' => 'ул. Свердловская, д. 120',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2882',
			'title' => 'ул. Свердловская, д. 122',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2883',
			'title' => 'ул. Свердловская, д. 205',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2884',
			'title' => 'ул. Свердловская, д. 207',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2885',
			'title' => 'ул. Свердловская, д. 209',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2886',
			'title' => 'ул. Свердловская, д. 211',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2887',
			'title' => 'ул. Свердловская, д. 213',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2888',
			'title' => 'ул. Свердловская, д. 215',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2889',
			'title' => 'ул. Свердловская, д. 219',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2890',
			'title' => 'ул. Свердловская, д. 225',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2891',
			'title' => 'ул. Свердловская, д. 227',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2892',
			'title' => 'ул. Свердловская, д. 229',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2893',
			'title' => 'ул. Свердловская, д. 237',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2894',
			'title' => 'ул. Свердловская, д. 243',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2895',
			'title' => 'ул. Свердловская, д. 259',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2896',
			'title' => 'ул. Свердловская, д. 78',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2897',
			'title' => 'ул. Свердловская, д. 80',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2898',
			'title' => 'ул. Свердловская, д. 82',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2899',
			'title' => 'ул. Свердловская, д. 86',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2900',
			'title' => 'ул. Свердловская, д. 88',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2901',
			'title' => 'ул. Свердловская, д. 90',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2902',
			'title' => 'ул. Свердловская, д. 92',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2903',
			'title' => 'ул. Складская, д. 27',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2904',
			'title' => 'ул. Стрелочная, д. 1',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2905',
			'title' => 'ул. Стрелочная, д. 17',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2906',
			'title' => 'ул. Стрелочная, д. 19',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2907',
			'title' => 'ул. Стрелочная, д. 20А',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2908',
			'title' => 'ул. Стрелочная, д. 22',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2909',
			'title' => 'ул. Стрелочная, д. 24',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2910',
			'title' => 'ул. Стрелочная, д. 26',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2911',
			'title' => 'ул. Стрелочная, д. 28',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2912',
			'title' => 'ул. Судостроительная, д. 12',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2913',
			'title' => 'ул. Судостроительная, д. 14',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2914',
			'title' => 'ул. Судостроительная, д. 25',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2915',
			'title' => 'ул. Судостроительная, д. 2А',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2916',
			'title' => 'ул. Судостроительная, д. 4А',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2917',
			'title' => 'ул. Судостроительная, д. 6А',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2918',
			'title' => 'ул. Туристская, д. 68',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2919',
			'title' => 'ул. Туристская, д. 74',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2920',
			'title' => 'ул. Туристская, д. 80',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2921',
			'title' => 'ул. Туристская, д. 82',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2922',
			'title' => 'ул. Туристская, д. 84',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2923',
			'title' => 'ул. Туруханская, д. 9',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2924',
			'title' => 'ул. Фурманова, д. 26',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2925',
			'title' => 'ул. Юрия Гагарина, д. 1',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2926',
			'title' => 'ул. Юрия Гагарина, д. 90',
			'company_id' => '6',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2927',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 14, к. А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2928',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 26',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2929',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 40',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2930',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 42',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2931',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 42А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2932',
			'title' => 'ул. Айвазовского, д. 1',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2933',
			'title' => 'ул. Айвазовского, д. 11',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2934',
			'title' => 'ул. Айвазовского, д. 17',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2935',
			'title' => 'ул. Айвазовского, д. 29',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2936',
			'title' => 'ул. Айвазовского, д. 3',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2937',
			'title' => 'ул. Айвазовского, д. 5',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2938',
			'title' => 'ул. Айвазовского, д. 7',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2939',
			'title' => 'ул. Академика Павлова, д. 7',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2940',
			'title' => 'ул. Амурская, д. 36',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2941',
			'title' => 'ул. Амурская, д. 44',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2942',
			'title' => 'ул. Волгоградская, д. 1',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2943',
			'title' => 'ул. Волгоградская, д. 10',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2944',
			'title' => 'ул. Волгоградская, д. 15',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2945',
			'title' => 'ул. Волгоградская, д. 21',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2946',
			'title' => 'ул. Волгоградская, д. 4',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2947',
			'title' => 'ул. Волгоградская, д. 6',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2948',
			'title' => 'ул. Волгоградская, д. 7',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2949',
			'title' => 'ул. Волгоградская, д. 8',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2950',
			'title' => 'ул. Волжская, д. 27',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2951',
			'title' => 'ул. Глинки, д. 15',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2952',
			'title' => 'ул. Глинки, д. 16',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2953',
			'title' => 'ул. Глинки, д. 17',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2954',
			'title' => 'ул. Глинки, д. 17А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2955',
			'title' => 'ул. Глинки, д. 18',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2956',
			'title' => 'ул. Глинки, д. 19',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2957',
			'title' => 'ул. Глинки, д. 19А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2958',
			'title' => 'ул. Глинки, д. 19Б',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2959',
			'title' => 'ул. Глинки, д. 1А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2960',
			'title' => 'ул. Глинки, д. 2',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2961',
			'title' => 'ул. Глинки, д. 20',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2962',
			'title' => 'ул. Глинки, д. 21',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2963',
			'title' => 'ул. Глинки, д. 21А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2964',
			'title' => 'ул. Глинки, д. 21Б',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2965',
			'title' => 'ул. Глинки, д. 23',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2966',
			'title' => 'ул. Глинки, д. 23А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2967',
			'title' => 'ул. Глинки, д. 23Б',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2968',
			'title' => 'ул. Глинки, д. 25А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2969',
			'title' => 'ул. Глинки, д. 25Б',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2970',
			'title' => 'ул. Глинки, д. 27',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2971',
			'title' => 'ул. Глинки, д. 27А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2972',
			'title' => 'ул. Глинки, д. 28',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2973',
			'title' => 'ул. Глинки, д. 28А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2974',
			'title' => 'ул. Глинки, д. 2А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2975',
			'title' => 'ул. Глинки, д. 4',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2976',
			'title' => 'ул. Глинки, д. 4А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2977',
			'title' => 'ул. Глинки, д. 7',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2978',
			'title' => 'ул. Глинки, д. 8А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2979',
			'title' => 'ул. Глинки, д. 9',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2980',
			'title' => 'ул. им Академика Вавилова, д. 68А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2981',
			'title' => 'ул. им Говорова, д. 44',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2982',
			'title' => 'ул. им Говорова, д. 46',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2983',
			'title' => 'ул. Кишиневская, д. 12',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2984',
			'title' => 'ул. Кишиневская, д. 7',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2985',
			'title' => 'ул. Краснофлотская 2-я, д. 18',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2986',
			'title' => 'ул. Краснофлотская 2-я, д. 33А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2987',
			'title' => 'ул. Львовская, д. 17',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2988',
			'title' => 'ул. Львовская, д. 28',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2989',
			'title' => 'ул. Львовская, д. 29А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2990',
			'title' => 'ул. Мичурина, д. 15',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2991',
			'title' => 'ул. Мичурина, д. 17',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2992',
			'title' => 'ул. Паровозная, д. 10',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2993',
			'title' => 'ул. Пархоменко, д. 10',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2994',
			'title' => 'ул. Песочная, д. 1',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2995',
			'title' => 'ул. Песочная, д. 14',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2996',
			'title' => 'ул. Песочная, д. 2',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2997',
			'title' => 'ул. Песочная, д. 3',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2998',
			'title' => 'ул. Рейдовая, д. 41',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '2999',
			'title' => 'ул. Рейдовая, д. 44',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3000',
			'title' => 'ул. Рейдовая, д. 52',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3001',
			'title' => 'ул. Рейдовая, д. 53',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3002',
			'title' => 'ул. Спортивная, д. 180',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3003',
			'title' => 'ул. Спортивная, д. 182',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3004',
			'title' => 'ул. Спортивная, д. 192',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3005',
			'title' => 'ул. Тобольская, д. 35А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3006',
			'title' => 'ул. Фестивальная, д. 4А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3007',
			'title' => 'ул. Читинская, д. 12',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3008',
			'title' => 'ул. Ширинская, д. 1',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3009',
			'title' => 'ул. Щербакова, д. 30',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3010',
			'title' => 'ул. Щербакова, д. 32',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3011',
			'title' => 'ул. Щербакова, д. 34',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3012',
			'title' => 'ул. Щербакова, д. 36',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3013',
			'title' => 'ул. Щербакова, д. 38',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3014',
			'title' => 'ул. Щербакова, д. 40',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3015',
			'title' => 'ул. Щербакова, д. 42',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3016',
			'title' => 'ул. Щербакова, д. 43',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3017',
			'title' => 'ул. Щербакова, д. 46',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3018',
			'title' => 'ул. Щербакова, д. 48',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3019',
			'title' => 'ул. Щербакова, д. 50',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3020',
			'title' => 'ул. Энергетиков, д. 32',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3021',
			'title' => 'ул. Энергетиков, д. 40А',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3022',
			'title' => 'ул. Энергетиков, д. 43',
			'company_id' => '7',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3023',
			'title' => 'пр-кт. Металлургов, д. 28 А',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3024',
			'title' => 'ул. 6-я Полярная, д. 5',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3025',
			'title' => 'ул. Армейская, д. 11',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3026',
			'title' => 'ул. Армейская, д. 13',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3027',
			'title' => 'ул. Армейская, д. 19',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3028',
			'title' => 'ул. Армейская, д. 7',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3029',
			'title' => 'ул. Бабушкина, д. 19',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3030',
			'title' => 'ул. Баумана, д. 10А',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3031',
			'title' => 'ул. Баумана, д. 14',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3032',
			'title' => 'ул. Баумана, д. 16',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3033',
			'title' => 'ул. Баумана, д. 18',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3034',
			'title' => 'ул. Баумана, д. 20Г',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3035',
			'title' => 'ул. Баумана, д. 20Д',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3036',
			'title' => 'ул. Внутриквартальная 3-я, д. 6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3037',
			'title' => 'ул. им А.С.Попова, д. 8Б',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3038',
			'title' => 'ул. им И.С.Никитина, д. 18',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3039',
			'title' => 'ул. им И.С.Никитина, д. 4А',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3040',
			'title' => 'ул. им И.С.Никитина, д. 6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3041',
			'title' => 'ул. Калинина, д. 63, к. 1',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3042',
			'title' => 'ул. Калинина, д. 63, к. 2',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3043',
			'title' => 'ул. Калинина, д. 65, к. 3',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3044',
			'title' => 'ул. Калинина, д. 65, к. 4',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3045',
			'title' => 'ул. Калинина, д. 65, к. 5',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3046',
			'title' => 'ул. Калинина, д. 65, к. 6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3047',
			'title' => 'ул. Калинина, д. 72/2',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3048',
			'title' => 'ул. Калинина, д. 76',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3049',
			'title' => 'ул. Калинина, д. 77, к. 1',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3050',
			'title' => 'ул. Калинина, д. 77, к. 3',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3051',
			'title' => 'ул. Калинина, д. 77, к. 4',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3052',
			'title' => 'ул. Калинина, д. 77, к. 5',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3053',
			'title' => 'ул. Краснодарская, д. 40',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3054',
			'title' => 'ул. Маршала Малиновского, д. 18',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3055',
			'title' => 'ул. Маршала Малиновского, д. 2',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3056',
			'title' => 'ул. Маршала Малиновского, д. 20',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3057',
			'title' => 'ул. Маршала Малиновского, д. 30',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3058',
			'title' => 'ул. Маршала Малиновского, д. 6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3059',
			'title' => 'ул. Партизана Железняка, д. 44А',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3060',
			'title' => 'ул. Смоленская 1-я, д. 2',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3061',
			'title' => 'ул. Смоленская 1-я, д. 4',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3062',
			'title' => 'ул. Смоленская 1-я, д. 6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3063',
			'title' => 'ул. Смоленская 2-я, д. 1',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3064',
			'title' => 'ул. Смоленская 2-я, д. 10',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3065',
			'title' => 'ул. Смоленская 2-я, д. 12',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3066',
			'title' => 'ул. Смоленская 2-я, д. 13',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3067',
			'title' => 'ул. Смоленская 2-я, д. 2',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3068',
			'title' => 'ул. Смоленская 2-я, д. 3',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3069',
			'title' => 'ул. Смоленская 2-я, д. 4',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3070',
			'title' => 'ул. Смоленская 2-я, д. 5',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3071',
			'title' => 'ул. Смоленская 2-я, д. 6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3072',
			'title' => 'ул. Смоленская 2-я, д. 7',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3073',
			'title' => 'ул. Смоленская 2-я, д. 8',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3074',
			'title' => 'ул. Смоленская 3-я, д. 11',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3075',
			'title' => 'ул. Смоленская 3-я, д. 5',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3076',
			'title' => 'ул. Спандаряна, д. 19',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3077',
			'title' => 'ул. Спандаряна, д. 23',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3078',
			'title' => 'ул. Спандаряна, д. 25',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3079',
			'title' => 'ул. Спандаряна, д. 27',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3080',
			'title' => 'ул. Спандаряна, д. 35',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3081',
			'title' => 'ул. Спандаряна, д. 37',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3082',
			'title' => 'ул. Спартаковцев, д. 1А',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3083',
			'title' => 'ул. Спартаковцев, д. 69',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3084',
			'title' => 'ул. Спартаковцев, д. 71',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3085',
			'title' => 'ул. Спартаковцев, д. 77',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3086',
			'title' => 'ул. Цимлянская, д. 114',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3087',
			'title' => 'ул. Цимлянская, д. 47',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3088',
			'title' => 'ул. Цимлянская, д. 49',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3089',
			'title' => 'ул. Цимлянская, д. 52',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3090',
			'title' => 'ул. Цимлянская, д. 56',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3091',
			'title' => 'ул. Цимлянская, д. 62',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3092',
			'title' => 'ул. Цимлянская, д. 63',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3093',
			'title' => 'ул. Цимлянская, д. 64',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3094',
			'title' => 'ул. Цимлянская, д. 65',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3095',
			'title' => 'ул. Цимлянская, д. 66',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3096',
			'title' => 'ул. Цимлянская, д. 68',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3097',
			'title' => 'ул. Цимлянская, д. 72',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3098',
			'title' => 'ул. Цимлянская, д. 96',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3099',
			'title' => 'ул. Шахтеров, д. 2Б, стр. 1',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3100',
			'title' => 'ул. Шахтеров, д. 2б/2',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3101',
			'title' => 'ул. Шахтеров, д. 2б/3',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3102',
			'title' => 'ул. Шахтеров, д. 2б/5',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3103',
			'title' => 'ул. Шахтеров, д. 2б/6',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3104',
			'title' => 'ул. Юбилейная, д. 38',
			'company_id' => '8',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3105',
			'title' => 'ул. Гусарова, д. 1',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3106',
			'title' => 'ул. Гусарова, д. 14',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3107',
			'title' => 'ул. Гусарова, д. 17',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3108',
			'title' => 'ул. Гусарова, д. 19',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3109',
			'title' => 'ул. Гусарова, д. 2',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3110',
			'title' => 'ул. Гусарова, д. 20',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3111',
			'title' => 'ул. Гусарова, д. 21',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3112',
			'title' => 'ул. Гусарова, д. 21А',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3113',
			'title' => 'ул. Гусарова, д. 22',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3114',
			'title' => 'ул. Гусарова, д. 23',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3115',
			'title' => 'ул. Гусарова, д. 25',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3116',
			'title' => 'ул. Гусарова, д. 27',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3117',
			'title' => 'ул. Гусарова, д. 28',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3118',
			'title' => 'ул. Гусарова, д. 3',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3119',
			'title' => 'ул. Гусарова, д. 30',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3120',
			'title' => 'ул. Гусарова, д. 32',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3121',
			'title' => 'ул. Гусарова, д. 33',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3122',
			'title' => 'ул. Гусарова, д. 38',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3123',
			'title' => 'ул. Гусарова, д. 4',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3124',
			'title' => 'ул. Гусарова, д. 46',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3125',
			'title' => 'ул. Гусарова, д. 48',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3126',
			'title' => 'ул. Гусарова, д. 49',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3127',
			'title' => 'ул. Гусарова, д. 5',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3128',
			'title' => 'ул. Гусарова, д. 50',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3129',
			'title' => 'ул. Гусарова, д. 51',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3130',
			'title' => 'ул. Гусарова, д. 52',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3131',
			'title' => 'ул. Гусарова, д. 53',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3132',
			'title' => 'ул. Гусарова, д. 54',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3133',
			'title' => 'ул. Гусарова, д. 57',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3134',
			'title' => 'ул. Гусарова, д. 58',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3135',
			'title' => 'ул. Гусарова, д. 59',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3136',
			'title' => 'ул. Гусарова, д. 6',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3137',
			'title' => 'ул. Гусарова, д. 60',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3138',
			'title' => 'ул. Гусарова, д. 61',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3139',
			'title' => 'ул. Гусарова, д. 62',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3140',
			'title' => 'ул. Гусарова, д. 63',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3141',
			'title' => 'ул. Гусарова, д. 64',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3142',
			'title' => 'ул. Гусарова, д. 65',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3143',
			'title' => 'ул. Гусарова, д. 68',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3144',
			'title' => 'ул. Гусарова, д. 69',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3145',
			'title' => 'ул. Гусарова, д. 7',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3146',
			'title' => 'ул. Гусарова, д. 71',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3147',
			'title' => 'ул. Гусарова, д. 72',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3148',
			'title' => 'ул. Гусарова, д. 73',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3149',
			'title' => 'ул. Гусарова, д. 75',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3150',
			'title' => 'ул. Гусарова, д. 76',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3151',
			'title' => 'ул. Гусарова, д. 77',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3152',
			'title' => 'ул. Гусарова, д. 79',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3153',
			'title' => 'ул. Гусарова, д. 80',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3154',
			'title' => 'ул. Гусарова, д. 9',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3155',
			'title' => 'ул. им Героя Советского Союза Б.К.Чернышева, д. 2',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3156',
			'title' => 'ул. Калинина, д. 67А',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3157',
			'title' => 'ул. Мирошниченко, д. 1',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3158',
			'title' => 'ул. Мирошниченко, д. 2',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3159',
			'title' => 'ул. Мирошниченко, д. 4',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3160',
			'title' => 'ул. Мирошниченко, д. 6',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3161',
			'title' => 'ул. П.И.Словцова, д. 11',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3162',
			'title' => 'ул. П.И.Словцова, д. 12',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3163',
			'title' => 'ул. П.И.Словцова, д. 13',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3164',
			'title' => 'ул. П.И.Словцова, д. 2',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3165',
			'title' => 'ул. П.И.Словцова, д. 3',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3166',
			'title' => 'ул. П.И.Словцова, д. 4',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3167',
			'title' => 'ул. П.И.Словцова, д. 7',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3168',
			'title' => 'ул. П.И.Словцова, д. 8',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3169',
			'title' => 'ул. П.И.Словцова, д. 9',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3170',
			'title' => 'ул. Садовая, д. 10',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3171',
			'title' => 'ул. Садовая, д. 12',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3172',
			'title' => 'ул. Садовая, д. 13',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3173',
			'title' => 'ул. Садовая, д. 14',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3174',
			'title' => 'ул. Садовая, д. 15',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3175',
			'title' => 'ул. Садовая, д. 19',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3176',
			'title' => 'ул. Садовая, д. 2',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3177',
			'title' => 'ул. Садовая, д. 22',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3178',
			'title' => 'ул. Садовая, д. 23',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3179',
			'title' => 'ул. Садовая, д. 3',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3180',
			'title' => 'ул. Садовая, д. 4',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3181',
			'title' => 'ул. Садовая, д. 5',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3182',
			'title' => 'ул. Садовая, д. 6',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3183',
			'title' => 'ул. Садовая, д. 7',
			'company_id' => '9',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3184',
			'title' => 'пр-кт. Комсомольский, д. 5А',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3185',
			'title' => 'пр-кт. Комсомольский, д. 7',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3186',
			'title' => 'ул. 9 Мая, д. 35А',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3187',
			'title' => 'ул. 9 Мая, д. 42А',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3188',
			'title' => 'ул. 9 Мая, д. 49',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3189',
			'title' => 'ул. 9 Мая, д. 51',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3190',
			'title' => 'ул. 9 Мая, д. 53',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3191',
			'title' => 'ул. 9 Мая, д. 55',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3192',
			'title' => 'ул. 9 Мая, д. 65',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3193',
			'title' => 'ул. Авиаторов, д. 38',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3194',
			'title' => 'ул. Авиаторов, д. 40',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3195',
			'title' => 'ул. Авиаторов, д. 42',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3196',
			'title' => 'ул. Авиаторов, д. 44',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3197',
			'title' => 'ул. Авиаторов, д. 50',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3198',
			'title' => 'ул. Авиаторов, д. 54',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3199',
			'title' => 'ул. Авиаторов, д. 64',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3200',
			'title' => 'ул. Алексеева, д. 10',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3201',
			'title' => 'ул. Алексеева, д. 14',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3202',
			'title' => 'ул. Алексеева, д. 17',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3203',
			'title' => 'ул. Алексеева, д. 19',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3204',
			'title' => 'ул. Алексеева, д. 21',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3205',
			'title' => 'ул. Алексеева, д. 22',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3206',
			'title' => 'ул. Алексеева, д. 23',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3207',
			'title' => 'ул. Алексеева, д. 24',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3208',
			'title' => 'ул. Алексеева, д. 24, к. 1',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3209',
			'title' => 'ул. Алексеева, д. 25',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3210',
			'title' => 'ул. Алексеева, д. 27',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3211',
			'title' => 'ул. Алексеева, д. 29',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3212',
			'title' => 'ул. Алексеева, д. 33',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3213',
			'title' => 'ул. Алексеева, д. 39',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3214',
			'title' => 'ул. Алексеева, д. 4',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3215',
			'title' => 'ул. Алексеева, д. 5',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3216',
			'title' => 'ул. Алексеева, д. 8',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3217',
			'title' => 'ул. Батурина, д. 9',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3218',
			'title' => 'ул. Водопьянова, д. 12',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3219',
			'title' => 'ул. Водопьянова, д. 14',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3220',
			'title' => 'ул. Водопьянова, д. 16',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3221',
			'title' => 'ул. Водопьянова, д. 18',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3222',
			'title' => 'ул. Водопьянова, д. 19',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3223',
			'title' => 'ул. Водопьянова, д. 20',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3224',
			'title' => 'ул. Водопьянова, д. 22',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3225',
			'title' => 'ул. Водопьянова, д. 24',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3226',
			'title' => 'ул. Водопьянова, д. 26',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3227',
			'title' => 'ул. Водопьянова, д. 28',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3228',
			'title' => 'ул. Водопьянова, д. 8А',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3229',
			'title' => 'ул. Воронова, д. 12, к. а',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3230',
			'title' => 'ул. Воронова, д. 12, к. к',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3231',
			'title' => 'ул. Дмитрия Мартынова, д. 30',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3232',
			'title' => 'ул. Дмитрия Мартынова, д. 32',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3233',
			'title' => 'ул. им Н.Н.Урванцева, д. 12',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3234',
			'title' => 'ул. им Н.Н.Урванцева, д. 13',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3235',
			'title' => 'ул. им Н.Н.Урванцева, д. 15',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3236',
			'title' => 'ул. им Н.Н.Урванцева, д. 17',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3237',
			'title' => 'ул. им Н.Н.Урванцева, д. 19',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3238',
			'title' => 'ул. им Н.Н.Урванцева, д. 5',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3239',
			'title' => 'ул. им Н.Н.Урванцева, д. 7',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3240',
			'title' => 'ул. Карамзина, д. 5',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3241',
			'title' => 'ул. Караульная, д. 40',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3242',
			'title' => 'ул. Караульная, д. 42',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3243',
			'title' => 'ул. Краснодарская, д. 35',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3244',
			'title' => 'ул. Краснодарская, д. 37',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3245',
			'title' => 'ул. Краснодарская, д. 39',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3246',
			'title' => 'ул. Молокова, д. 10',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3247',
			'title' => 'ул. Молокова, д. 12',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3248',
			'title' => 'ул. Молокова, д. 14',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3249',
			'title' => 'ул. Молокова, д. 16',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3250',
			'title' => 'ул. Молокова, д. 1а',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3251',
			'title' => 'ул. Молокова, д. 1г',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3252',
			'title' => 'ул. Молокова, д. 8',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3253',
			'title' => 'ул. Чернышевского, д. 75',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3254',
			'title' => 'ул. Чернышевского, д. 75а',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3255',
			'title' => 'ул. Чернышевского, д. 77',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3256',
			'title' => 'ул. Чернышевского, д. 79',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3257',
			'title' => 'ул. Чернышевского, д. 81',
			'company_id' => '10',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3258',
			'title' => 'пер. 1-й Индустриальный, д. 2',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3259',
			'title' => 'пер. 1-й Индустриальный, д. 4',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3260',
			'title' => 'ул. 8 Марта, д. 18б',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3261',
			'title' => 'ул. 8 Марта, д. 20ж',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3262',
			'title' => 'ул. Баумана, д. 18А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3263',
			'title' => 'ул. Баумана, д. 20А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3264',
			'title' => 'ул. Баумана, д. 8А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3265',
			'title' => 'ул. Высотная, д. 5',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3266',
			'title' => 'ул. Демьяна Бедного, д. 22',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3267',
			'title' => 'ул. Демьяна Бедного, д. 31',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3268',
			'title' => 'ул. Демьяна Бедного, д. 33',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3269',
			'title' => 'ул. Калинина, д. 1/63',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3270',
			'title' => 'ул. Калинина, д. 2Б',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3271',
			'title' => 'ул. Калинина, д. 39А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3272',
			'title' => 'ул. Калинина, д. 39Б',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3273',
			'title' => 'ул. Калинина, д. 43А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3274',
			'title' => 'ул. Калинина, д. 43Б',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3275',
			'title' => 'ул. Калинина, д. 45А, к. 11',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3276',
			'title' => 'ул. Калинина, д. 45А, к. 4',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3277',
			'title' => 'ул. Калинина, д. 45А, к. 7',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3278',
			'title' => 'ул. Калинина, д. 45а/1',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3279',
			'title' => 'ул. Калинина, д. 45а/10',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3280',
			'title' => 'ул. Калинина, д. 45а/2',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3281',
			'title' => 'ул. Калинина, д. 45а/3',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3282',
			'title' => 'ул. Калинина, д. 45а/8',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3283',
			'title' => 'ул. Калинина, д. 45а/9',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3284',
			'title' => 'ул. Калинина, д. 45В',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3285',
			'title' => 'ул. Калинина, д. 45Г',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3286',
			'title' => 'ул. Калинина, д. 5',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3287',
			'title' => 'ул. Калинина, д. 50',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3288',
			'title' => 'ул. Калинина, д. 52, к. 1',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3289',
			'title' => 'ул. Калинина, д. 52/2',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3290',
			'title' => 'ул. Калинина, д. 52/3',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3291',
			'title' => 'ул. Калинина, д. 5А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3292',
			'title' => 'ул. Калинина, д. 68а',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3293',
			'title' => 'ул. Калинина, д. 69Б',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3294',
			'title' => 'ул. Калинина, д. 69В',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3295',
			'title' => 'ул. Калинина, д. 7',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3296',
			'title' => 'ул. Калинина, д. 72/1',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3297',
			'title' => 'ул. Калинина, д. 72/3',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3298',
			'title' => 'ул. Калинина, д. 72/7',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3299',
			'title' => 'ул. Калинина, д. 74',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3300',
			'title' => 'ул. Калинина, д. 78',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3301',
			'title' => 'ул. Калинина, д. 7А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3302',
			'title' => 'ул. Комбайностроителей, д. 10',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3303',
			'title' => 'ул. Комбайностроителей, д. 12, лит. А',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3304',
			'title' => 'ул. Комбайностроителей, д. 18',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3305',
			'title' => 'ул. Мечникова, д. 44',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3306',
			'title' => 'ул. Мечникова, д. 46',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3307',
			'title' => 'ул. Новая Заря, д. 21',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3308',
			'title' => 'ул. Новая Заря, д. 7',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3309',
			'title' => 'ул. Новосибирская, д. 56',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3310',
			'title' => 'ул. Новосибирская, д. 60',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3311',
			'title' => 'ул. Норильская, д. 3Г',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3312',
			'title' => 'ул. Спартаковцев, д. 73',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3313',
			'title' => 'ул. Спартаковцев, д. 75',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3314',
			'title' => 'ул. Спартаковцев, д. 79',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3315',
			'title' => 'ул. Станционная, д. 7',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3316',
			'title' => 'ул. Толстого, д. 65',
			'company_id' => '12',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3317',
			'title' => 'ул. Волжская, д. 2',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3318',
			'title' => 'ул. Волжская, д. 26',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3319',
			'title' => 'ул. Волжская, д. 28',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3320',
			'title' => 'ул. Волжская, д. 31',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3321',
			'title' => 'ул. Волжская, д. 35',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3322',
			'title' => 'ул. Волжская, д. 36',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3323',
			'title' => 'ул. Волжская, д. 38',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3324',
			'title' => 'ул. Волжская, д. 39',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3325',
			'title' => 'ул. Волжская, д. 40',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3326',
			'title' => 'ул. Волжская, д. 42',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3327',
			'title' => 'ул. Волжская, д. 45',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3328',
			'title' => 'ул. Волжская, д. 46',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3329',
			'title' => 'ул. Волжская, д. 47',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3330',
			'title' => 'ул. Волжская, д. 49',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3331',
			'title' => 'ул. Волжская, д. 5',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3332',
			'title' => 'ул. Волжская, д. 50',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3333',
			'title' => 'ул. Волжская, д. 51',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3334',
			'title' => 'ул. Волжская, д. 52',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3335',
			'title' => 'ул. Волжская, д. 6',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3336',
			'title' => 'ул. Волжская, д. 7',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3337',
			'title' => 'ул. им Говорова, д. 48',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3338',
			'title' => 'ул. Инициаторов, д. 1',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3339',
			'title' => 'ул. Инициаторов, д. 17',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3340',
			'title' => 'ул. Инициаторов, д. 18',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3341',
			'title' => 'ул. Инициаторов, д. 19',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3342',
			'title' => 'ул. Инициаторов, д. 2',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3343',
			'title' => 'ул. Инициаторов, д. 20',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3344',
			'title' => 'ул. Инициаторов, д. 21',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3345',
			'title' => 'ул. Инициаторов, д. 22',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3346',
			'title' => 'ул. Инициаторов, д. 23',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3347',
			'title' => 'ул. Инициаторов, д. 24',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3348',
			'title' => 'ул. Инициаторов, д. 25',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3349',
			'title' => 'ул. Инициаторов, д. 26',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3350',
			'title' => 'ул. Инициаторов, д. 27',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3351',
			'title' => 'ул. Инициаторов, д. 28',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3352',
			'title' => 'ул. Инициаторов, д. 2А',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3353',
			'title' => 'ул. Инициаторов, д. 3',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3354',
			'title' => 'ул. Инициаторов, д. 3А',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3355',
			'title' => 'ул. Инициаторов, д. 4',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3356',
			'title' => 'ул. Энергетиков, д. 57',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3357',
			'title' => 'ул. Энергетиков, д. 59',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3358',
			'title' => 'ул. Энергетиков, д. 61',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3359',
			'title' => 'ул. Энергетиков, д. 63',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3360',
			'title' => 'ул. Энергетиков, д. 67',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3361',
			'title' => 'ул. Энергетиков, д. 69',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3362',
			'title' => 'ул. Энергетиков, д. 71',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3363',
			'title' => 'ул. Энергетиков, д. 73',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3364',
			'title' => 'ул. Энергетиков, д. 73а',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3365',
			'title' => 'ул. Энергетиков, д. 75',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3366',
			'title' => 'ул. Энергетиков, д. 77',
			'company_id' => '14',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3367',
			'title' => 'б-р. Солнечный Бульвар, д. 5',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3368',
			'title' => 'пр-кт. 60 лет образования СССР, д. 29',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3369',
			'title' => 'пр-кт. 60 лет образования СССР, д. 50',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3370',
			'title' => 'пр-кт. 60 лет образования СССР, д. 61',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3371',
			'title' => 'ул. 40 лет Победы, д. 26',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3372',
			'title' => 'ул. 40 лет Победы, д. 30',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3373',
			'title' => 'ул. 40 лет Победы, д. 30д',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3374',
			'title' => 'ул. 40 лет Победы, д. 32',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3375',
			'title' => 'ул. 40 лет Победы, д. 33',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3376',
			'title' => 'ул. 40 лет Победы, д. 35',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3377',
			'title' => 'ул. 40 лет Победы, д. 6а',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3378',
			'title' => 'ул. Абытаевская, д. 10',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3379',
			'title' => 'ул. Абытаевская, д. 4',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3380',
			'title' => 'ул. Абытаевская, д. 4а',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3381',
			'title' => 'ул. Абытаевская, д. 6',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3382',
			'title' => 'ул. Абытаевская, д. 8',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3383',
			'title' => 'ул. Алексеева, д. 109',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3384',
			'title' => 'ул. Алексеева, д. 111',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3385',
			'title' => 'ул. Алексеева, д. 113',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3386',
			'title' => 'ул. Алексеева, д. 115',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3387',
			'title' => 'ул. Батурина, д. 19',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3388',
			'title' => 'ул. Батурина, д. 20',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3389',
			'title' => 'ул. им Героя Советского Союза Б.А.Микуцкого, д. 2',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3390',
			'title' => 'ул. им Корнеева, д. 24а',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3391',
			'title' => 'ул. им Корнеева, д. 26',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3392',
			'title' => 'ул. Караульная, д. 82',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3393',
			'title' => 'ул. Молокова, д. 23',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3394',
			'title' => 'ул. Молокова, д. 40',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3395',
			'title' => 'ул. Молокова, д. 46',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3396',
			'title' => 'ул. Молокова, д. 50',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3397',
			'title' => 'ул. Молокова, д. 58',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3398',
			'title' => 'ул. Молокова, д. 60',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3399',
			'title' => 'ул. Молокова, д. 62',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3400',
			'title' => 'ул. Молокова, д. 64',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3401',
			'title' => 'ул. Молокова, д. 66',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3402',
			'title' => 'ул. Молокова, д. 68',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3403',
			'title' => 'ул. Новосибирская, д. 39А',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3404',
			'title' => 'ул. Славы, д. 9',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3405',
			'title' => 'ул. Сопочная, д. 36',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3406',
			'title' => 'ул. Шахтеров, д. 42',
			'company_id' => '15',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3407',
			'title' => 'пер. Автобусный, д. 44',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3408',
			'title' => 'пер. Вузовский, д. 5',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3409',
			'title' => 'пер. Якорный, д. 1',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3410',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 100А',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3411',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 100б',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3412',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 102',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3413',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 102, к. а',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3414',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 102/3',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3415',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 102/4',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3416',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 104',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3417',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 125А',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3418',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 65А',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3419',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 71',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3420',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 77',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3421',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 83А',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3422',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 85',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3423',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 97',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3424',
			'title' => 'ул. Веселая, д. 14',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3425',
			'title' => 'ул. Западная, д. 5А',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3426',
			'title' => 'ул. Западная, д. 7',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3427',
			'title' => 'ул. им Академика Вавилова, д. 33',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3428',
			'title' => 'ул. им Академика Вавилова, д. 54',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3429',
			'title' => 'ул. им Академика Вавилова, д. 58',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3430',
			'title' => 'ул. им Академика Вавилова, д. 74',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3431',
			'title' => 'ул. им Академика Вавилова, д. 76',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3432',
			'title' => 'ул. Ключевская, д. 67',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3433',
			'title' => 'ул. Ключевская, д. 69',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3434',
			'title' => 'ул. Космонавта Николаева, д. 9',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3435',
			'title' => 'ул. Побежимова, д. 46',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3436',
			'title' => 'ул. Саянская, д. 259',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3437',
			'title' => 'ул. Свердловская, д. 115',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3438',
			'title' => 'ул. Шелковая, д. 1',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3439',
			'title' => 'ул. Шелковая, д. 13',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3440',
			'title' => 'ул. Шелковая, д. 5',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3441',
			'title' => 'ул. Шелковая, д. 6',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3442',
			'title' => 'ул. Шелковая, д. 7',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3443',
			'title' => 'ул. Шелковая, д. 8',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3444',
			'title' => 'ул. Шелковая, д. 9',
			'company_id' => '16',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3445',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 46',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3446',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 48',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3447',
			'title' => 'ул. Астраханская, д. 13',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3448',
			'title' => 'ул. Верхняя, д. 38А',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3449',
			'title' => 'ул. Верхняя, д. 40',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3450',
			'title' => 'ул. Верхняя, д. 5Б',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3451',
			'title' => 'ул. Верхняя, д. 5В',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3452',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 1',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3453',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 1, к. г',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3454',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 12',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3455',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 13',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3456',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 14',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3457',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 14А',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3458',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 18',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3459',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 1Б',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3460',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 1В',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3461',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 2',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3462',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 20',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3463',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 21',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3464',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 22',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3465',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 3',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3466',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 30',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3467',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 4',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3468',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 6',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3469',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 8',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3470',
			'title' => 'ул. им Героя Советского Союза И.А.Борисевича, д. 8 А',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3471',
			'title' => 'ул. Малаховская, д. 4',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3472',
			'title' => 'ул. Ползунова, д. 10',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3473',
			'title' => 'ул. Ползунова, д. 9',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3474',
			'title' => 'ул. Энергетиков, д. 16',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3475',
			'title' => 'ул. Энергетиков, д. 20',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3476',
			'title' => 'ул. Юности, д. 4',
			'company_id' => '17',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3477',
			'title' => 'наб. Ярыгинская, д. 17',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3478',
			'title' => 'наб. Ярыгинская, д. 19',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3479',
			'title' => 'наб. Ярыгинская, д. 19 А',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3480',
			'title' => 'наб. Ярыгинская, д. 21',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3481',
			'title' => 'наб. Ярыгинская, д. 23',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3482',
			'title' => 'наб. Ярыгинская, д. 25',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3483',
			'title' => 'наб. Ярыгинская, д. 29',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3484',
			'title' => 'наб. Ярыгинская, д. 31',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3485',
			'title' => 'наб. Ярыгинская, д. 33',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3486',
			'title' => 'наб. Ярыгинская, д. 35',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3487',
			'title' => 'наб. Ярыгинская, д. 41',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3488',
			'title' => 'ул. Академика Киренского, д. 22',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3489',
			'title' => 'ул. Академика Киренского, д. 24',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3490',
			'title' => 'ул. Академика Киренского, д. 24 А',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3491',
			'title' => 'ул. Борисова, д. 30',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3492',
			'title' => 'ул. Борисова, д. 32',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3493',
			'title' => 'ул. Борисова, д. 34',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3494',
			'title' => 'ул. Борисова, д. 36',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3495',
			'title' => 'ул. Борисова, д. 38',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3496',
			'title' => 'ул. Борисова, д. 40',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3497',
			'title' => 'ул. Борисова, д. 42, стр. исправный',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3498',
			'title' => 'ул. Борисова, д. 44',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3499',
			'title' => 'ул. Дачная, д. 28',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3500',
			'title' => 'ул. Карамзина, д. 14',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3501',
			'title' => 'ул. Карамзина, д. 14А',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3502',
			'title' => 'ул. Карамзина, д. 16',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3503',
			'title' => 'ул. Карамзина, д. 18',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3504',
			'title' => 'ул. Карамзина, д. 24',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3505',
			'title' => 'ул. Карамзина, д. 28',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3506',
			'title' => 'ул. Карамзина, д. 30',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3507',
			'title' => 'ул. Карамзина, д. 32',
			'company_id' => '18',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3508',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 32',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3509',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 34',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3510',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 38',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3511',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 40А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3512',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 48',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3513',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 50',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3514',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 7А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3515',
			'title' => 'ул. Айвазовского, д. 21',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3516',
			'title' => 'ул. Айвазовского, д. 23',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3517',
			'title' => 'ул. Айвазовского, д. 9',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3518',
			'title' => 'ул. Глинки, д. 10А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3519',
			'title' => 'ул. Глинки, д. 11',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3520',
			'title' => 'ул. Глинки, д. 11А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3521',
			'title' => 'ул. Глинки, д. 13',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3522',
			'title' => 'ул. Глинки, д. 25',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3523',
			'title' => 'ул. Глинки, д. 26',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3524',
			'title' => 'ул. Глинки, д. 26А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3525',
			'title' => 'ул. Глинки, д. 6',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3526',
			'title' => 'ул. Глинки, д. 6А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3527',
			'title' => 'ул. Глинки, д. 8',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3528',
			'title' => 'ул. Краснофлотская 2-я, д. 35А',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3529',
			'title' => 'ул. Краснофлотская 2-я, д. 5',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3530',
			'title' => 'ул. Крылова, д. 11',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3531',
			'title' => 'ул. Крылова, д. 13',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3532',
			'title' => 'ул. Пархоменко, д. 3',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3533',
			'title' => 'ул. Песочная, д. 5',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3534',
			'title' => 'ул. Песочная, д. 7',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3535',
			'title' => 'ул. Рейдовая, д. 48',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3536',
			'title' => 'ул. Рейдовая, д. 50',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3537',
			'title' => 'ул. Рейдовая, д. 51',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3538',
			'title' => 'ул. Фестивальная, д. 6',
			'company_id' => '19',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3539',
			'title' => 'пр-кт. 60 лет образования СССР, д. 21',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3540',
			'title' => 'пр-кт. 60 лет образования СССР, д. 23',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3541',
			'title' => 'пр-кт. 60 лет образования СССР, д. 25',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3542',
			'title' => 'пр-кт. 60 лет образования СССР, д. 27',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3543',
			'title' => 'пр-кт. 60 лет образования СССР, д. 35',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3544',
			'title' => 'пр-кт. 60 лет образования СССР, д. 37',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3545',
			'title' => 'пр-кт. 60 лет образования СССР, д. 43',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3546',
			'title' => 'пр-кт. 60 лет образования СССР, д. 43, к. 2',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3547',
			'title' => 'пр-кт. 60 лет образования СССР, д. 43, к. 3',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3548',
			'title' => 'пр-кт. 60 лет образования СССР, д. 45',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3549',
			'title' => 'пр-кт. 60 лет образования СССР, д. 47',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3550',
			'title' => 'пр-кт. Комсомольский, д. 1',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3551',
			'title' => 'пр-кт. Свободный, д. 74',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3552',
			'title' => 'ул. 40 лет Победы, д. 18',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3553',
			'title' => 'ул. 40 лет Победы, д. 20',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3554',
			'title' => 'ул. 40 лет Победы, д. 39',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3555',
			'title' => 'ул. 40 лет Победы, д. 4',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3556',
			'title' => 'ул. 40 лет Победы, д. 41',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3557',
			'title' => 'ул. 9 Мая, д. 21',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3558',
			'title' => 'ул. 9 Мая, д. 23',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3559',
			'title' => 'ул. 9 Мая, д. 25',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3560',
			'title' => 'ул. им Н.Н.Урванцева, д. 18',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3561',
			'title' => 'ул. им Н.Н.Урванцева, д. 20',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3562',
			'title' => 'ул. им Н.Н.Урванцева, д. 6',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3563',
			'title' => 'ул. Светлова, д. 31',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3564',
			'title' => 'ул. Славы, д. 1',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3565',
			'title' => 'ул. Славы, д. 11',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3566',
			'title' => 'ул. Славы, д. 13',
			'company_id' => '20',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3567',
			'title' => 'пр-кт. Ульяновский, д. 14, к. г',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3568',
			'title' => 'пр-кт. Ульяновский, д. 18, лит. Б',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3569',
			'title' => 'пр-кт. Ульяновский, д. 28',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3570',
			'title' => 'ул. 9 Мая, д. 83',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3571',
			'title' => 'ул. Ады Лебедевой, д. 47',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3572',
			'title' => 'ул. Академика Киренского, д. 75',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3573',
			'title' => 'ул. Алексеева, д. 50',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3574',
			'title' => 'ул. Е.Д.Стасовой, д. 40И',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3575',
			'title' => 'ул. Железнодорожников, д. 22, лит. Д',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3576',
			'title' => 'ул. им В.М.Комарова, д. 7, лит. А',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3577',
			'title' => 'ул. им Сергея Лазо, д. 20',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3578',
			'title' => 'ул. им Сергея Лазо, д. 22',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3579',
			'title' => 'ул. им Сергея Лазо, д. 24',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3580',
			'title' => 'ул. им Сергея Лазо, д. 36',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3581',
			'title' => 'ул. им Сергея Лазо, д. 8',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3582',
			'title' => 'ул. Космонавта Николаева, д. 5',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3583',
			'title' => 'ул. Куйбышева, д. 79',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3584',
			'title' => 'ул. Куйбышева, д. 87',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3585',
			'title' => 'ул. Ленинградская, д. 6',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3586',
			'title' => 'ул. Ленинградская, д. 8',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3587',
			'title' => 'ул. Марковского, д. 80',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3588',
			'title' => 'ул. Новосибирская, д. 1',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3589',
			'title' => 'ул. Новосибирская, д. 1, лит. А',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3590',
			'title' => 'ул. Новосибирская, д. 3',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3591',
			'title' => 'ул. Новосибирская, д. 5',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3592',
			'title' => 'ул. Тельмана, д. 12',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3593',
			'title' => 'ул. Толстого, д. 67а',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3594',
			'title' => 'ул. Яковлева, д. 1А',
			'company_id' => '21',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3595',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 165',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3596',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 182',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3597',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 185',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3598',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 189',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3599',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 197',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3600',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 199',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3601',
			'title' => 'ул. 60 лет Октября, д. 40А',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3602',
			'title' => 'ул. 60 лет Октября, д. 44',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3603',
			'title' => 'ул. Парашютная, д. 66А',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3604',
			'title' => 'ул. Парашютная, д. 78а',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3605',
			'title' => 'ул. Парашютная, д. 82',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3606',
			'title' => 'ул. Парашютная, д. 82А',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3607',
			'title' => 'ул. Парашютная, д. 88а',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3608',
			'title' => 'ул. Свердловская, д. 51А',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3609',
			'title' => 'ул. Свердловская, д. 55',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3610',
			'title' => 'ул. Семафорная, д. 17',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3611',
			'title' => 'ул. Семафорная, д. 21',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3612',
			'title' => 'ул. Судостроительная, д. 113',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3613',
			'title' => 'ул. Судостроительная, д. 117',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3614',
			'title' => 'ул. Судостроительная, д. 127',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3615',
			'title' => 'ул. Судостроительная, д. 24а',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3616',
			'title' => 'ул. Судостроительная, д. 30',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3617',
			'title' => 'ул. Судостроительная, д. 52',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3618',
			'title' => 'ул. Судостроительная, д. 52А',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3619',
			'title' => 'ул. Судостроительная, д. 54',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3620',
			'title' => 'ул. Судостроительная, д. 76',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3621',
			'title' => 'ул. Судостроительная, д. 78',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3622',
			'title' => 'ул. Судостроительная, д. 82',
			'company_id' => '22',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3623',
			'title' => 'пр-кт. Комсомольский, д. 10',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3624',
			'title' => 'пр-кт. Комсомольский, д. 6',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3625',
			'title' => 'пр-кт. Комсомольский, д. 8',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3626',
			'title' => 'ул. 9 Мая, д. 17 Д',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3627',
			'title' => 'ул. им Б.З.Шумяцкого, д. 2',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3628',
			'title' => 'ул. им Б.З.Шумяцкого, д. 4',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3629',
			'title' => 'ул. Караульная, д. 38',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3630',
			'title' => 'ул. Любы Шевцовой, д. 80',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3631',
			'title' => 'ул. Любы Шевцовой, д. 82',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3632',
			'title' => 'ул. Любы Шевцовой, д. 84',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3633',
			'title' => 'ул. Любы Шевцовой, д. 84а',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3634',
			'title' => 'ул. Любы Шевцовой, д. 88',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3635',
			'title' => 'ул. Мате Залки, д. 12, лит. а',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3636',
			'title' => 'ул. Мате Залки, д. 15',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3637',
			'title' => 'ул. Мате Залки, д. 17',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3638',
			'title' => 'ул. Мате Залки, д. 19',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3639',
			'title' => 'ул. Мате Залки, д. 21',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3640',
			'title' => 'ул. Мате Залки, д. 29',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3641',
			'title' => 'ул. Мате Залки, д. 30',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3642',
			'title' => 'ул. Мате Залки, д. 31',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3643',
			'title' => 'ул. Мате Залки, д. 33',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3644',
			'title' => 'ул. Мате Залки, д. 34',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3645',
			'title' => 'ул. Мате Залки, д. 37',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3646',
			'title' => 'ул. Мате Залки, д. 39',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3647',
			'title' => 'ул. Мате Залки, д. 41',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3648',
			'title' => 'ул. Мате Залки, д. 6А',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3649',
			'title' => 'пр-кт. Комсомольский, д. 4',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3650',
			'title' => 'пр-кт. Металлургов, д. 22А',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3651',
			'title' => 'пр-кт. Молодежный, д. 3',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3652',
			'title' => 'ул. 40 лет Победы, д. 2',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3653',
			'title' => 'ул. им Героя Советского Союза Б.А.Микуцкого, д. 12',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3654',
			'title' => 'ул. Маршала К.Рокоссовского, д. 24А',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3655',
			'title' => 'ул. Норильская, д. 4',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3656',
			'title' => 'ул. Норильская, д. 4, к. к',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3657',
			'title' => 'ул. Норильская, д. 8',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3658',
			'title' => 'ул. Светлова, д. 25',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3659',
			'title' => 'ул. Ястынская, д. 1',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3660',
			'title' => 'ул. Ястынская, д. 10',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3661',
			'title' => 'ул. Ястынская, д. 10, к. а',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3662',
			'title' => 'ул. Ястынская, д. 13',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3663',
			'title' => 'ул. Ястынская, д. 17',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3664',
			'title' => 'ул. Ястынская, д. 17, к. а',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3665',
			'title' => 'ул. Ястынская, д. 2, к. д',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3666',
			'title' => 'ул. Ястынская, д. 2, к. ж',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3667',
			'title' => 'ул. Ястынская, д. 3',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3668',
			'title' => 'ул. Ястынская, д. 3, к. а',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3669',
			'title' => 'ул. Ястынская, д. 5, к. а',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3670',
			'title' => 'ул. Ястынская, д. 5.',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3671',
			'title' => 'ул. Ястынская, д. 6, к. г',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3672',
			'title' => 'ул. Ястынская, д. 7',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3673',
			'title' => 'ул. Ястынская, д. 9',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3674',
			'title' => 'ул. Ястынская, д. 9, к. а',
			'company_id' => '24',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3675',
			'title' => 'б-р. Ботанический, д. 25',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3676',
			'title' => 'ул. Ботаническая 2-я, д. 2',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3677',
			'title' => 'ул. Ботаническая, д. 1 А',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3678',
			'title' => 'ул. Ботаническая, д. 1 Б',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3679',
			'title' => 'ул. Ботаническая, д. 1 В',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3680',
			'title' => 'ул. Ботаническая, д. 1 Г',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3681',
			'title' => 'ул. Ботаническая, д. 1 Е',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3682',
			'title' => 'ул. Ботаническая, д. 1 И',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3683',
			'title' => 'ул. Ботаническая, д. 22',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3684',
			'title' => 'ул. Ботаническая, д. 22 Б',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3685',
			'title' => 'ул. Ботаническая, д. 22 Г',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3686',
			'title' => 'ул. Изумрудная, д. 1',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3687',
			'title' => 'ул. Изумрудная, д. 5',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3688',
			'title' => 'ул. Изумрудная, д. 7',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3689',
			'title' => 'ул. Изумрудная, д. 9',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3690',
			'title' => 'ул. Калиновая, д. 1',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3691',
			'title' => 'ул. Лиственная, д. 18',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3692',
			'title' => 'ул. Фруктовая, д. 18',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3693',
			'title' => 'ул. Фруктовая, д. 2',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3694',
			'title' => 'ул. Фруктовая, д. 20',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3695',
			'title' => 'ул. Фруктовая, д. 22',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3696',
			'title' => 'ул. Фруктовая, д. 24',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3697',
			'title' => 'ул. Фруктовая, д. 4',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3698',
			'title' => 'ул. Фруктовая, д. 6',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3699',
			'title' => 'ул. Чистопрудная, д. 13',
			'company_id' => '25',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3700',
			'title' => 'пр-кт. 60 лет образования СССР, д. 19',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3701',
			'title' => 'пр-кт. 60 лет образования СССР, д. 4а',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3702',
			'title' => 'ул. Дальневосточная, д. 55',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3703',
			'title' => 'ул. Дальневосточная, д. 57',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3704',
			'title' => 'ул. Дальневосточная, д. 59',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3705',
			'title' => 'ул. Дальневосточная, д. 61',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3706',
			'title' => 'ул. Дальневосточная, д. 63',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3707',
			'title' => 'ул. Дмитрия Мартынова, д. 13',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3708',
			'title' => 'ул. Дмитрия Мартынова, д. 15',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3709',
			'title' => 'ул. Дмитрия Мартынова, д. 17',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3710',
			'title' => 'ул. Дмитрия Мартынова, д. 19',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3711',
			'title' => 'ул. Линейная, д. 109',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3712',
			'title' => 'ул. Мужества, д. 18',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3713',
			'title' => 'ул. Мужества, д. 20',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3714',
			'title' => 'ул. Мужества, д. 22',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3715',
			'title' => 'ул. Мужества, д. 24',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3716',
			'title' => 'ул. Чернышевского, д. 100',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3717',
			'title' => 'ул. Чернышевского, д. 102',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3718',
			'title' => 'ул. Чернышевского, д. 104',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3719',
			'title' => 'ул. Чернышевского, д. 106',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3720',
			'title' => 'ул. Чернышевского, д. 108',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3721',
			'title' => 'ул. Чернышевского, д. 98',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3722',
			'title' => 'ул. Шахтеров, д. 40',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3723',
			'title' => 'ул. Шахтеров, д. 44',
			'company_id' => '26',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3724',
			'title' => 'пр-кт. Свободный, д. 75А',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3725',
			'title' => 'ул. Высотная, д. 21А',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3726',
			'title' => 'ул. Высотная, д. 21В',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3727',
			'title' => 'ул. Высотная, д. 25',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3728',
			'title' => 'ул. Гусарова, д. 74',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3729',
			'title' => 'ул. Крупской, д. 1',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3730',
			'title' => 'ул. Крупской, д. 16',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3731',
			'title' => 'ул. Крупской, д. 18',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3732',
			'title' => 'ул. Крупской, д. 2',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3733',
			'title' => 'ул. Крупской, д. 20',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3734',
			'title' => 'ул. Крупской, д. 22',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3735',
			'title' => 'ул. Крупской, д. 3',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3736',
			'title' => 'ул. Крупской, д. 34А',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3737',
			'title' => 'ул. Крупской, д. 5',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3738',
			'title' => 'ул. Крупской, д. 7',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3739',
			'title' => 'ул. Крупской, д. 9',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3740',
			'title' => 'ул. Курчатова, д. 11',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3741',
			'title' => 'ул. Курчатова, д. 9',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3742',
			'title' => 'ул. Можайского, д. 12',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3743',
			'title' => 'ул. П.И.Словцова, д. 6',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3744',
			'title' => 'ул. Хабаровская 1-я, д. 6',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3745',
			'title' => 'ул. Хабаровская 1-я, д. 8',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3746',
			'title' => 'ул. Хабаровская 2-я, д. 2',
			'company_id' => '27',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3747',
			'title' => 'пр-кт. Мира, д. 12',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3748',
			'title' => 'ул. Ады Лебедевой, д. 31/38',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3749',
			'title' => 'ул. Ады Лебедевой, д. 66',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3750',
			'title' => 'ул. Горького, д. 42',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3751',
			'title' => 'ул. Дубровинского, д. 54А',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3752',
			'title' => 'ул. Дубровинского, д. 56',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3753',
			'title' => 'ул. Дубровинского, д. 58',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3754',
			'title' => 'ул. Дубровинского, д. 62',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3755',
			'title' => 'ул. Дубровинского, д. 62а',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3756',
			'title' => 'ул. Карла Маркса, д. 21',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3757',
			'title' => 'ул. Карла Маркса, д. 47',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3758',
			'title' => 'ул. Конституции СССР, д. 13',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3759',
			'title' => 'ул. Конституции СССР, д. 15',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3760',
			'title' => 'ул. Конституции СССР, д. 25',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3761',
			'title' => 'ул. Копылова, д. 48',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3762',
			'title' => 'ул. Ленина, д. 116',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3763',
			'title' => 'ул. Ленина, д. 128',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3764',
			'title' => 'ул. Марковского, д. 50',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3765',
			'title' => 'ул. Парижской Коммуны, д. 10',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3766',
			'title' => 'ул. Парижской Коммуны, д. 14',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3767',
			'title' => 'ул. Парижской Коммуны, д. 40',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3768',
			'title' => 'ул. Парижской Коммуны, д. 8А',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3769',
			'title' => 'ул. Сурикова, д. 53',
			'company_id' => '28',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3770',
			'title' => 'ул. 52 Квартал, д. 10',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3771',
			'title' => 'ул. Верхняя, д. 40А',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3772',
			'title' => 'ул. Глинки, д. 10',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3773',
			'title' => 'ул. Добролюбова, д. 19',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3774',
			'title' => 'ул. Котовского, д. 26',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3775',
			'title' => 'ул. Львовская, д. 11А',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3776',
			'title' => 'ул. Монтажников, д. 16',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3777',
			'title' => 'ул. Монтажников, д. 20',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3778',
			'title' => 'ул. Монтажников, д. 22',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3779',
			'title' => 'ул. Парковая, д. 11',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3780',
			'title' => 'ул. Парковая, д. 13',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3781',
			'title' => 'ул. Песочная, д. 4',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3782',
			'title' => 'ул. Рейдовая, д. 72',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3783',
			'title' => 'ул. Свердловская, д. 104А',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3784',
			'title' => 'ул. Семафорная, д. 345',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3785',
			'title' => 'ул. Семафорная, д. 363',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3786',
			'title' => 'ул. Семафорная, д. 403',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3787',
			'title' => 'ул. Семафорная, д. 423',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3788',
			'title' => 'ул. Семафорная, д. 439, к. 3',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3789',
			'title' => 'ул. Семафорная, д. 439/1',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3790',
			'title' => 'ул. Семафорная, д. 439/2',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3791',
			'title' => 'ул. Щербакова, д. 45',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3792',
			'title' => 'ул. Энергетиков, д. 39',
			'company_id' => '29',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3793',
			'title' => 'ул. 9 Мая, д. 20',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3794',
			'title' => 'ул. 9 Мая, д. 24',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3795',
			'title' => 'ул. Амурская, д. 20',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3796',
			'title' => 'ул. Амурская, д. 24',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3797',
			'title' => 'ул. Батурина, д. 30',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3798',
			'title' => 'ул. Батурина, д. 30, к. 1',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3799',
			'title' => 'ул. Батурина, д. 30, к. 2',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3800',
			'title' => 'ул. Батурина, д. 30, к. 3',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3801',
			'title' => 'ул. Батурина, д. 30, к. 4',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3802',
			'title' => 'ул. Батурина, д. 32',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3803',
			'title' => 'ул. Батурина, д. 34',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3804',
			'title' => 'ул. Линейная, д. 97',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3805',
			'title' => 'ул. Мужества, д. 14',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3806',
			'title' => 'ул. Светлогорская, д. 9',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3807',
			'title' => 'ул. Соревнования, д. 25',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3808',
			'title' => 'ул. Ястынская, д. 11',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3809',
			'title' => 'ул. Ястынская, д. 12, стр. а',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3810',
			'title' => 'ул. Ястынская, д. 14, стр. а',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3811',
			'title' => 'ул. Ястынская, д. 15',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3812',
			'title' => 'ул. Ястынская, д. 15, стр. а',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3813',
			'title' => 'ул. Ястынская, д. 19',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3814',
			'title' => 'ул. Ястынская, д. 19, стр. а',
			'company_id' => '30',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3815',
			'title' => 'ул. Академика Павлова, д. 22',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3816',
			'title' => 'ул. Академика Павлова, д. 27',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3817',
			'title' => 'ул. Академика Павлова, д. 37',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3818',
			'title' => 'ул. Академика Павлова, д. 47',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3819',
			'title' => 'ул. Алеши Тимошенкова, д. 137',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3820',
			'title' => 'ул. Алеши Тимошенкова, д. 173',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3821',
			'title' => 'ул. Астраханская, д. 9А',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3822',
			'title' => 'ул. Волгоградская, д. 17А',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3823',
			'title' => 'ул. Гастелло, д. 23',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3824',
			'title' => 'ул. им газеты Пионерская Правда, д. 13',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3825',
			'title' => 'ул. Кутузова, д. 18',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3826',
			'title' => 'ул. Кутузова, д. 20',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3827',
			'title' => 'ул. Кутузова, д. 62',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3828',
			'title' => 'ул. Московская, д. 10',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3829',
			'title' => 'ул. Новая, д. 4',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3830',
			'title' => 'ул. Транзитная, д. 26',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3831',
			'title' => 'ул. Транзитная, д. 54',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3832',
			'title' => 'ул. Щорса, д. 47',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3833',
			'title' => 'ул. Щорса, д. 48А',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3834',
			'title' => 'ул. Щорса, д. 49',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3835',
			'title' => 'ул. Щорса, д. 71',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3836',
			'title' => 'ул. Щорса, д. 75',
			'company_id' => '31',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3837',
			'title' => 'ул. Академика Павлова, д. 74',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3838',
			'title' => 'ул. Академика Павлова, д. 76',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3839',
			'title' => 'ул. Академика Павлова, д. 78',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3840',
			'title' => 'ул. Академика Павлова, д. 80',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3841',
			'title' => 'ул. Академика Павлова, д. 84',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3842',
			'title' => 'ул. Академика Павлова, д. 88',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3843',
			'title' => 'ул. Академика Павлова, д. 90',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3844',
			'title' => 'ул. Гастелло, д. 25',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3845',
			'title' => 'ул. Гастелло, д. 32',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3846',
			'title' => 'ул. Гастелло, д. 41',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3847',
			'title' => 'ул. Кутузова, д. 109',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3848',
			'title' => 'ул. Кутузова, д. 64',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3849',
			'title' => 'ул. Транзитная, д. 52',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3850',
			'title' => 'ул. Транзитная, д. 58',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3851',
			'title' => 'ул. Транзитная, д. 6',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3852',
			'title' => 'ул. Транзитная, д. 60',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3853',
			'title' => 'ул. Транзитная, д. 62',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3854',
			'title' => 'ул. Щербакова, д. 5',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3855',
			'title' => 'ул. Щербакова, д. 7',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3856',
			'title' => 'ул. Щорса, д. 16',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3857',
			'title' => 'ул. Щорса, д. 24',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3858',
			'title' => 'ул. Щорса, д. 48',
			'company_id' => '32',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3859',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 20',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3860',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 22',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3861',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 24',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3862',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 24А',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3863',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 26',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3864',
			'title' => 'ул. им Героя Советского Союза Д.М.Карбышева, д. 28',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3865',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 8В',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3866',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 11',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3867',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 12',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3868',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 14',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3869',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 16',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3870',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 18',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3871',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 19',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3872',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 22',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3873',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 25',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3874',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 25А',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3875',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 27Б',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3876',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 3',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3877',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 8',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3878',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 8Б',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3879',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 8В',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3880',
			'title' => 'ул. Калинина, д. 80А',
			'company_id' => '33',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3881',
			'title' => 'пр-кт. 60 лет образования СССР, д. 26, лит. И',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3882',
			'title' => 'пр-кт. 60 лет образования СССР, д. 32',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3883',
			'title' => 'пр-кт. 60 лет образования СССР, д. 34',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3884',
			'title' => 'пр-кт. 60 лет образования СССР, д. 36',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3885',
			'title' => 'пр-кт. 60 лет образования СССР, д. 38 Ж',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3886',
			'title' => 'пр-кт. 60 лет образования СССР, д. 38 К',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3887',
			'title' => 'пр-кт. 60 лет образования СССР, д. 38, к. а',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3888',
			'title' => 'пр-кт. 60 лет образования СССР, д. 38, к. Д',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3889',
			'title' => 'пр-кт. Молодежный, д. 23',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3890',
			'title' => 'пр-кт. Молодежный, д. 25',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3891',
			'title' => 'пр-кт. Молодежный, д. 27',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3892',
			'title' => 'пр-кт. Молодежный, д. 31',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3893',
			'title' => 'пр-кт. Молодежный, д. 33',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3894',
			'title' => 'ул. Карамзина, д. 21',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3895',
			'title' => 'ул. Карамзина, д. 23',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3896',
			'title' => 'ул. Паровозная, д. 5, к. а',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3897',
			'title' => 'ул. Судостроительная, д. 137',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3898',
			'title' => 'ул. Судостроительная, д. 139',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3899',
			'title' => 'ул. Судостроительная, д. 141',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3900',
			'title' => 'ул. Судостроительная, д. 143',
			'company_id' => '34',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3901',
			'title' => 'пер. Якорный, д. 10',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3902',
			'title' => 'пер. Якорный, д. 17А',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3903',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 165Г',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3904',
			'title' => 'ул. им Академика Вавилова, д. 27',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3905',
			'title' => 'ул. им Академика Вавилова, д. 37Г',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3906',
			'title' => 'ул. им Академика Вавилова, д. 37Д',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3907',
			'title' => 'ул. им Академика Вавилова, д. 47В',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3908',
			'title' => 'ул. им Академика Вавилова, д. 47Г',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3909',
			'title' => 'ул. Краснодарская, д. 10 А',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3910',
			'title' => 'ул. Краснодарская, д. 8',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3911',
			'title' => 'ул. Семафорная, д. 287',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3912',
			'title' => 'ул. Семафорная, д. 293',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3913',
			'title' => 'ул. Семафорная, д. 311',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3914',
			'title' => 'ул. Семафорная, д. 321',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3915',
			'title' => 'ул. Судостроительная, д. 20',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3916',
			'title' => 'ул. Судостроительная, д. 26а',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3917',
			'title' => 'ул. Судостроительная, д. 35',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3918',
			'title' => 'ул. Судостроительная, д. 37А',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3919',
			'title' => 'ул. Шелковая, д. 4А',
			'company_id' => '35',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3920',
			'title' => 'б-р. Ботанический, д. 11',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3921',
			'title' => 'б-р. Ботанический, д. 19',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3922',
			'title' => 'б-р. Ботанический, д. 21',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3923',
			'title' => 'б-р. Ботанический, д. 23',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3924',
			'title' => 'ул. Ботаническая, д. 16 А',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3925',
			'title' => 'ул. Ботаническая, д. 18',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3926',
			'title' => 'ул. им А.С.Попова, д. 18',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3927',
			'title' => 'ул. им А.С.Попова, д. 20',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3928',
			'title' => 'ул. им А.С.Попова, д. 24',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3929',
			'title' => 'ул. Лиственная, д. 20',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3930',
			'title' => 'ул. Лиственная, д. 23',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3931',
			'title' => 'ул. Мирошниченко, д. 20',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3932',
			'title' => 'ул. Мирошниченко, д. 22',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3933',
			'title' => 'ул. Пихтовая, д. 57',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3934',
			'title' => 'ул. Седова, д. 13',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3935',
			'title' => 'ул. Седова, д. 13 А',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3936',
			'title' => 'ул. Фруктовая, д. 16',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3937',
			'title' => 'ул. Фруктовая, д. 3',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3938',
			'title' => 'ул. Фруктовая, д. 5',
			'company_id' => '36',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3939',
			'title' => 'пр-кт. Металлургов, д. 28А',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3940',
			'title' => 'пр-кт. Мира, д. 120',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3941',
			'title' => 'ул. Академгородок, д. 21',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3942',
			'title' => 'ул. Академгородок, д. 25',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3943',
			'title' => 'ул. Высотная, д. 11',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3944',
			'title' => 'ул. Высотная, д. 13',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3945',
			'title' => 'ул. Гастелло, д. 17',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3946',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 10',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3947',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 27',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3948',
			'title' => 'ул. Калинина, д. 78А',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3949',
			'title' => 'ул. Комбайностроителей, д. 7',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3950',
			'title' => 'ул. Курчатова, д. 12',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3951',
			'title' => 'ул. Курчатова, д. 2',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3952',
			'title' => 'ул. Курчатова, д. 4',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3953',
			'title' => 'ул. Курчатова, д. 8',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3954',
			'title' => 'ул. Можайского, д. 21',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3955',
			'title' => 'ул. Можайского, д. 4',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3956',
			'title' => 'ул. Панфиловцев, д. 7',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3957',
			'title' => 'ул. Судостроительная, д. 27',
			'company_id' => '37',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3958',
			'title' => 'пр-кт. Металлургов, д. 30',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3959',
			'title' => 'пр-кт. Металлургов, д. 32',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3960',
			'title' => 'пр-кт. Металлургов, д. 32Б',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3961',
			'title' => 'пр-кт. Металлургов, д. 36',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3962',
			'title' => 'пр-кт. Металлургов, д. 38',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3963',
			'title' => 'пр-кт. Металлургов, д. 55/38',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3964',
			'title' => 'пр-кт. Ульяновский, д. 24А',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3965',
			'title' => 'ул. Алексеева, д. 43',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3966',
			'title' => 'ул. им В.М.Комарова, д. 6',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3967',
			'title' => 'ул. Краснодарская, д. 3',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3968',
			'title' => 'ул. Маршала Малиновского, д. 27',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3969',
			'title' => 'ул. Партизана Железняка, д. 34',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3970',
			'title' => 'ул. Писателя Н.Устиновича, д. 4',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3971',
			'title' => 'ул. Тельмана, д. 14А',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3972',
			'title' => 'ул. Тельмана, д. 27',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3973',
			'title' => 'ул. Тельмана, д. 28А',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3974',
			'title' => 'ул. Тельмана, д. 3А',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3975',
			'title' => 'ул. Тельмана, д. 41А',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3976',
			'title' => 'ул. Ферганская, д. 3',
			'company_id' => '38',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3977',
			'title' => 'ул. Батурина, д. 10',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3978',
			'title' => 'ул. Батурина, д. 15',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3979',
			'title' => 'ул. Батурина, д. 5',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3980',
			'title' => 'ул. Батурина, д. 5Г',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3981',
			'title' => 'ул. Батурина, д. 5Д',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3982',
			'title' => 'ул. Весны, д. 7Б',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3983',
			'title' => 'ул. Взлетная, д. 30',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3984',
			'title' => 'ул. Дубровинского, д. 100',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3985',
			'title' => 'ул. Молокова, д. 17',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3986',
			'title' => 'ул. Молокова, д. 19',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3987',
			'title' => 'ул. Молокова, д. 27',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3988',
			'title' => 'ул. Молокова, д. 31В',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3989',
			'title' => 'ул. Молокова, д. 31Д',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3990',
			'title' => 'ул. Молокова, д. 5В',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3991',
			'title' => 'ул. Молокова, д. 5Г',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3992',
			'title' => 'ул. Перенсона, д. 1',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3993',
			'title' => 'ул. Перенсона, д. 1а',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3994',
			'title' => 'ул. Северная, д. 10',
			'company_id' => '39',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3995',
			'title' => 'б-р. Солнечный Бульвар, д. 7',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3996',
			'title' => 'пр-кт. 60 лет образования СССР, д. 20',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3997',
			'title' => 'пр-кт. 60 лет образования СССР, д. 39',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3998',
			'title' => 'пр-кт. 60 лет образования СССР, д. 8',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '3999',
			'title' => 'пр-кт. Молодежный, д. 1',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4000',
			'title' => 'пр-кт. Молодежный, д. 2',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4001',
			'title' => 'пр-кт. Молодежный, д. 6',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4002',
			'title' => 'пр-кт. Молодежный, д. 7',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4003',
			'title' => 'ул. 40 лет Победы, д. 12',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4004',
			'title' => 'ул. им П.М.Петрушина, д. 1',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4005',
			'title' => 'ул. Светлова, д. 3',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4006',
			'title' => 'ул. Светлова, д. 3А',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4007',
			'title' => 'ул. Светлова, д. 5',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4008',
			'title' => 'ул. Светлова, д. 5А',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4009',
			'title' => 'ул. Светлова, д. 5Г',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4010',
			'title' => 'ул. Светлова, д. 7/1',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4011',
			'title' => 'ул. Светлова, д. 9/1',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4012',
			'title' => 'ул. Светлова, д. 9/2',
			'company_id' => '40',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4013',
			'title' => 'наб. Ярыгинская, д. 11',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4014',
			'title' => 'наб. Ярыгинская, д. 13',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4015',
			'title' => 'наб. Ярыгинская, д. 13А',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4016',
			'title' => 'наб. Ярыгинская, д. 15',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4017',
			'title' => 'наб. Ярыгинская, д. 3',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4018',
			'title' => 'наб. Ярыгинская, д. 5',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4019',
			'title' => 'наб. Ярыгинская, д. 7',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4020',
			'title' => 'наб. Ярыгинская, д. 9',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4021',
			'title' => 'наб. Ярыгинская, д. 9А',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4022',
			'title' => 'ул. Карамзина, д. 10',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4023',
			'title' => 'ул. Карамзина, д. 12',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4024',
			'title' => 'ул. Карамзина, д. 4',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4025',
			'title' => 'ул. Карамзина, д. 6',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4026',
			'title' => 'ул. Карамзина, д. 7',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4027',
			'title' => 'ул. Карамзина, д. 8',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4028',
			'title' => 'ул. Судостроительная, д. 155',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4029',
			'title' => 'ул. Судостроительная, д. 157',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4030',
			'title' => 'ул. Судостроительная, д. 175',
			'company_id' => '41',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4031',
			'title' => 'пр-кт. Мира, д. 105',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4032',
			'title' => 'пр-кт. Свободный, д. 12',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4033',
			'title' => 'ул. Баумана, д. 10',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4034',
			'title' => 'ул. Баумана, д. 12',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4035',
			'title' => 'ул. Баумана, д. 12А',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4036',
			'title' => 'ул. Баумана, д. 16А',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4037',
			'title' => 'ул. Баумана, д. 20',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4038',
			'title' => 'ул. Горького, д. 59',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4039',
			'title' => 'ул. Калинина, д. 45и',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4040',
			'title' => 'ул. Карла Маркса, д. 148',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4041',
			'title' => 'ул. Ломоносова, д. 100',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4042',
			'title' => 'ул. Ломоносова, д. 52',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4043',
			'title' => 'ул. Ломоносова, д. 94, к. 1',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4044',
			'title' => 'ул. Ломоносова, д. 94А',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4045',
			'title' => 'ул. Новая Заря, д. 13',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4046',
			'title' => 'ул. Новая Заря, д. 19',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4047',
			'title' => 'ул. Новосибирская, д. 54',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4048',
			'title' => 'ул. Спартаковцев, д. 71А',
			'company_id' => '42',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4049',
			'title' => 'пер. Уютный, д. 7',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4050',
			'title' => 'пер. Уютный, д. 9',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4051',
			'title' => 'пр-кт. Свободный, д. 72А',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4052',
			'title' => 'ул. Академика Киренского, д. 65',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4053',
			'title' => 'ул. Академика Киренского, д. 69',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4054',
			'title' => 'ул. Бабушкина, д. 2',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4055',
			'title' => 'ул. Вербная, д. 10',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4056',
			'title' => 'ул. Вербная, д. 4',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4057',
			'title' => 'ул. Вербная, д. 6',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4058',
			'title' => 'ул. Вербная, д. 8',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4059',
			'title' => 'ул. Гусарова, д. 15',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4060',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 6А',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4061',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 1Г',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4062',
			'title' => 'ул. Курчатова, д. 15А',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4063',
			'title' => 'ул. Курчатова, д. 15Б',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4064',
			'title' => 'ул. Курчатова, д. 9В',
			'company_id' => '43',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4065',
			'title' => 'ул. Академика Павлова, д. 13',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4066',
			'title' => 'ул. Алеши Тимошенкова, д. 139',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4067',
			'title' => 'ул. Алеши Тимошенкова, д. 141',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4068',
			'title' => 'ул. Алеши Тимошенкова, д. 143',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4069',
			'title' => 'ул. Алеши Тимошенкова, д. 145',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4070',
			'title' => 'ул. Алеши Тимошенкова, д. 149',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4071',
			'title' => 'ул. Гастелло, д. 35',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4072',
			'title' => 'ул. Гастелло, д. 37',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4073',
			'title' => 'ул. Гастелло, д. 39',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4074',
			'title' => 'ул. Корнетова Дружинника, д. 4',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4075',
			'title' => 'ул. Кутузова, д. 93',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4076',
			'title' => 'ул. Кутузова, д. 95',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4077',
			'title' => 'ул. Станочная, д. 13',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4078',
			'title' => 'ул. Щербакова, д. 1',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4079',
			'title' => 'ул. Щербакова, д. 3',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4080',
			'title' => 'ул. Щорса, д. 14',
			'company_id' => '44',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4081',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 39',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4082',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 40',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4083',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 40А',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4084',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 42',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4085',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 42, к. А',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4086',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 44',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4087',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 47',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4088',
			'title' => 'проезд. Центральный, д. 2',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4089',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 9',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4090',
			'title' => 'ул. Академика Павлова, д. 35',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4091',
			'title' => 'ул. Волжская, д. 9',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4092',
			'title' => 'ул. Семафорная, д. 419',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4093',
			'title' => 'ул. Семафорная, д. 435',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4094',
			'title' => 'ул. Юности, д. 17',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4095',
			'title' => 'ул. Юности, д. 21',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4096',
			'title' => 'ул. Юности, д. 23',
			'company_id' => '45',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4097',
			'title' => 'д. 3, ул. Апрельская',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4098',
			'title' => 'д. ул. Апрельская, д. 1',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4099',
			'title' => 'ул. Капитанская, д. 10',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4100',
			'title' => 'ул. Капитанская, д. 12',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4101',
			'title' => 'ул. Капитанская, д. 14',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4102',
			'title' => 'ул. Капитанская, д. 16',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4103',
			'title' => 'ул. Капитанская, д. 6',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4104',
			'title' => 'ул. Капитанская, д. 8',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4105',
			'title' => 'ул. Навигационная, д. 4',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4106',
			'title' => 'ул. Навигационная, д. 5',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4107',
			'title' => 'ул. Навигационная, д. 7',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4108',
			'title' => 'ул. Парусная, д. 10',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4109',
			'title' => 'ул. Парусная, д. 12',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4110',
			'title' => 'ул. Парусная, д. 8',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4111',
			'title' => 'ул. Пушкина, д. 32',
			'company_id' => '46',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4112',
			'title' => 'пр-кт. Комсомольский, д. 23',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4113',
			'title' => 'ул. Водянникова, д. 2, лит. а',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4114',
			'title' => 'ул. Водянникова, д. 2, лит. в',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4115',
			'title' => 'ул. им Героя Советского Союза А.В.Водянников, д. 2, стр. Б',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4116',
			'title' => 'ул. Линейная, д. 78',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4117',
			'title' => 'ул. Любы Шевцовой, д. 69',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4118',
			'title' => 'ул. Мате Залки, д. 11',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4119',
			'title' => 'ул. Мате Залки, д. 11, стр. А',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4120',
			'title' => 'ул. Мате Залки, д. 13',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4121',
			'title' => 'ул. Мате Залки, д. 7',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4122',
			'title' => 'ул. Мате Залки, д. 9',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4123',
			'title' => 'ул. Чернышевского, д. 63',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4124',
			'title' => 'ул. Чернышевского, д. 65',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4125',
			'title' => 'ул. Чернышевского, д. 67',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4126',
			'title' => 'ул. Юрия Гагарина, д. 64, стр. 1',
			'company_id' => '47',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4127',
			'title' => 'пер. Светлогорский, д. 10',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4128',
			'title' => 'пер. Светлогорский, д. 8',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4129',
			'title' => 'ул. 9 Мая, д. 39',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4130',
			'title' => 'ул. 9 Мая, д. 47',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4131',
			'title' => 'ул. 9 Мая, д. 54',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4132',
			'title' => 'ул. 9 Мая, д. 54А',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4133',
			'title' => 'ул. 9 Мая, д. 56',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4134',
			'title' => 'ул. 9 Мая, д. 56а',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4135',
			'title' => 'ул. Водопьянова, д. 5',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4136',
			'title' => 'ул. Мате Залки, д. 20',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4137',
			'title' => 'ул. Мате Залки, д. 22',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4138',
			'title' => 'ул. Мате Залки, д. 38',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4139',
			'title' => 'ул. Мужества, д. 21',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4140',
			'title' => 'ул. Мужества, д. 23',
			'company_id' => '48',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4141',
			'title' => 'ул. А.Д.Кравченко, д. 2',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4142',
			'title' => 'ул. А.Д.Кравченко, д. 6',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4143',
			'title' => 'ул. А.Д.Кравченко, д. 8',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4144',
			'title' => 'ул. Академика Киренского, д. 41',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4145',
			'title' => 'ул. Академика Киренского, д. 43',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4146',
			'title' => 'ул. Академика Киренского, д. 45',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4147',
			'title' => 'ул. Академика Киренского, д. 83',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4148',
			'title' => 'ул. Белорусская, д. 7',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4149',
			'title' => 'ул. Белорусская, д. 9',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4150',
			'title' => 'ул. Михаила Годенко, д. 1',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4151',
			'title' => 'ул. Михаила Годенко, д. дом 6',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4152',
			'title' => 'ул. Серова, д. 10',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4153',
			'title' => 'ул. Серова, д. 12',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4154',
			'title' => 'ул. Серова, д. 38',
			'company_id' => '49',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4155',
			'title' => 'пер. Вузовский, д. 14',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4156',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 103Б',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4157',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 109А',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4158',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 122',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4159',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 130',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4160',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 69',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4161',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 73',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4162',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 75А',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4163',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 92А',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4164',
			'title' => 'ул. Западная, д. 3а',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4165',
			'title' => 'ул. им Академика Вавилова, д. 51, к. а',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4166',
			'title' => 'ул. Коммунальная, д. 8А',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4167',
			'title' => 'ул. Семафорная, д. 327',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4168',
			'title' => 'ул. Шелковая, д. 4',
			'company_id' => '50',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4169',
			'title' => 'ул. Железнодорожников, д. 18б',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4170',
			'title' => 'ул. Железнодорожников, д. 18в',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4171',
			'title' => 'ул. Железнодорожников, д. 21',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4172',
			'title' => 'ул. Железнодорожников, д. 26 А',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4173',
			'title' => 'ул. Железнодорожников, д. 32',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4174',
			'title' => 'ул. Маерчака, д. 1',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4175',
			'title' => 'ул. Маерчака, д. 25',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4176',
			'title' => 'ул. Маерчака, д. 27',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4177',
			'title' => 'ул. Мечникова, д. 10',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4178',
			'title' => 'ул. Озерная, д. 41',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4179',
			'title' => 'ул. Северо-Енисейская, д. 46',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4180',
			'title' => 'ул. Северо-Енисейская, д. 46а',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4181',
			'title' => 'ул. Северо-Енисейская, д. 50',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4182',
			'title' => 'ул. Северо-Енисейская, д. 52',
			'company_id' => '51',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4183',
			'title' => 'ул. 78 Добровольческой бригады, д. 28',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4184',
			'title' => 'ул. 78 Добровольческой бригады, д. 32',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4185',
			'title' => 'ул. 78 Добровольческой бригады, д. 34',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4186',
			'title' => 'ул. 78 Добровольческой бригады, д. 40',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4187',
			'title' => 'ул. Алексеева, д. 45',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4188',
			'title' => 'ул. Алексеева, д. 47',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4189',
			'title' => 'ул. Алексеева, д. 51',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4190',
			'title' => 'ул. Алексеева, д. 53',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4191',
			'title' => 'ул. Алексеева, д. 89',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4192',
			'title' => 'ул. Батурина, д. 36',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4193',
			'title' => 'ул. Батурина, д. 38',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4194',
			'title' => 'ул. Батурина, д. 40',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4195',
			'title' => 'ул. Шахтеров, д. 38',
			'company_id' => '52',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4196',
			'title' => 'ул. Алексеева, д. 7',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4197',
			'title' => 'ул. Бограда, д. 26',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4198',
			'title' => 'ул. Диктатуры пролетариата, д. 11',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4199',
			'title' => 'ул. Ивана Забобонова, д. 14',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4200',
			'title' => 'ул. Карла Маркса, д. 139',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4201',
			'title' => 'ул. Карла Маркса, д. 141',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4202',
			'title' => 'ул. Карла Маркса, д. 147',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4203',
			'title' => 'ул. Красной Армии, д. 22',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4204',
			'title' => 'ул. Обороны, д. 2Б',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4205',
			'title' => 'ул. Сады, д. 2И',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4206',
			'title' => 'ул. Сурикова, д. 17/58',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4207',
			'title' => 'ул. Урицкого, д. 120',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4208',
			'title' => 'ул. Урицкого, д. 124',
			'company_id' => '53',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4209',
			'title' => 'ул. Дмитрия Мартынова, д. 21',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4210',
			'title' => 'ул. Дмитрия Мартынова, д. 24',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4211',
			'title' => 'ул. Дмитрия Мартынова, д. 25',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4212',
			'title' => 'ул. Дмитрия Мартынова, д. 29',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4213',
			'title' => 'ул. Дмитрия Мартынова, д. 31',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4214',
			'title' => 'ул. Дмитрия Мартынова, д. 33',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4215',
			'title' => 'ул. Дмитрия Мартынова, д. 37',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4216',
			'title' => 'ул. Дмитрия Мартынова, д. 39',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4217',
			'title' => 'ул. Линейная, д. 99',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4218',
			'title' => 'ул. Мужества, д. 16',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4219',
			'title' => 'ул. Чернышевского, д. 110',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4220',
			'title' => 'ул. Чернышевского, д. 71',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4221',
			'title' => 'ул. Чернышевского, д. 73',
			'company_id' => '54',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4222',
			'title' => 'ул. Алексеева, д. 101',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4223',
			'title' => 'ул. Алексеева, д. 103',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4224',
			'title' => 'ул. Алексеева, д. 97',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4225',
			'title' => 'ул. Алексеева, д. 99',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4226',
			'title' => 'ул. Армейская, д. 31',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4227',
			'title' => 'ул. Воронова, д. 10А',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4228',
			'title' => 'ул. Воронова, д. 10б',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4229',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 18, лит. Ж',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4230',
			'title' => 'ул. Краснодарская, д. 22а',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4231',
			'title' => 'ул. Октябрьская, д. 3',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4232',
			'title' => 'ул. Октябрьская, д. 5',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4233',
			'title' => 'ул. П.И.Словцова, д. 1',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4234',
			'title' => 'ул. Писателя Н.Устиновича, д. 34',
			'company_id' => '55',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4235',
			'title' => 'ул. Александра Матросова, д. 5',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4236',
			'title' => 'ул. Александра Матросова, д. 7',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4237',
			'title' => 'ул. Алексеева, д. 3',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4238',
			'title' => 'ул. Горького, д. 44',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4239',
			'title' => 'ул. Дубровинского, д. 50',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4240',
			'title' => 'ул. Железнодорожников, д. 16б',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4241',
			'title' => 'ул. Карла Маркса, д. 19',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4242',
			'title' => 'ул. Королева, д. 3',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4243',
			'title' => 'ул. Королева, д. 7Б',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4244',
			'title' => 'ул. Ленина, д. 127',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4245',
			'title' => 'ул. Ленина, д. 129',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4246',
			'title' => 'ул. Перенсона, д. 38/49',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4247',
			'title' => 'ул. Профсоюзов, д. 16',
			'company_id' => '56',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4248',
			'title' => 'ул. Машиностроителей, д. 17, Дивногорск',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4249',
			'title' => 'ул. Взлетная, д. 34',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4250',
			'title' => 'ул. Взлетная, д. 36',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4251',
			'title' => 'ул. Взлетная, д. 38',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4252',
			'title' => 'ул. Калинина, д. 177',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4253',
			'title' => 'ул. Калинина, д. 179',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4254',
			'title' => 'ул. Калинина, д. 181',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4255',
			'title' => 'ул. Калинина, д. 183',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4256',
			'title' => 'ул. Калинина, д. 183 А',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4257',
			'title' => 'ул. Куйбышева, д. 93',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4258',
			'title' => 'ул. Молокова, д. 29',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4259',
			'title' => 'ул. Молокова, д. 31',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4260',
			'title' => 'ул. Молокова, д. 33',
			'company_id' => '57',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4261',
			'title' => 'ул. Березина, д. 108',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4262',
			'title' => 'ул. Березина, д. 110',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4263',
			'title' => 'ул. Дальневосточная, д. 3/1',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4264',
			'title' => 'ул. Дальневосточная, д. 3/2',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4265',
			'title' => 'ул. Дальневосточная, д. 3/3',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4266',
			'title' => 'ул. Караульная, д. 46',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4267',
			'title' => 'ул. Ольховая, д. 10',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4268',
			'title' => 'ул. Ольховая, д. 4',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4269',
			'title' => 'ул. Ольховая, д. 6',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4270',
			'title' => 'ул. Партизана Железняка, д. 61',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4271',
			'title' => 'ул. Фурманова, д. 93',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4272',
			'title' => 'ул. Фурманова, д. 95',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4273',
			'title' => 'ул. Чернышевского, д. 120',
			'company_id' => '58',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4274',
			'title' => 'ул. Алексеева, д. 11',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4275',
			'title' => 'ул. Алексеева, д. 9',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4276',
			'title' => 'ул. Грунтовая, д. 28д',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4277',
			'title' => 'ул. Ивана Забобонова, д. 16',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4278',
			'title' => 'ул. Ивана Забобонова, д. 18',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4279',
			'title' => 'ул. Кутузова, д. 83а',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4280',
			'title' => 'ул. Сады, д. 1К',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4281',
			'title' => 'ул. Сады, д. 2К',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4282',
			'title' => 'ул. Свердловская, д. 139',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4283',
			'title' => 'ул. Свердловская, д. 141',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4284',
			'title' => 'ул. Судостроительная, д. 159',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4285',
			'title' => 'ул. Судостроительная, д. 161',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4286',
			'title' => 'ул. Судостроительная, д. 163',
			'company_id' => '59',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4287',
			'title' => 'ул. Весны, д. 2',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4288',
			'title' => 'ул. Взлетная, д. 12',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4289',
			'title' => 'ул. Декабристов, д. 32',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4290',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 18',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4291',
			'title' => 'ул. им Корнеева, д. 61',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4292',
			'title' => 'ул. Ленина, д. 135',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4293',
			'title' => 'ул. Линейная, д. 76',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4294',
			'title' => 'ул. Славы, д. 15',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4295',
			'title' => 'ул. Славы, д. 5',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4296',
			'title' => 'ул. Славы, д. 7',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4297',
			'title' => 'ул. Сурикова, д. 6',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4298',
			'title' => 'ул. Урицкого, д. 51',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4299',
			'title' => 'ул. Хабаровская 2-я, д. 4А',
			'company_id' => '60',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4300',
			'title' => 'пр-кт. Металлургов, д. 14',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4301',
			'title' => 'пр-кт. Ульяновский, д. 2',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4302',
			'title' => 'пр-кт. Ульяновский, д. 2Б',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4303',
			'title' => 'пр-кт. Ульяновский, д. 2В',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4304',
			'title' => 'ул. Космонавта Терешковой, д. 2',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4305',
			'title' => 'ул. Космонавта Терешковой, д. 4',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4306',
			'title' => 'ул. Космонавта Терешковой, д. 8',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4307',
			'title' => 'ул. Новгородская, д. 8А',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4308',
			'title' => 'ул. Писателя Н.Устиновича, д. 16',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4309',
			'title' => 'ул. Тельмана, д. 15',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4310',
			'title' => 'ул. Тельмана, д. 17',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4311',
			'title' => 'ул. Ферганская, д. 8',
			'company_id' => '61',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4312',
			'title' => 'ул. Дмитрия Мартынова, д. 11',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4313',
			'title' => 'ул. Дмитрия Мартынова, д. 9',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4314',
			'title' => 'ул. Линейная, д. 84',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4315',
			'title' => 'ул. Линейная, д. 94',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4316',
			'title' => 'ул. Полярная, д. 10',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4317',
			'title' => 'ул. Полярная, д. 12',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4318',
			'title' => 'ул. Полярная, д. 4',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4319',
			'title' => 'ул. Полярная, д. 6',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4320',
			'title' => 'ул. Полярная, д. 8',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4321',
			'title' => 'ул. Шахтеров, д. 19',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4322',
			'title' => 'ул. Шахтеров, д. 21',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4323',
			'title' => 'ул. Шахтеров, д. 23',
			'company_id' => '62',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4324',
			'title' => 'ул. Калинина, д. 15',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4325',
			'title' => 'ул. Калинина, д. 17',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4326',
			'title' => 'ул. Калинина, д. 37',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4327',
			'title' => 'ул. Калинина, д. 8',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4328',
			'title' => 'ул. Ломоносова, д. 11, к. А',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4329',
			'title' => 'ул. Норильская, д. 4 Г',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4330',
			'title' => 'ул. Норильская, д. 4 Д',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4331',
			'title' => 'ул. Республики, д. 33 А',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4332',
			'title' => 'ул. Республики, д. 35',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4333',
			'title' => 'ул. Республики, д. 37 А',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4334',
			'title' => 'ул. Республики, д. 39',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4335',
			'title' => 'пр-кт. Комсомольский, д. 9',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4336',
			'title' => 'ул. 9 Мая, д. 12',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4337',
			'title' => 'ул. 9 Мая, д. 18',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4338',
			'title' => 'ул. 9 Мая, д. 20 а',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4339',
			'title' => 'ул. 9 Мая, д. 26',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4340',
			'title' => 'ул. 9 Мая, д. 41',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4341',
			'title' => 'ул. им Н.Н.Урванцева, д. 26',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4342',
			'title' => 'ул. им Н.Н.Урванцева, д. 30',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4343',
			'title' => 'ул. им Н.Н.Урванцева, д. 33',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4344',
			'title' => 'ул. им Н.Н.Урванцева, д. 6А',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4345',
			'title' => 'ул. Светлогорская, д. 17 А',
			'company_id' => '64',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4346',
			'title' => 'ул. Академика Павлова, д. 79',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4347',
			'title' => 'ул. Академика Павлова, д. 83',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4348',
			'title' => 'ул. Академика Павлова, д. 85',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4349',
			'title' => 'ул. Александра Матросова, д. 7А',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4350',
			'title' => 'ул. Александра Матросова, д. 9',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4351',
			'title' => 'ул. Александра Матросова, д. 9А',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4352',
			'title' => 'ул. Алеши Тимошенкова, д. 165',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4353',
			'title' => 'ул. Алеши Тимошенкова, д. 197',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4354',
			'title' => 'ул. Кутузова, д. 44',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4355',
			'title' => 'ул. Львовская, д. 52',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4356',
			'title' => 'ул. Паровозная, д. 2',
			'company_id' => '13',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4357',
			'title' => 'пр-кт. Металлургов, д. 2А',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4358',
			'title' => 'ул. 78 Добровольческой бригады, д. 11',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4359',
			'title' => 'ул. Взлетная, д. 7А',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4360',
			'title' => 'ул. Взлетная, д. 7д',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4361',
			'title' => 'ул. Взлетная, д. 7Ж',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4362',
			'title' => 'ул. Взлетная, д. 7К',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4363',
			'title' => 'ул. Партизана Железняка, д. 26А',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4364',
			'title' => 'ул. Партизана Железняка, д. 9Г',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4365',
			'title' => 'ул. Тельмана, д. 1 а',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4366',
			'title' => 'ул. Тельмана, д. 18А',
			'company_id' => '66',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4367',
			'title' => 'ул. 9 Мая, д. 17, к. 1',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4368',
			'title' => 'ул. 9 Мая, д. 19',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4369',
			'title' => 'ул. 9 Мая, д. 5',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4370',
			'title' => 'ул. 9 Мая, д. 7',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4371',
			'title' => 'ул. им Б.З.Шумяцкого, д. 6',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4372',
			'title' => 'ул. Космонавтов, д. 17',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4373',
			'title' => 'ул. Космонавтов, д. 17А',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4374',
			'title' => 'ул. Космонавтов, д. 17Г',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4375',
			'title' => 'ул. Мате Залки, д. 2Д',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4376',
			'title' => 'ул. Мате Залки, д. 4Г',
			'company_id' => '67',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4377',
			'title' => 'ул. Дмитрия Мартынова, д. 27',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4378',
			'title' => 'ул. Дмитрия Мартынова, д. 35',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4379',
			'title' => 'ул. Дмитрия Мартынова, д. 41',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4380',
			'title' => 'ул. Дмитрия Мартынова, д. 43',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4381',
			'title' => 'ул. Дмитрия Мартынова, д. 45',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4382',
			'title' => 'ул. Дмитрия Мартынова, д. 47',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4383',
			'title' => 'ул. Караульная, д. 48',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4384',
			'title' => 'ул. Любы Шевцовой, д. 74',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4385',
			'title' => 'ул. Любы Шевцовой, д. 76',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4386',
			'title' => 'ул. Любы Шевцовой, д. 78',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4387',
			'title' => 'ул. Апрельская, д. 5а',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4388',
			'title' => 'ул. Кутузова, д. 56',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4389',
			'title' => 'ул. Кутузова, д. 66',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4390',
			'title' => 'ул. Львовская, д. 26',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4391',
			'title' => 'ул. Спортивная, д. 170',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4392',
			'title' => 'ул. Щорса, д. 54',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4393',
			'title' => 'ул. Щорса, д. 65',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4394',
			'title' => 'ул. Щорса, д. 69',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4395',
			'title' => 'ул. Щорса, д. 87',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4396',
			'title' => 'ул. Щорса, д. 97',
			'company_id' => '69',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4397',
			'title' => 'ул. Академгородок, д. 1',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4398',
			'title' => 'ул. Академгородок, д. 15',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4399',
			'title' => 'ул. Академгородок, д. 16',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4400',
			'title' => 'ул. Академгородок, д. 17а',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4401',
			'title' => 'ул. Академгородок, д. 18',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4402',
			'title' => 'ул. Академгородок, д. 19',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4403',
			'title' => 'ул. Академгородок, д. 1А',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4404',
			'title' => 'ул. Академгородок, д. 20А',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4405',
			'title' => 'ул. Академгородок, д. 7',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4406',
			'title' => 'ул. Академгородок, д. 9',
			'company_id' => '70',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4407',
			'title' => 'ул. Авиаторов, д. 45',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4408',
			'title' => 'ул. Авиаторов, д. дом 47',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4409',
			'title' => 'ул. Петра Ломако, д. 14',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4410',
			'title' => 'ул. Петра Ломако, д. 2',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4411',
			'title' => 'ул. Петра Ломако, д. 8',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4412',
			'title' => 'д. 1, Солонцы',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4413',
			'title' => 'д. 3, сооруж. Жилой дом, Солонцы',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4414',
			'title' => 'д. 5А, сооруж. жилой дом, Солонцы',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4415',
			'title' => 'д. дом 5, Солонцы',
			'company_id' => '71',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4416',
			'title' => 'пр-кт. Свободный, д. 57',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4417',
			'title' => 'ул. им Героя Советского Союза Н.Я.Тотмина, д. 7',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4418',
			'title' => 'ул. Крупской, д. 32',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4419',
			'title' => 'ул. Крупской, д. 6',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4420',
			'title' => 'ул. Курчатова, д. 13',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4421',
			'title' => 'ул. Курчатова, д. 15, к. 1',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4422',
			'title' => 'ул. Курчатова, д. 15, стр. 2',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4423',
			'title' => 'ул. Курчатова, д. 7А',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4424',
			'title' => 'ул. Ладо Кецховели, д. 93',
			'company_id' => '72',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4425',
			'title' => 'б-р. Ботанический, д. 17',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4426',
			'title' => 'ул. Новостроек, д. 1, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4427',
			'title' => 'ул. Новостроек, д. 2, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4428',
			'title' => 'ул. Новостроек, д. 3, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4429',
			'title' => 'ул. Новостроек, д. 4, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4430',
			'title' => 'ул. Новостроек, д. 5, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4431',
			'title' => 'ул. Новостроек, д. 6, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4432',
			'title' => 'ул. Новостроек, д. 7, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4433',
			'title' => 'ул. Новостроек, д. 8, Солонцы',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4434',
			'title' => 'ул. 9 Мая, д. 63',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4435',
			'title' => 'ул. Водопьянова, д. 4',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4436',
			'title' => 'ул. Водопьянова, д. 6В',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4437',
			'title' => 'ул. Водопьянова, д. 8',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4438',
			'title' => 'ул. им Н.Н.Урванцева, д. 14',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4439',
			'title' => 'ул. им Н.Н.Урванцева, д. 2',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4440',
			'title' => 'ул. Светлогорская, д. 17Г',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4441',
			'title' => 'ул. Светлогорская, д. 33',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4442',
			'title' => 'ул. Светлогорская, д. 35',
			'company_id' => '74',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4443',
			'title' => 'пр-кт. Мира, д. 16',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4444',
			'title' => 'пр-кт. Мира, д. 22',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4445',
			'title' => 'пр-кт. Мира, д. 37',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4446',
			'title' => 'ул. 9 Января, д. 23',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4447',
			'title' => 'ул. 9 Января, д. 26А',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4448',
			'title' => 'ул. Дубровинского, д. 82',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4449',
			'title' => 'ул. Карла Маркса, д. 56',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4450',
			'title' => 'ул. Марковского, д. 7',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4451',
			'title' => 'ул. Сурикова, д. 3',
			'company_id' => '75',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4452',
			'title' => 'ул. Дивная, д. 6, к. 1_2',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4453',
			'title' => 'ул. Дивная, д. 8, к. 1_13',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4454',
			'title' => 'ул. Живописная, д. 1, к. 12, 13',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4455',
			'title' => 'ул. Живописная, д. 3, к. 1, 2',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4456',
			'title' => 'ул. Живописная, д. 3, к. 3, 4',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4457',
			'title' => 'ул. Живописная, д. 3, к. 5_9',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4458',
			'title' => 'ул. Живописная, д. 4, к. 1_14',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4459',
			'title' => 'ул. Живописная, д. 5, к. 1_13',
			'company_id' => '76',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4460',
			'title' => 'ул. Гусарова, д. 11',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4461',
			'title' => 'ул. Гусарова, д. 1а',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4462',
			'title' => 'ул. Е.Д.Стасовой, д. 15',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4463',
			'title' => 'ул. Е.Д.Стасовой, д. 17',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4464',
			'title' => 'ул. Е.Д.Стасовой, д. 19',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4465',
			'title' => 'ул. Е.Д.Стасовой, д. 23',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4466',
			'title' => 'ул. Е.Д.Стасовой, д. 27',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4467',
			'title' => 'ул. Карла Маркса, д. 8а',
			'company_id' => '77',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4468',
			'title' => 'ул. Авиаторов, д. 23',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4469',
			'title' => 'ул. Авиаторов, д. 25',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4470',
			'title' => 'ул. Авиаторов, д. 39',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4471',
			'title' => 'ул. Авиаторов, д. 41',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4472',
			'title' => 'ул. Весны, д. 21',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4473',
			'title' => 'ул. Весны, д. 25',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4474',
			'title' => 'ул. Крайняя, д. 2а',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4475',
			'title' => 'ул. Молокова, д. 28',
			'company_id' => '23',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4476',
			'title' => 'пер. Светлогорский, д. 6',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4477',
			'title' => 'ул. 9 Мая, д. 10',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4478',
			'title' => 'ул. 9 Мая, д. 52',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4479',
			'title' => 'ул. Водопьянова, д. 11',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4480',
			'title' => 'ул. Водопьянова, д. 11, к. Г',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4481',
			'title' => 'ул. Водопьянова, д. 11А',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4482',
			'title' => 'ул. Водопьянова, д. 13',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4483',
			'title' => 'ул. Водопьянова, д. 7а',
			'company_id' => '78',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4484',
			'title' => 'б-р. Солнечный Бульвар, д. 13',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4485',
			'title' => 'ул. Воронова, д. 12Б',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4486',
			'title' => 'ул. Воронова, д. 12В',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4487',
			'title' => 'ул. Курчатова, д. 6',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4488',
			'title' => 'ул. Партизана Железняка, д. 11А',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4489',
			'title' => 'ул. Партизана Железняка, д. 11Б',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4490',
			'title' => 'ул. Телевизорная, д. 9',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4491',
			'title' => 'ул. Телевизорная, д. 9т, к. 0, стр. 0',
			'company_id' => '79',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4492',
			'title' => 'пр-кт. 60 лет образования СССР, д. 50а',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4493',
			'title' => 'пр-кт. 60 лет образования СССР, д. 52',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4494',
			'title' => 'пр-кт. 60 лет образования СССР, д. 54',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4495',
			'title' => 'пр-кт. 60 лет образования СССР, д. 58',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4496',
			'title' => 'пр-кт. 60 лет образования СССР, д. 60',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4497',
			'title' => 'пр-кт. 60 лет образования СССР, д. 62',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4498',
			'title' => 'пр-кт. 60 лет образования СССР, д. 64',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4499',
			'title' => 'пр-кт. 60 лет образования СССР, д. 66',
			'company_id' => '80',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4500',
			'title' => 'ул. Калинина, д. 185',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4501',
			'title' => 'ул. Куйбышева, д. 97',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4502',
			'title' => 'ул. Куйбышева, д. 97г',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4503',
			'title' => 'ул. Менжинского, д. 10ж',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4504',
			'title' => 'ул. Толстого, д. 17а',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4505',
			'title' => 'ул. Толстого, д. 43А',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4506',
			'title' => 'ул. Толстого, д. 45А',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4507',
			'title' => 'ул. Толстого, д. 47А',
			'company_id' => '81',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4508',
			'title' => 'ул. Дмитрия Мартынова, д. 18',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4509',
			'title' => 'ул. Дмитрия Мартынова, д. 20',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4510',
			'title' => 'ул. Дмитрия Мартынова, д. 22',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4511',
			'title' => 'ул. Линейная, д. 105',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4512',
			'title' => 'ул. Линейная, д. 107',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4513',
			'title' => 'ул. Чернышевского, д. 114',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4514',
			'title' => 'ул. Чернышевского, д. 118',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4515',
			'title' => 'ул. Чернышевского, д. 118А',
			'company_id' => '82',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4516',
			'title' => 'ул. Академика Павлова, д. 39',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4517',
			'title' => 'ул. Академика Павлова, д. 9',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4518',
			'title' => 'ул. Алеши Тимошенкова, д. 167',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4519',
			'title' => 'ул. Алеши Тимошенкова, д. 175',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4520',
			'title' => 'ул. Кочубея, д. 9',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4521',
			'title' => 'ул. Читинская, д. 10',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4522',
			'title' => 'ул. Щорса, д. 20',
			'company_id' => '83',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4523',
			'title' => 'пер. Светлогорский, д. 15',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4524',
			'title' => 'пер. Светлогорский, д. 17',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4525',
			'title' => 'пер. Светлогорский, д. 19',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4526',
			'title' => 'пер. Светлогорский, д. 21',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4527',
			'title' => 'пер. Светлогорский, д. 23',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4528',
			'title' => 'ул. 9 Мая, д. 60А',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4529',
			'title' => 'ул. 9 Мая, д. 60Г',
			'company_id' => '84',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4530',
			'title' => 'ул. Краснодарская 2-я, д. 5',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4531',
			'title' => 'ул. Краснодарская 2-я, д. 5а',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4532',
			'title' => 'ул. Краснодарская 2-я, д. 5б',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4533',
			'title' => 'ул. Краснодарская 2-я, д. 6',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4534',
			'title' => 'ул. Маршала Малиновского, д. 12',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4535',
			'title' => 'ул. Маршала Малиновского, д. 16',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4536',
			'title' => 'ул. Маршала Малиновского, д. 24',
			'company_id' => '85',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4537',
			'title' => 'пр-кт. Комсомольский, д. 22',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4538',
			'title' => 'пр-кт. Комсомольский, д. 22, к. 1',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4539',
			'title' => 'пр-кт. Комсомольский, д. 22, к. 2',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4540',
			'title' => 'ул. 9 Мая, д. 28',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4541',
			'title' => 'ул. Краснодарская, д. 17А',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4542',
			'title' => 'ул. Маршала К.Рокоссовского, д. 24',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4543',
			'title' => 'ул. Партизана Железняка, д. 2Г',
			'company_id' => '86',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4544',
			'title' => 'ул. Малаховская, д. 2',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4545',
			'title' => 'ул. Ползунова, д. 20',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4546',
			'title' => 'ул. Ползунова, д. 7',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4547',
			'title' => 'ул. Юности, д. 10',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4548',
			'title' => 'ул. Юности, д. 6',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4549',
			'title' => 'ул. Юности, д. 8',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4550',
			'title' => 'ул. Юности, д. 8А',
			'company_id' => '87',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4551',
			'title' => 'пер. Медицинский, д. 14Д',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4552',
			'title' => 'ул. 60 лет Октября, д. 80 а',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4553',
			'title' => 'ул. Семафорная, д. 189А',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4554',
			'title' => 'ул. Семафорная, д. 191А',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4555',
			'title' => 'ул. Судостроительная, д. 86',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4556',
			'title' => 'ул. Судостроительная, д. 88',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4557',
			'title' => 'ул. Судостроительная, д. 97',
			'company_id' => '88',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4558',
			'title' => 'ул. 9 Мая, д. 59А',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4559',
			'title' => 'ул. Норильская, д. 1',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4560',
			'title' => 'ул. Норильская, д. 1А',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4561',
			'title' => 'ул. Норильская, д. 3',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4562',
			'title' => 'ул. Норильская, д. 3А',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4563',
			'title' => 'ул. Норильская, д. 3Д',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4564',
			'title' => 'ул. Соколовская, д. 76',
			'company_id' => '89',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4565',
			'title' => 'ул. им Сергея Лазо, д. 8А',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4566',
			'title' => 'ул. Калинина, д. 10',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4567',
			'title' => 'ул. Калинина, д. 12',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4568',
			'title' => 'ул. Ладо Кецховели, д. 68',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4569',
			'title' => 'ул. Ладо Кецховели, д. 99',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4570',
			'title' => 'ул. Пархоменко, д. 1',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4571',
			'title' => 'ул. Энергетиков, д. 44',
			'company_id' => '90',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4572',
			'title' => 'ул. 60 лет Октября, д. 169',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4573',
			'title' => 'ул. Алеши Тимошенкова, д. 117',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4574',
			'title' => 'ул. Алеши Тимошенкова, д. 77',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4575',
			'title' => 'ул. Алеши Тимошенкова, д. 79',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4576',
			'title' => 'ул. Королева, д. 6',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4577',
			'title' => 'ул. Семафорная, д. 183',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4578',
			'title' => 'ул. Судостроительная, д. 26',
			'company_id' => '91',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4579',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 5В',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4580',
			'title' => 'ул. Глинки, д. 12',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4581',
			'title' => 'ул. Глинки, д. 12А',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4582',
			'title' => 'ул. Глинки, д. 22',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4583',
			'title' => 'ул. Глинки, д. 22А',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4584',
			'title' => 'ул. Глинки, д. 30А',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4585',
			'title' => 'ул. Рязанская, д. 31',
			'company_id' => '92',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4586',
			'title' => 'ул. Академика Киренского, д. 32',
			'company_id' => '93',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4587',
			'title' => 'ул. Академика Киренского, д. 32К',
			'company_id' => '93',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4588',
			'title' => 'ул. Академика Киренского, д. 32М',
			'company_id' => '93',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4589',
			'title' => 'ул. Дачная, д. 35',
			'company_id' => '93',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4590',
			'title' => 'ул. Дачная, д. 35а',
			'company_id' => '93',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4591',
			'title' => 'ул. Дачная, д. 37',
			'company_id' => '93',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4592',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 14 И',
			'company_id' => '94',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4593',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 16Г',
			'company_id' => '94',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4594',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 18А',
			'company_id' => '94',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4595',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 18Г',
			'company_id' => '94',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4596',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 18Д',
			'company_id' => '94',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4597',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 7А',
			'company_id' => '94',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4598',
			'title' => 'ул. Калинина, д. 18',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4599',
			'title' => 'ул. Калинина, д. 41б',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4600',
			'title' => 'ул. Норильская, д. 4А',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4601',
			'title' => 'ул. Норильская, д. 6А',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4602',
			'title' => 'ул. Норильская, д. 8А',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4603',
			'title' => 'ул. Норильская, д. 8Г',
			'company_id' => '63',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4604',
			'title' => 'ул. Петра Подзолкова, д. 3А',
			'company_id' => '95',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4605',
			'title' => 'ул. Петра Подзолкова, д. 5',
			'company_id' => '95',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4606',
			'title' => 'ул. Петра Подзолкова, д. 5А',
			'company_id' => '95',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4607',
			'title' => 'ул. Петра Подзолкова, д. 5А, стр. 2',
			'company_id' => '95',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4608',
			'title' => 'ул. Петра Подзолкова, д. 5Б',
			'company_id' => '95',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4609',
			'title' => 'ул. Петра Подзолкова, д. 5В',
			'company_id' => '95',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4610',
			'title' => 'ул. Весны, д. 3',
			'company_id' => '96',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4611',
			'title' => 'ул. Взлетная, д. 24',
			'company_id' => '96',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4612',
			'title' => 'ул. Взлетная, д. 24а',
			'company_id' => '96',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4613',
			'title' => 'ул. Взлетная, д. 26А',
			'company_id' => '96',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4614',
			'title' => 'ул. Взлетная, д. 26Б',
			'company_id' => '96',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4615',
			'title' => 'ул. Взлетная, д. 26Г',
			'company_id' => '96',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4616',
			'title' => 'ул. Гусарова, д. 12',
			'company_id' => '97',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4617',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 1',
			'company_id' => '97',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4618',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 14',
			'company_id' => '97',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4619',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 3',
			'company_id' => '97',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4620',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 6',
			'company_id' => '97',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4621',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 8',
			'company_id' => '97',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4622',
			'title' => 'пр-кт. Металлургов, д. 10Б',
			'company_id' => '98',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4623',
			'title' => 'пр-кт. Металлургов, д. 16',
			'company_id' => '98',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4624',
			'title' => 'ул. Водопьянова, д. 10',
			'company_id' => '98',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4625',
			'title' => 'ул. Водопьянова, д. 6А',
			'company_id' => '98',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4626',
			'title' => 'ул. Мате Залки, д. 4',
			'company_id' => '98',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4627',
			'title' => 'ул. Светлогорская, д. 37',
			'company_id' => '98',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4628',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 28',
			'company_id' => '100',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4629',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 32',
			'company_id' => '100',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4630',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 34',
			'company_id' => '100',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4631',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 36',
			'company_id' => '100',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4632',
			'title' => 'ул. Карамзина, д. 20',
			'company_id' => '100',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4633',
			'title' => 'ул. Карамзина, д. 22',
			'company_id' => '100',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4634',
			'title' => 'ул. Норильская, д. 34',
			'company_id' => '102',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4635',
			'title' => 'ул. Норильская, д. 36',
			'company_id' => '102',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4636',
			'title' => 'ул. Норильская, д. 38',
			'company_id' => '102',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4637',
			'title' => 'ул. Норильская, д. 40',
			'company_id' => '102',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4638',
			'title' => 'ул. Норильская, д. 42',
			'company_id' => '102',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4639',
			'title' => 'ул. Норильская, д. 44',
			'company_id' => '102',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4640',
			'title' => 'ул. Воронова, д. 12, лит. Д',
			'company_id' => '101',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4641',
			'title' => 'ул. Воронова, д. 14и',
			'company_id' => '101',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4642',
			'title' => 'ул. Маршала К.Рокоссовского, д. 24, лит. Б',
			'company_id' => '101',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4643',
			'title' => 'ул. Ястынская, д. 14',
			'company_id' => '101',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4644',
			'title' => 'ул. Ястынская, д. 4',
			'company_id' => '101',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4645',
			'title' => 'ул. Ястынская, д. 6',
			'company_id' => '101',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4646',
			'title' => 'ул. Авиаторов, д. 21',
			'company_id' => '103',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4647',
			'title' => 'ул. Весны, д. 32',
			'company_id' => '103',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4648',
			'title' => 'ул. Весны, д. 34',
			'company_id' => '103',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4649',
			'title' => 'ул. Воронова, д. 20',
			'company_id' => '103',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4650',
			'title' => 'ул. Мичурина, д. 2Ж',
			'company_id' => '103',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4651',
			'title' => 'ул. Мичурина, д. 2Ж, лит. 1Б',
			'company_id' => '103',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4652',
			'title' => 'ул. 52 Квартал, д. 1г',
			'company_id' => '104',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4653',
			'title' => 'ул. Мичурина, д. 2 Д',
			'company_id' => '104',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4654',
			'title' => 'ул. Молокова, д. 13',
			'company_id' => '104',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4655',
			'title' => 'ул. Молокова, д. 5А',
			'company_id' => '104',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4656',
			'title' => 'ул. Молокова, д. 5Б',
			'company_id' => '104',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4657',
			'title' => 'ул. Судостроительная, д. 25 Д',
			'company_id' => '105',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4658',
			'title' => 'ул. Судостроительная, д. 25а',
			'company_id' => '105',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4659',
			'title' => 'ул. Судостроительная, д. 25Г',
			'company_id' => '105',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4660',
			'title' => 'ул. Судостроительная, д. 27 А',
			'company_id' => '105',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4661',
			'title' => 'ул. Судостроительная, д. 90',
			'company_id' => '105',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4662',
			'title' => 'ул. им Б.З.Шумяцкого, д. 7, стр. Г',
			'company_id' => '106',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4663',
			'title' => 'ул. им Б.З.Шумяцкого, д. 7, стр. Д',
			'company_id' => '106',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4664',
			'title' => 'ул. им Н.Н.Урванцева, д. 4А',
			'company_id' => '106',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4665',
			'title' => 'ул. им Н.Н.Урванцева, д. 6, стр. Д',
			'company_id' => '106',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4666',
			'title' => 'ул. им Н.Н.Урванцева, д. 8, стр. А',
			'company_id' => '106',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4667',
			'title' => 'пр-кт. Металлургов, д. 28Б',
			'company_id' => '108',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4668',
			'title' => 'пр-кт. Металлургов, д. 30Д',
			'company_id' => '108',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4669',
			'title' => 'пр-кт. Металлургов, д. 34',
			'company_id' => '108',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4670',
			'title' => 'ул. Воронова, д. 31',
			'company_id' => '108',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4671',
			'title' => 'ул. Воронова, д. 35',
			'company_id' => '108',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4672',
			'title' => 'пр-кт. Мира, д. 158',
			'company_id' => '107',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4673',
			'title' => 'ул. Джамбульская, д. 4Б',
			'company_id' => '107',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4674',
			'title' => 'ул. Новгородская, д. 10А',
			'company_id' => '107',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4675',
			'title' => 'ул. Новгородская, д. 12',
			'company_id' => '107',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4676',
			'title' => 'ул. Новгородская, д. 4',
			'company_id' => '107',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4677',
			'title' => 'ул. Академгородок, д. 13',
			'company_id' => '109',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4678',
			'title' => 'ул. Академгородок, д. 17Б',
			'company_id' => '109',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4679',
			'title' => 'ул. Академгородок, д. 20',
			'company_id' => '109',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4680',
			'title' => 'ул. Академгородок, д. 6',
			'company_id' => '109',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4681',
			'title' => 'пр-кт. 60 лет образования СССР, д. 42, лит. А',
			'company_id' => '110',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4682',
			'title' => 'ул. им Героя Советского Союза Б.А.Микуцкого, д. 3',
			'company_id' => '110',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4683',
			'title' => 'ул. Партизана Железняка, д. 21 Г',
			'company_id' => '110',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4684',
			'title' => 'ул. Партизана Железняка, д. 21, лит. А',
			'company_id' => '110',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4685',
			'title' => 'ул. Линейная, д. 116',
			'company_id' => '111',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4686',
			'title' => 'ул. Линейная, д. 118',
			'company_id' => '111',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4687',
			'title' => 'ул. Линейная, д. 120',
			'company_id' => '111',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4688',
			'title' => 'ул. Линейная, д. 122',
			'company_id' => '111',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4689',
			'title' => 'ул. 9 Мая, д. 63а',
			'company_id' => '112',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4690',
			'title' => 'ул. 9 Мая, д. 67',
			'company_id' => '112',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4691',
			'title' => 'ул. 9 Мая, д. 69',
			'company_id' => '112',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4692',
			'title' => 'ул. Судостроительная, д. 62',
			'company_id' => '112',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4693',
			'title' => 'ул. Александра Матросова, д. 23',
			'company_id' => '113',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4694',
			'title' => 'ул. Александра Матросова, д. 25',
			'company_id' => '113',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4695',
			'title' => 'ул. Ломоносова, д. 94г',
			'company_id' => '113',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4696',
			'title' => 'ул. Пушкина, д. 22А',
			'company_id' => '113',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4697',
			'title' => 'ул. Бабушкина, д. 41',
			'company_id' => '114',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4698',
			'title' => 'ул. Бабушкина, д. 41, к. Д',
			'company_id' => '114',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4699',
			'title' => 'ул. Советская, д. 135',
			'company_id' => '114',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4700',
			'title' => 'ул. Советская, д. 137',
			'company_id' => '114',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4701',
			'title' => 'ул. Академика Киренского, д. 17',
			'company_id' => '115',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4702',
			'title' => 'ул. Академика Киренского, д. 3А',
			'company_id' => '115',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4703',
			'title' => 'ул. Дачная, д. 30 А',
			'company_id' => '115',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4704',
			'title' => 'ул. Ивана Забобонова, д. 10',
			'company_id' => '115',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4705',
			'title' => 'ул. Е.Д.Стасовой, д. 40, лит. И',
			'company_id' => '116',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4706',
			'title' => 'ул. Е.Д.Стасовой, д. 40А',
			'company_id' => '116',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4707',
			'title' => 'ул. Е.Д.Стасовой, д. 40к',
			'company_id' => '116',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4708',
			'title' => 'ул. Е.Д.Стасовой, д. 40Л',
			'company_id' => '116',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4709',
			'title' => 'пр-кт. Комсомольский, д. 5',
			'company_id' => '117',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4710',
			'title' => 'ул. 9 Мая, д. 17',
			'company_id' => '117',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4711',
			'title' => 'ул. 9 Мая, д. 27',
			'company_id' => '117',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4712',
			'title' => 'ул. им Б.З.Шумяцкого, д. 7, лит. а',
			'company_id' => '117',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4713',
			'title' => 'ул. Свердловская, д. 15 Б',
			'company_id' => '119',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4714',
			'title' => 'ул. Свердловская, д. 15В',
			'company_id' => '119',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4715',
			'title' => 'ул. Свердловская, д. 17Б',
			'company_id' => '119',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4716',
			'title' => 'ул. Свердловская, д. 17В',
			'company_id' => '119',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4717',
			'title' => 'ул. 9 Мая, д. 58',
			'company_id' => '118',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4718',
			'title' => 'ул. 9 Мая, д. 58 Б',
			'company_id' => '118',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4719',
			'title' => 'ул. 9 Мая, д. 58А',
			'company_id' => '118',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4720',
			'title' => 'ул. Мате Залки, д. 6',
			'company_id' => '118',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4721',
			'title' => 'пр-кт. Машиностроителей, д. 11а',
			'company_id' => '120',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4722',
			'title' => 'ул. Даурская, д. 10',
			'company_id' => '120',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4723',
			'title' => 'ул. Даурская, д. 6',
			'company_id' => '120',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4724',
			'title' => 'ул. Тобольская, д. 1',
			'company_id' => '120',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4725',
			'title' => 'ул. Весны, д. 9',
			'company_id' => '121',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4726',
			'title' => 'ул. Гусарова, д. 13',
			'company_id' => '121',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4727',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 10В',
			'company_id' => '121',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4728',
			'title' => 'ул. им Героя Советского Союза М.А.Юшкова, д. 18, к. Г',
			'company_id' => '121',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4729',
			'title' => 'проезд. Северный, д. 10',
			'company_id' => '122',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4730',
			'title' => 'проезд. Северный, д. 4',
			'company_id' => '122',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4731',
			'title' => 'проезд. Северный, д. 6',
			'company_id' => '122',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4732',
			'title' => 'проезд. Северный, д. 8',
			'company_id' => '122',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4733',
			'title' => 'кв-л. 25-й, д. 9, Ачинск',
			'company_id' => '123',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4734',
			'title' => 'ул. 40 лет ВЛКСМ, д. 16, Ачинск',
			'company_id' => '123',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4735',
			'title' => 'ул. Декабристов, д. 44, Ачинск',
			'company_id' => '123',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4736',
			'title' => 'ул. Революции, д. 11, Ачинск',
			'company_id' => '123',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4737',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 115, к. а',
			'company_id' => '124',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4738',
			'title' => 'пр-кт. им газеты Красноярский Рабочий, д. 137А',
			'company_id' => '124',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4739',
			'title' => 'ул. Побежимова, д. 11',
			'company_id' => '124',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4740',
			'title' => 'пр-кт. 60 лет образования СССР, д. 31',
			'company_id' => '125',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4741',
			'title' => 'пр-кт. 60 лет образования СССР, д. 33',
			'company_id' => '125',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4742',
			'title' => 'ул. 40 лет Победы, д. 28',
			'company_id' => '125',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4743',
			'title' => 'ул. Водопьянова, д. 10А',
			'company_id' => '127',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4744',
			'title' => 'ул. им Н.Н.Урванцева, д. 29',
			'company_id' => '127',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4745',
			'title' => 'ул. Светлогорская, д. 19',
			'company_id' => '127',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4746',
			'title' => 'ул. Мате Залки, д. 2',
			'company_id' => '129',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4747',
			'title' => 'ул. Мате Залки, д. 2, к. 1',
			'company_id' => '129',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4748',
			'title' => 'ул. Мате Залки, д. 2А',
			'company_id' => '129',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4749',
			'title' => 'ул. Академика Киренского, д. 9а',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4750',
			'title' => 'ул. Александра Матросова, д. 30, стр. 83',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4751',
			'title' => 'ул. им Героя Советского Союза Б.К.Чернышева, д. 4',
			'company_id' => '73',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4752',
			'title' => 'ул. Молокова, д. 1 г',
			'company_id' => '130',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4753',
			'title' => 'ул. Молокова, д. 1 Д',
			'company_id' => '130',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4754',
			'title' => 'ул. Молокова, д. 1 К',
			'company_id' => '130',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4755',
			'title' => 'ул. Ладо Кецховели, д. 40',
			'company_id' => '128',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4756',
			'title' => 'ул. Ладо Кецховели, д. 62А',
			'company_id' => '128',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4757',
			'title' => 'ул. Новосибирская, д. 29',
			'company_id' => '128',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4758',
			'title' => 'ул. Соколовская, д. 72',
			'company_id' => '131',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4759',
			'title' => 'ул. Соколовская, д. 74',
			'company_id' => '131',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4760',
			'title' => 'ул. Соколовская, д. 80',
			'company_id' => '131',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4761',
			'title' => 'ул. Новая, д. 68',
			'company_id' => '133',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4762',
			'title' => 'ул. Щербакова, д. 15',
			'company_id' => '133',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4763',
			'title' => 'ул. Щорса, д. 8',
			'company_id' => '133',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4764',
			'title' => 'ул. Алексеева, д. 48А',
			'company_id' => '132',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4765',
			'title' => 'ул. Октябрьская, д. 8',
			'company_id' => '132',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4766',
			'title' => 'ул. Октябрьская, д. 8, к. а',
			'company_id' => '132',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4767',
			'title' => 'ул. Кирова, д. 43',
			'company_id' => '134',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4768',
			'title' => 'ул. Ленина, д. 52',
			'company_id' => '134',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4769',
			'title' => 'б-р. Солнечный Бульвар, д. 15',
			'company_id' => '135',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4770',
			'title' => 'ул. Светлова, д. 8',
			'company_id' => '135',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4771',
			'title' => 'пр-кт. Свободный, д. 28',
			'company_id' => '136',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4772',
			'title' => 'пр-кт. Свободный, д. 28 А',
			'company_id' => '136',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4773',
			'title' => 'ул. Михаила Годенко, д. 3',
			'company_id' => '137',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4774',
			'title' => 'ул. Софьи Ковалевской, д. 2',
			'company_id' => '137',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4775',
			'title' => 'ул. Соколовская, д. 76А',
			'company_id' => '138',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4776',
			'title' => 'ул. Соколовская, д. 80а',
			'company_id' => '138',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4777',
			'title' => 'ул. Академика Киренского, д. 58',
			'company_id' => '139',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4778',
			'title' => 'ул. Ладо Кецховели, д. 17а',
			'company_id' => '139',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4779',
			'title' => 'ул. Ключевская, д. 83',
			'company_id' => '140',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4780',
			'title' => 'ул. Ключевская, д. 85',
			'company_id' => '140',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4781',
			'title' => 'проезд. Северный, д. 12',
			'company_id' => '141',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4782',
			'title' => 'проезд. Северный, д. 16',
			'company_id' => '141',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4783',
			'title' => 'г. Красноярск, ул. им Героя Советского Союза М.А.Юшкова, д. дом 36 ж',
			'company_id' => '143',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4784',
			'title' => 'ул. Михаила Годенко, д. 7',
			'company_id' => '143',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4785',
			'title' => 'ул. Батурина, д. 5А',
			'company_id' => '144',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4786',
			'title' => 'ул. Весны, д. 2А',
			'company_id' => '144',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4787',
			'title' => 'ул. Московская, д. 41',
			'company_id' => '145',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4788',
			'title' => 'ул. Чайковского, д. 8',
			'company_id' => '145',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4789',
			'title' => 'ул. Ленина, д. 65, Уяр',
			'company_id' => '146',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4790',
			'title' => 'ул. Лермонтова, д. 3, Уяр',
			'company_id' => '146',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4791',
			'title' => 'ул. Ады Лебедевой, д. 141',
			'company_id' => '147',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4792',
			'title' => 'ул. Декабристов, д. 49',
			'company_id' => '147',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4793',
			'title' => 'ул. Академгородок, д. 23',
			'company_id' => '148',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4794',
			'title' => 'ул. Академгородок, д. 28',
			'company_id' => '148',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4795',
			'title' => 'ул. 8 Марта, д. 22Б',
			'company_id' => '149',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4796',
			'title' => 'ул. Александра Матросова, д. 21',
			'company_id' => '149',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4797',
			'title' => 'ул. Лесопарковая, д. 25',
			'company_id' => '150',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4798',
			'title' => 'ул. Лесопарковая, д. 27',
			'company_id' => '150',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4799',
			'title' => 'ул. Калинина, д. 65, к. 2',
			'company_id' => '151',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4800',
			'title' => 'ул. им Героя Советского Союза В.В.Вильского, д. 16',
			'company_id' => '152',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4801',
			'title' => 'ул. Партизана Железняка, д. 19А',
			'company_id' => '153',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4802',
			'title' => 'ул. П.И.Словцова, д. 10',
			'company_id' => '154',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4803',
			'title' => 'б-р. Солнечный Бульвар, д. 11, к. общ',
			'company_id' => '155',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4804',
			'title' => 'ул. Северо-Енисейская, д. 48',
			'company_id' => '156',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4805',
			'title' => 'ул. Республики, д. 47',
			'company_id' => '158',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4806',
			'title' => 'ул. Михаила Годенко, д. 4',
			'company_id' => '160',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4807',
			'title' => 'ул. Норильская, д. 16, лит. И',
			'company_id' => '161',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4808',
			'title' => 'ул. Ленина, д. 5, к. А',
			'company_id' => '162',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4809',
			'title' => 'ул. Молокова, д. 28А',
			'company_id' => '159',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4810',
			'title' => 'ул. Светлогорская, д. 11а',
			'company_id' => '164',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4811',
			'title' => 'ул. Кутузова, д. 40А',
			'company_id' => '163',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4812',
			'title' => 'ул. Семафорная, д. 417',
			'company_id' => '165',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4813',
			'title' => 'ул. Мичурина, д. 14',
			'company_id' => '166',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4814',
			'title' => 'пр-кт. Комсомольский, д. 3Ж',
			'company_id' => '167',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4815',
			'title' => 'ул. Норильская, д. 1Д',
			'company_id' => '169',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4816',
			'title' => 'ул. Калинина, д. 47 Н',
			'company_id' => '168',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4817',
			'title' => 'ул. им Шевченко, д. 48',
			'company_id' => '170',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4818',
			'title' => 'ул. Весны, д. 7',
			'company_id' => '171',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4819',
			'title' => 'ул. Железнодорожников, д. 11',
			'company_id' => '172',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4820',
			'title' => 'ул. 26 Бакинских Комиссаров, д. 5г',
			'company_id' => '173',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4821',
			'title' => 'ул. Волочаевская, д. 11а',
			'company_id' => '175',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4822',
			'title' => 'пер. Афонтовский, д. 9',
			'company_id' => '176',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4823',
			'title' => 'ул. Борисова, д. 28',
			'company_id' => '174',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4824',
			'title' => 'ул. Авиаторов, д. 29',
			'company_id' => '177',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4825',
			'title' => 'ул. Березина, д. 98',
			'company_id' => '68',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4826',
			'title' => 'ул. 40 лет Победы, д. 10',
			'company_id' => '178',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4827',
			'title' => 'ул. Кишиневская, д. 4 А',
			'company_id' => '180',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4828',
			'title' => 'ул. Дубенского, д. 2',
			'company_id' => '181',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4829',
			'title' => 'пр-кт. 60 лет образования СССР, д. 62Д',
			'company_id' => '182',
		]);

		Capsule::table('adresses')->insert([
			'id' => '4830',
			'title' => 'ул. Железнодорожников, д. 9',
			'company_id' => '179',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
