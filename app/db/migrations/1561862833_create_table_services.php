<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_services_1561862833
{
    public function up()
    {
        Capsule::schema()->create('services', function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('category_id');
            $table->integer('normativ');
            $table->integer('ball');
        });
    }
}