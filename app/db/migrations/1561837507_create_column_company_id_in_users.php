<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_column_company_id_in_users_1561837507 {
    public function up() {
        Capsule::schema()->table('users', function($table) {
            $table->integer('company_id')->default(0);
        });
    }
}
