<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_categories_1561862834
{
    public function up()
    {
        Capsule::schema()->create('categories', function ($table) {
            $table->increments('id');
            $table->string('title');
        });
    }
}