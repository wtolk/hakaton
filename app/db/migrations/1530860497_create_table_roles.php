<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_roles_1530860497 {
    public function up() {
        Capsule::schema()->create('roles', function($table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name')->nullable();
            $table->text('permissions');
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
