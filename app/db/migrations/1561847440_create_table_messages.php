<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_messages_1561847440 {
    public function up() {
        Capsule::schema()->create('messages', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('chat_id')->nullable();
            $table->integer('recipient_id')->nullable();
            $table->text('text')->nullable();
            $table->timestamp('created_at');
        });
    }
}
