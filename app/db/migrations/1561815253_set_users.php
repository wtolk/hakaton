<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use App\Helpers\Config;

class set_users_1561815253 {
    public function up() {
        $dataset = [
            [
                'email' => 'user1@example.com',
                'password' => 'user1',
                'snils' => '1234',
                'first_name' => 'Игорь',
                'last_name' => 'Печенька'
            ],
            [
                'email' => 'demo@jkh.ru',
                'password' => 'demo',
                'snils' => '1234',
                'first_name' => 'Игорь',
                'last_name' => 'Печенька'
            ],
            [
                'email' => 'root',
                'password' => 'root',
                'snils' => '1234',
                'first_name' => 'Игорь',
                'last_name' => 'Печенька'
            ]
        ];

        $sentinel = function () {
            $sentinel = (new \Cartalyst\Sentinel\Native\Facades\Sentinel())->getSentinel();
            $sentinel->getUserRepository()->setModel(\App\Models\User::class);
            $sentinel->getPersistenceRepository()->setUsersModel(\App\Models\User::class);
            return $sentinel;
        };

        $sentinel = $sentinel();

        foreach ($dataset as $data){
            $user = \App\Models\User::where('email', $data['email'])->first();
            if ($user === null ){
                $user = $sentinel->registerAndActivate($data);
                if ($user->email == 'root') {
                    $role = $sentinel->findRoleBySlug('root');
                } elseif ($user->email == 'demo@jkh.ru') {
                    $role = $sentinel->findRoleBySlug('company');
                } else {
                    $role = $sentinel->findRoleBySlug('client');
                }
                $role->users()->attach($user);
            }
        }
    }
}