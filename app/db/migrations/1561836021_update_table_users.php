<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_users_1561836021 {
    public function up() {
        Capsule::schema()->table('users', function($table) {
            $table->integer('address_id');
            $table->integer('apartments');
        });
    }
}
