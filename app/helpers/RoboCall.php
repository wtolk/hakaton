<?php


use Curl\Curl;

class RoboCall
{
    public static function call($phone, $message)
    {
        $url = 'https://calltools.ru/lk/cabapi_external/api/v1/phones/call/';
        $curl = new Curl();
        $curl->get($url, [
            'phone' => '+'.$phone,
            'public_key' => '95fbad22f9983b6377b956f160f03291',
            'text' => $message,
            'campaign_id' => 1267411664
        ]);
    }
}