<?php

namespace App\Helpers;

use App\Models\NavigationItem;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use League\Event\Event;

use App\Helpers\EventDispatcher;

class TwigCustomExtensions extends AbstractExtension
{
    public function __construct($container) {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('getNavigation', array($this, 'getNavigation')),
            new TwigFunction('showPaginate', array($this, 'showPaginate')),
        );
    }

    public function getNavigation($menu_id)
    {
        $user = $this->container['sentinel']->check();
        $items = NavigationItem::where('navigation_id', $menu_id)->orderBy('position')->where('parent_id', null)->with('children')->get()->toArray();

        $this->container->emitter->emit('render_navigation', [&$items]);

        foreach ($items as $key=>$item)
        {
            if ($this->hasAccess($user, $item) === false)
            {
                unset($items[$key]);
            }

            if (count($item['children']) > 0)
            {
                foreach ($item['children'] as $sub_key=>$sub_item)
                {
                    $this->hasAccess($user, $sub_item);
                    if ($this->hasAccess($user, $sub_item) === false)
                    {
                        unset($items[$key]['children'][$key]);
                    }
                }

            }
        }

        unset($item, $sub_item);

        $len = 0;
        $active_key = 0;
        $sub_active_key = null;
        foreach($items as $key=>$item)
        {
            $path = $this->container->router->pathFor($item['path']);
            $pos = strripos($_SERVER['REQUEST_URI'], $path);

            if ($pos || $pos === 0)
            {
                if (strlen($path) > $len)
                {
                    $len = strlen($path);
                    $active_key = $key;
                }
            }

            if (count($item['children']) > 0)
            {
                foreach ($item['children'] as $sub_key=>$sub_item)
                {
                    $path = $this->container->router->pathFor($sub_item['path']);
                    $pos = strripos($_SERVER['REQUEST_URI'], $path);

                    if ($pos || $pos === 0)
                    {
                        if (strlen($path) > $len)
                        {
                            $len = strlen($path);
                            $sub_active_key = $sub_key;
                            $sub_active_parent_key = $key;
                        }
                    }
                }

            }
        }

        if (!is_null($sub_active_key))
        {
            $items[$sub_active_parent_key]['children'][$sub_active_key]['class'] = 'active';
        }else{
            $items[$active_key]['class'] = 'active';
        }

        return $items;
    }

    public function hasAccess($user, $item)
    {
        $permissions = json_decode($item['permissions']);

        $permissions_arr = [];
        foreach ($permissions as $permission_name=>$value)
        {
            if($value == 'true')
            {
                array_push($permissions_arr, $permission_name);
            }
        }

        if (!$user->hasAccess($permissions_arr)) {
            unset($item);
            return false;
        }

        return true;
    }

    /**
     * @param $objects - массив или объект для пагинации
     * @return string - html разметка пагинации
     */
    public function showPaginate($objects)
    {
        $html = '';
        if (is_array($objects)) {
            if ($objects['total'] > $objects['per_page']) {
                if ($objects['current_page'] > 1) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page=1">&lt;&lt;В начало</a>
                                </li>';
                }
                for ($i = 3; $i >= 1; $i--) {
                    if ($objects['current_page'] - $i > 0) {
                        $number = $objects['current_page'] - $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page='.$number.'">'.$number.'</a>
                                  </li>';
                    }
                }
                $html .= '<li class="page-item active">
                                <a href="" class="page-link active-link">'.$objects['current_page'].'</a>
                            </li>';
                for ($i = 1; $i <= 3; $i++) {
                    if ($objects['current_page'] + $i < $objects['last_page']) {
                        $number = $objects['current_page'] + $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page='.$number.'">'.$number.'</a>
                                  </li>';
                    }
                }
                if ($objects['current_page'] < $objects['last_page']) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page='.$objects['last_page'].'">В конец&gt;&gt;</a>
                                </li>';
                }
            }
        } else {
            if ($objects->total() > $objects->perPage()) {
                if ($objects->currentPage() > 1) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page=1">&lt;&lt;В начало</a>
                                </li>';
                }
                for ($i = 3; $i >= 1; $i--) {
                    if ($objects->currentPage() - $i > 0) {
                        $number = $objects->currentPage() - $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page='.$number.'">'.$number.'</a>
                                  </li>';
                    }
                }
                $html .= '<li class="page-item active">
                                <a href="" class="page-link active-link">'.$objects->currentPage().'</a>
                            </li>';
                for ($i = 1; $i <= 3; $i++) {
                    if ($objects->currentPage() + $i < $objects->lastPage()) {
                        $number = $objects->currentPage() + $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page='.$number.'">'.$number.'</a>
                                  </li>';
                    }
                }
                if ($objects->currentPage() < $objects->lastPage()) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?'.$this->getDataFromUrl().'page='.$objects->lastPage().'">В конец&gt;&gt;</a>
                                </li>';
                }
            }
        }
        return '<nav class="row justify-content-center">
                    <ul class="pagination">
                        '.$html.'
                    </ul>
                </nav>
            ';
    }

    function getDataFromUrl()
    {
        $urlParams = '';
        foreach ($_REQUEST as $key => $value) {
            if ($key != 'page') {
                $urlParams .= $key.'='.$value.'&';
            }
        }
        return $urlParams;
    }
}