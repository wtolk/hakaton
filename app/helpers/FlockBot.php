<?php
/**
 * Created by PhpStorm.
 * User: h
 * Date: 30.08.2018
 * Time: 15:31
 */

namespace App\Helpers;


use Curl\Curl;

class FlockBot
{

    /* ID, token чатов и пользоватей
     *
     * App ID: 8aa884b5-aa93-4c81-bac5-479c20b041b7
     * App Secret: 6eaef109-4ea9-4594-aa3e-b4404d7fccc2
     * Token: b0ce17a6-731f-45d2-94c1-ada4ba265f93
     * Захаров Андрей ID: u:v8fq8hfhm819f6m1
     * Общая команда ID: g:194408_lobby
     * Бот Евгений ID: u:Bnjh4jnjj6c74rj4
     * Бот Token: 3eb3da3c-3dff-4203-9673-b015efd62f33
     * */
    private static $curl;
    private static $instance;

    /**
     * @var string Захаров Андрей ID
     */
    public static $Z_ANDREY_ID = 'u:v8fq8hfhm819f6m1';

    /**
     * @var string Общий чат ID
     */
    public static $OUR_CHAT_ID = 'g:194408_lobby';

    /**
     * @var string Token Бота
     */
    public static $BOT_TOKEN = '3eb3da3c-3dff-4203-9673-b015efd62f33';

    /**
     * @var string ID Бота
     */
    public static $BOT_ID = 'u:Bnjh4jnjj6c74rj4';

    /**
     * FlockBot constructor.
     * @throws \ErrorException
     */
    private function __construct()
    {
        self::$curl = new Curl();
    }

    public static function sendMessage(string $to, string $text, string $from = null)
    {
        $from = $from ?? self::$Z_ANDREY_ID;
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        self::$instance::$curl->get('https://api.flock.co/v1/chat.sendMessage?to='.$to.'&text='.$text.'&token=3eb3da3c-3dff-4203-9673-b015efd62f33&onBehalfOf='.$from.'');
    }

}