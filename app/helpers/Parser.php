<?php
namespace App\Helpers;


error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

use App\Models\Adress;
use App\Models\Company;
use \Curl\Curl;
use Illuminate\Database\Capsule\Manager as DB;



class Parser
{
    public $html;
    public $list_link;
    public $count_links;
    public $curl;
    public $otvet;
    public $cook;
    public $site;

    public function __construct($url, $site, $massiv = '', $ref = '' ) // получает контент страницы $massiv это куки
    {
        $this->site = $site;
        $curl = new Curl();
        $curl->setOpt(CURLOPT_FOLLOWLOCATION, TRUE);
        $curl->setUserAgent('Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.7.62 Version/11.01');
        //устанавливаем куки
        if (!empty($massiv)) {
            foreach ($massiv as $key => $value) {
                $curl->setCookie($key, $value);
            }
        }
        //устанавливаем реферер
        $curl->setReferrer($ref);

        $html = $curl->get($url);
        $this->cook = $curl->getResponseCookies();
        $this->html = $html;
        $this->curl = $curl;
        //$this->otvet = $curl->getResponseCookie('sessid');
    }

    public function get($selector)
    {
        $parser = new \nokogiri($this->html);
        return $parser->get($selector)->toArray();
    }

    public function getListLink($selector) // получаем список нужных ссылок для обхода
    {
        $parser = new \nokogiri($this->html);
        $this->list_link = $parser->get($selector)->toArray();
        $this->count_links = count($this->list_link);
    }

    public function getSelector($selector)// получаем инфо нужного селектора
    {
        $parser = new \nokogiri($this->html);
        return $parser->get($selector)->toArray()['0'];
    }

    public function setOrder() // Ложим полученные ссылки в очередь
    {

        foreach ($this->list_link as $link) {
            if ($this->site == 'job.khit.info') {
                $url = phpUri::parse('http://job.khit.info/')->join($link['href']); // преобразовали в абсолютный путь
            } elseif ( $this->site == 'avito' ) {
                $url = phpUri::parse('http://avito.ru')->join($link['href']); // преобразовали в абсолютный путь
            }

            // вычленяем из урла, один парметр с id
            $remote_id = $this->getIdFromUrl($url);
            // Получаем массив remote_id из базы
            $rids = DB::table('parser_list')->pluck('remote_id')->toArray();
            // Если remote_id есть в базе, пропускаем цикл
            if (in_array($remote_id, $rids)) {
                continue;
            }

            //Записываем информацию о ссылке в базе
            DB::table('parser_list')->insert(
                ['url' => $url, 'remote_id' => $remote_id, 'site' => $this->site]
            );


        }
    }



    public function getData($selector)
    {
        $bot = new \nokogiri($this->html);
        $data = $bot->get($selector)->toArray();
        //var_dump($data['0']['#text']['0']);
        //$text = $data['0']['#text'];
        if (!array_key_exists('#text', $data['0'])) {
            $text = 'нет данных';
        } else {
            $text = $data['0']['#text'];
        }
        //var_dump($text);
        //die;
        return $text;
    }

    public function getText($selector)
    {
        $bot = new \nokogiri($this->html);
        $data = $bot->get($selector)->toText();
        return $data;
    }

    public function getHTML($selector)
    {
        $bot = new \nokogiri($this->html);
//        ddd($bot->get('.item-description-text')->toXml());
        $data = $bot->get($selector)->toXml();
        return $data;
    }

    public static function getCompanyInfo($link)
    {
        $p = new Parser($link, 'minjkh');
        $company = [];

        $labels = $p->get('dl.company dt');

        foreach ($labels as $i => $label) {
            $index = $i + 1;
            if ($label['#text'][0] == "ИНН") {
                $company['inn'] = $p->getText('dl.company dd:nth-child('.$index.')');
            }
            if ($label['#text'][0] == "E-mail") {
                $company['email'] = $p->getText('dl.company dd:nth-child('.$index.')');
            }
            if ($label['#text'][0] == "Дома в управлении") {
                $company['house_count'] = $p->getText('dl.company dd:nth-child('.$index.')');
                $company['house_count'] = explode(' ', $company['house_count'])[0];
                $company['house_count'] = str_replace(' ', '', $company['house_count']);
            }
            if ($label['#text'][0] == "Диспетчерская служба") {
                $company['phones'] = $p->getText('dl.company dd:nth-child('.$index.')');
            }
            if ($label['#text'][0] == "Телефон (ы)" && !$company['phones']) {
                $company['phones'] = $p->getText('dl.company dd:nth-child('.$index.')');
            }

            $company['title'] = $p->getText('h1');


        }

        $record = Company::where('title', '=', $company['title'])->first();
        if (!$record) {
            Company::create($company);
        }

    }

    public static function getAdressesFromCompanyPage($link) {

        $p = new Parser($link, 'minjkh');
        $company_id = $p->get('#companyid')[0]['value'];
        $company_title = $p->getText('h1');

        $curl = new Curl();
        $curl->post('http://mingkh.ru/api/houses/', [
            'current' => 1,
            'rowCount' => 2140,
            'companyid' => $company_id
        ]);

        $records = json_decode($curl->rawResponse, true)['rows'];
        foreach ($records as $record) {
            $adress = str_replace(', Красноярск', '', $record['address']);
            Adress::create(['title' => $adress, 'company_id' => Company::where('title', '=', $company_title)->first()->id]);
        }

    }







}