<?php
namespace FileUploader\Controllers;

use App\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Slim\Http\Request;
use Slim\Http\Response;

class UploaderController extends Controller
{
    /**Страница с загрузчиком
     */
    public function uploaderPage()
    {
        $this->render('uploader.twig');
    }


    public function upload(Request $request)
    {
        $data = $request->getParams();

        $test_user_data = [
            'email' => 'test',
            'password' => 'test'
        ];

        $user = User::create($test_user_data)->checkFiles('files', $data['uploader']);
        ddd($user->id);
    }


    public function uploaderExistsPage($request, $response, $args)
    {
        $user = User::with(['files' => function (MorphMany $query) {
            $query->orderBy('position');
        }])->find($args['user_id']);

        $this->twig_vars['user'] = $user->toArray();
        $this->render('uploader.twig');
    }

    public function uploadExists(Request $request, Response $response, $args)
    {
        $data = $request->getParams();
        $user = User::find($args['user_id'])->checkFiles('files', $data['uploader']);
        return $response->withRedirect('/uploader/'.$user->id);
    }
}